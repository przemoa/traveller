import os
import pytest

from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.service.traveller_database.traveller_database import TravellerDatabase
from traveller.common.types import TravellerException

import paths_for_tests
from test_traveller_database.base_test_class import BaseTestClass


class TestCountryCityAndAdminRegions(BaseTestClass):
    """
    Stateful test, tests have to be executed in numerical order to work correctly!
    """
    def setup_class(self):
        db_path = paths_for_tests.TEMPORARY_TRAVELLER_DATABASE
        try:
            os.remove(db_path)
        except OSError:
            pass
        try:
            self.db = TravellerDatabase(database_path=db_path)
        except IOError:
            print('Traveller database can not be created at path: {}'
                  .format(db_path))
            assert False

    def teardown_class(self):
        pass

    def test_whole_sequence(self):
        self.partial_1_test_add_country()
        self.partial_1_test_get_by_id()
        self.partial_1_test_add_admin_region_major()
        self.partial_1_test_add_admin_minor()
        self.partial_1_test_add_city()

        self.partial_1_test_read_country()
        self.partial_1_test_read_admin_region_major()
        self.partial_1_test_read_admin_minor()
        self.partial_1_test_read_cities()

        self.partial_1_test_get_location_selection_from_youngest_selected_location_id()

        self.partial_1_test_update_city()

        self.partial_1_test_delete_city()


    def partial_1_test_add_country(self):
        # Adds 5 countries
        countries = [
            Country(
                name='Poland',
                iso_code='PL',
                continent_code='EU'),
            Country(
                name='Germany',
                iso_code='DE',
                continent_code='EU'),
            Country(
                name='Sweden',
                iso_code='SE',
                continent_code='EU'),
            Country(
                name='Albania',
                iso_code='XY',
                continent_code='EU'),
            Country(
                name='Portugal',
                iso_code='PO',
                continent_code='EU'),
        ]
        for country in countries:
            self.db.add_country(country)

    def partial_1_test_get_by_id(self):
        country_1 = self.db.get_item_by_db_id(Country, 1)
        assert country_1.iso_code == 'PL'

        country_3 = self.db.get_item_by_db_id(Country, 3)
        assert country_3.iso_code == 'SE'

    def partial_1_test_add_admin_region_major(self):
        # Adds 1 location, 3 major regions
        country = Country(
                    name='Poland',
                    iso_code='PL',
                    continent_code='EU')
        admin_region_major = AdminRegionMajor(
            ascii_name='Lubusz',
            country_code='PL')
        self.db.add_admin_region_major(
            admin_region_major=admin_region_major,
            country=country,
            raise_for_duplicate=False)

        admin_region_major = AdminRegionMajor(
            ascii_name='Greater Poland')
        self.db.add_admin_region_major(admin_region_major=admin_region_major, country=country)

        country = Country(
                    name='France',
                    iso_code='FR',
                    continent_code='EU')
        admin_region_major = AdminRegionMajor(
            ascii_name='Nice Whole')
        self.db.add_admin_region_major(admin_region_major=admin_region_major, country=country)
        with pytest.raises(TravellerException):
            self.db.add_admin_region_major(admin_region_major=admin_region_major, country=country)

    def partial_1_test_add_admin_minor(self):
        # Adds 0 location, 1 major region, 4 minor regions
        country = Country(
            name='Poland',
            iso_code='PL',
            continent_code='EU')
        admin_region_major = AdminRegionMajor(
            ascii_name='Lubusz',
            country_code='PL')
        admin_region_minor = AdminRegionMinor(
            ascii_name='Zielonogorski'
        )
        self.db.add_admin_region_minor(
            admin_region_minor=admin_region_minor,
            admin_region_major=admin_region_major,
            country=country,
            raise_for_duplicate=False)
        with pytest.raises(TravellerException):
            self.db.add_admin_region_minor(
                admin_region_minor=admin_region_minor,
                admin_region_major=admin_region_major,
                country=country)

        self.db.add_admin_region_minor(
            admin_region_minor=AdminRegionMinor('Gorzowski'),
            admin_region_major=AdminRegionMajor('Lubusz'),
            country=Country(iso_code="PL"),
            raise_for_duplicate=False)
        self.db.add_admin_region_minor(
            admin_region_minor=AdminRegionMinor('Gorzowski'),
            admin_region_major=AdminRegionMajor('Lubusz'),
            country=Country(iso_code="PL"),
            raise_for_duplicate=False)


        self.db.add_admin_region_minor(
            admin_region_minor=AdminRegionMinor('Poznanski'),
            admin_region_major=AdminRegionMajor('Greater Poland'),
            country=Country(iso_code="PL"),
            raise_for_duplicate=False)

        self.db.add_admin_region_minor(
            admin_region_minor=AdminRegionMinor('Nice City'),
            admin_region_major=AdminRegionMajor('Nice Whole'),
            country=Country(iso_code="FR"),
            raise_for_duplicate=False)

    def partial_1_test_add_city(self):
        # Adder 0 location, 0 major region, 1 minor region, 3 cities
        country = Country(
            name='Poland',
            iso_code='PL',
            continent_code='EU')
        admin_region_major = AdminRegionMajor(
            ascii_name='Lubusz',
            country_code='PL')
        admin_region_minor = AdminRegionMinor(
            ascii_name='Zielonogorski'
        )
        city = City('Kargowa', population=4000)
        self.db.add_city(
            city=city,
            admin_region_minor=admin_region_minor,
            admin_region_major=admin_region_major,
            country=country,
            raise_for_duplicate=False)

        self.db.add_city(
            city=City('Komorniki'),
            admin_region_minor=AdminRegionMinor('Poznanski'),
            admin_region_major=AdminRegionMajor('Greater Poland'),
            country=Country('PL'),
            raise_for_duplicate=False)

        self.db.add_city(
            city=City('Komorniki'),
            admin_region_minor=AdminRegionMinor('Poznanski'),
            admin_region_major=AdminRegionMajor('Greater Poland'),
            country=Country('PL'),
            raise_for_duplicate=False)

        self.db.add_city(
            city=City('Zielona Gora'),
            admin_region_minor=AdminRegionMinor('Zielona Gora Powiat'),
            admin_region_major=AdminRegionMajor('Lubusz'),
            country=Country('PL'),
            raise_for_duplicate=False)

    def partial_1_test_read_country(self):
        countries = self.db.get_country_prompts(contained_text='')
        assert len(countries) == 6

    def partial_1_test_read_admin_region_major(self):
        country_pl = self.db.get_country_by_iso_code(iso_code='PL')
        admin_majors = self.db.get_admin_region_major_prompts(
            contained_text='', parent_location=country_pl)
        assert len(admin_majors) == 2

        admin_majors = self.db.get_admin_region_major_prompts(
            contained_text='')
        assert len(admin_majors) == 3

        admin_majors = self.db.get_admin_region_major_prompts(
            contained_text='Great', parent_location=country_pl)
        assert len(admin_majors) == 1

    def partial_1_test_read_admin_minor(self):
        country_pl = self.db.get_country_by_iso_code(iso_code='PL')
        admin_minors = self.db.get_admin_region_minor_prompts(
            contained_text='', parent_location=None)
        assert len(admin_minors) == 5

        admin_minors = self.db.get_admin_region_minor_prompts(
            contained_text='', parent_location=country_pl)
        assert len(admin_minors) == 4

        admin_major = AdminRegionMajor('Lubusz')
        self.db.add_admin_region_major(
            admin_major,
            country=country_pl,
            raise_for_duplicate=False)

        admin_minors = self.db.get_admin_region_minor_prompts(
            contained_text='', parent_location=admin_major)
        assert len(admin_minors) == 3

    def partial_1_test_read_cities(self):
        cities = self.db.get_city_prompts(parent_location=None, contained_text='')
        assert len(cities) == 3
        country_pl = self.db.get_country_by_iso_code(iso_code='PL')

        cities = self.db.get_city_prompts(parent_location=country_pl, contained_text='')
        assert len(cities) == 3

        admin_major = AdminRegionMajor('Lubusz')
        self.db.add_admin_region_major(
            admin_major,
            country=country_pl,
            raise_for_duplicate=False)
        cities = self.db.get_city_prompts(parent_location=admin_major, contained_text='')
        assert len(cities) == 2

        admin_minor = AdminRegionMinor('Zielona Gora Powiat')
        self.db.add_admin_region_minor(admin_region_minor=admin_minor,
                                       admin_region_major=admin_major,
                                       country=country_pl,
                                       raise_for_duplicate=False)
        cities = self.db.get_city_prompts(parent_location=admin_minor, contained_text='')
        assert len(cities) == 1

    def partial_1_test_update_city(self):
        country_pl = self.db.get_country_by_iso_code(iso_code='PL')
        admin_major = AdminRegionMajor('Lubusz')
        admin_minor = AdminRegionMinor('Zielona Gora Powiat')
        self.db.add_admin_region_minor(admin_region_minor=admin_minor,
                                       admin_region_major=admin_major,
                                       country=country_pl,
                                       raise_for_duplicate=False)
        cities = self.db.get_city_prompts(parent_location=admin_minor, contained_text='')
        assert len(cities) == 1
        city = cities[0]
        city.population = 12345
        city.ascii_name = 'nowa nazwa miasta'
        self.db.update_item(city)

        cities_updated = self.db.get_city_prompts(parent_location=admin_minor, contained_text='')
        assert len(cities_updated) == 1
        city_updated = cities[0]
        assert city_updated.population == 12345
        assert city_updated.ascii_name == 'nowa nazwa miasta'

    def partial_1_test_delete_city(self):
        cities = self.db.get_city_prompts(parent_location=None, contained_text='')
        assert len(cities) == 3

        self.db.delete_item(cities[0])
        cities = self.db.get_city_prompts(parent_location=None, contained_text='')
        assert len(cities) == 2

    def partial_1_test_get_location_selection_from_youngest_selected_location_id(self):
        selection = self.db.get_location_selection_from_youngest_selected_location_id(16)
        print(selection.get_short_description_text_or_empty_for_field())
        assert selection.city.ascii_name == 'Komorniki'
        assert selection.admin_minor.ascii_name == 'Poznanski'
        assert selection.admin_major.ascii_name == 'Greater Poland'


if __name__ == '__main__':
    # use it for debug
    ttdb = TestCountryCityAndAdminRegions()
    ttdb.setup_class()

    ttdb.test_whole_sequence()
