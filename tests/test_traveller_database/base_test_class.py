import os

from traveller.service.traveller_database.traveller_database import TravellerDatabase

import paths_for_tests


class BaseTestClass:
    def setup_class(self):
        db_path = paths_for_tests.TEMPORARY_TRAVELLER_DATABASE
        try:
            os.remove(db_path)
        except OSError:
            pass
        try:
            print('About to create database: "{}"'.format({db_path}))
            self.db = TravellerDatabase(database_path=db_path)
        except IOError:
            print('Traveller database can not be created at path: {}'
                  .format(db_path))
            assert False

    def test_whole_sequence(self):
        pass

    def teardown_class(self):
        pass
