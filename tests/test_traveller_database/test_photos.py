import datetime
import pytest

from traveller.items.standalone.journey import Journey
from traveller.items.standalone.photo import Photo
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.common.types import TravellerException

from test_traveller_database.base_test_class import BaseTestClass

import paths_for_tests


class TestPhotos(BaseTestClass):
    """
    Stateful test, tests have to be executed in numerical order to work correctly!
    """

    def test_whole_sequence(self):
        self.add_empty_journey_with_only_name()
        self.add_photo_1()
        self.add_photo_2()
        self.update_photo_1()

    def add_empty_journey_with_only_name(self):
        journey1 = Journey(name='first one')
        self.db.add_journey(journey1)
        assert journey1.db_id == 2

    def add_photo_1(self):
        photo = Photo.create_from_file(
            file_path=paths_for_tests.IMG_1_PATH)
        self.db.add_item(photo)
        photos = self.db.get_all_items_of_type(item_type=Photo)
        assert len(photos) == 1
        assert photos[0].camera_model == 'Nexus 5'
        assert photos[0].photo_time == 1534159249
        assert photos[0].file_name == 'IMG_20180813_132048.jpg'
        assert photos[0].directory_name == 'img'
        assert photos[0].thumbnail is not None
        assert photos[0].associated_item_id is None
        assert photos[0].latitude == pytest.approx(45.959205555555556)
        assert photos[0].longitude == pytest.approx(9.163411111111111)

    def add_photo_2(self):
        photo = Photo.create_from_file(
            file_path=paths_for_tests.IMG_2_PATH)
        self.db.add_item(photo)
        photos = self.db.get_all_items_of_type(item_type=Photo)
        assert len(photos) == 2
        assert photos[1].file_name == 'IMG_20180809_140552.jpg'
        assert photos[1].directory_name == 'img2'

    def update_photo_1(self):
        photo = self.db.get_all_items_of_type(item_type=Photo)[0]
        photo.photo_time = 888
        self.db.update_item(photo)

        photos = self.db.get_all_items_of_type(item_type=Photo)
        assert photos[0].photo_time == 888


if __name__ == '__main__':
    # use it for debug
    test_class = TestPhotos()
    test_class.setup_class()

    test_class.test_whole_sequence()

