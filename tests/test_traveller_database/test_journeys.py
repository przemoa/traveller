import datetime
import pytest

from traveller.items.standalone.journey import Journey
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.common.types import TravellerException

from test_traveller_database.base_test_class import BaseTestClass


class TestJourney(BaseTestClass):
    """
    Stateful test, tests have to be executed in numerical order to work correctly!
    """

    def test_whole_sequence(self):
        self.add_empty_journey_with_only_name()
        self.add_journey_2()
        self.add_journey_3()
        self.add_journey_without_name()
        self.add_journey_with_same_name_and_date()
        self.add_journey_with_same_name_other_date()
        self.update_journey_2()
        self.delete_journey_2()


    def add_empty_journey_with_only_name(self):
        assert self._get_number_of_journeys() == 1  # 'Undefined' journey

        journey1 = Journey(name='first one')
        self.db.add_journey(journey1)
        assert journey1.db_id == 2
        assert self._get_number_of_journeys() == 2
        assert self._get_nth_journey(2).name == 'first one'

    def add_journey_2(self):
        country_1 = Country(iso_code='PL', name='Poland')
        country_2 = Country(iso_code='NE', name='Not Existing')
        admin_major = AdminRegionMajor(ascii_name='Lubusz')
        self.db.add_admin_region_major(
            admin_region_major=admin_major,
            country=country_1)

        journey2 = Journey(name='super journey')
        journey2.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
        journey2.set_end_time_from_datetime(datetime.datetime(2018, 11, 15, 21, 00))
        journey2.start_location_item_id = admin_major.item_id
        journey2.end_location_item_id = country_2.item_id

        self.db.add_journey(journey2)
        assert self._get_number_of_journeys() == 3
        assert self._get_nth_journey(2).name == 'first one'

        journey_2_read = self._get_nth_journey(3)
        assert journey_2_read.name == 'super journey'
        assert self._get_nth_journey(3).get_start_time_as_datetime() == datetime.datetime(2018, 11, 15, 18, 00)
        assert self._get_nth_journey(3).get_end_time_as_datetime() == datetime.datetime(2018, 11, 15, 21, 00)

        start_location_item_id = self._get_nth_journey(3).start_location_item_id
        start_location = self.db.get_item_by_item_id(start_location_item_id)
        assert isinstance(start_location, AdminRegionMajor)
        assert start_location.ascii_name == 'Lubusz'

    def add_journey_3(self):
        journey3 = Journey(name='other journey')
        journey3.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
        journey3.set_end_time_from_datetime(datetime.datetime(2018, 11, 15, 21, 00))

        self.db.add_journey(journey3)
        assert self._get_number_of_journeys() == 4

    def add_journey_without_name(self):
        assert self._get_number_of_journeys() == 4
        journey = Journey()
        with pytest.raises(ValueError):
            self.db.add_journey(journey)
        assert self._get_number_of_journeys() == 4

    def add_journey_with_same_name_and_date(self):
        assert self._get_number_of_journeys() == 4
        journey3 = Journey(name='other journey')
        journey3.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
        journey3.set_end_time_from_datetime(datetime.datetime(2018, 11, 15, 21, 00))

        with pytest.raises(TravellerException):
            self.db.add_journey(journey3)
        assert self._get_number_of_journeys() == 4

    def add_journey_with_same_name_other_date(self):
        assert self._get_number_of_journeys() == 4
        journey3 = Journey(name='other journey')
        journey3.set_start_time_from_datetime(datetime.datetime(2018, 11, 14, 18, 00))
        journey3.set_end_time_from_datetime(datetime.datetime(2018, 11, 15, 21, 00))

        self.db.add_journey(journey3)
        assert self._get_number_of_journeys() == 5

    def update_journey_2(self):
        assert self._get_number_of_journeys() == 5
        journey_2_read = self._get_nth_journey(3)
        assert journey_2_read.name == 'super journey'
        assert journey_2_read.get_start_time_as_datetime() == datetime.datetime(2018, 11, 15, 18, 00)

        journey_2_read.name = 'new name'
        journey_2_read.set_start_time_from_datetime(datetime.datetime(2017, 11, 15, 18, 00))
        self.db.update_item(journey_2_read)

        journey_2_read_again = self._get_nth_journey(3)
        assert journey_2_read_again.name == 'new name'
        assert journey_2_read_again.get_start_time_as_datetime() == journey_2_read.get_start_time_as_datetime()

    def delete_journey_2(self):
        pass

    def _get_number_of_journeys(self):
        journyes = self.db.get_journeys()
        return len(journyes)
    
    def _get_nth_journey(self, db_id):
        journyes = self.db.get_journeys()
        assert db_id <= len(journyes)
        return journyes[db_id-1]


if __name__ == '__main__':
    # use it for debug
    test_class = TestJourney()
    test_class.setup_class()

    test_class.test_whole_sequence()

