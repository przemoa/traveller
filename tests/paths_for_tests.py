import os


PROMPT_DATABASE_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    '..', 'src', 'traveller', 'prompts', 'data',
    'location_prompts.sqlite')

_TEST_FILES_DIRECTORY = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'test_files')

if not os.path.exists(_TEST_FILES_DIRECTORY):
    os.makedirs(_TEST_FILES_DIRECTORY)

TEMPORARY_TRAVELLER_DATABASE = os.path.join(
    _TEST_FILES_DIRECTORY,
    'temporary_traveller_database.sqlite')

IMG_1_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'test_files/img/IMG_20180813_132048.jpg')

IMG_2_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'test_files/img/img2/IMG_20180809_140552.jpg')
