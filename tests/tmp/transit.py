from traveller.items.item import Item


class TransitType:
    CAR = 'car'
    TRAIN = 'train'
    PLANE = 'plane'
    FOOT = 'on foot'
    BIKE = 'bike'
    BOAT = 'boat'
    BUS = 'bus'


class Transit(Item):
    """Transit - single transit (travel) between some places
    TODO:
    start, end, transit_type,

    Attributes:
        start_location (location):
        end_location (location):
        start_time
        end_time
    """

    def __init__(self):
        super().__init__()
