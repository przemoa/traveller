import datetime

from traveller.items.standalone.journey import Journey


def test_equal_yes():
    journey_1 = Journey(name='some journey')
    journey_1.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
    journey_2 = Journey(name='some journey')
    journey_2.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
    assert journey_1 == journey_2

    journey_2.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 13, 24))
    assert journey_1 == journey_2


def test_not_equal():
    journey_1 = Journey(name='some journey')
    journey_1.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
    journey_2 = Journey(name='some journey')
    journey_2.set_start_time_from_datetime(datetime.datetime(2018, 11, 14, 18, 00))
    assert journey_1 != journey_2

    journey_2.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 24))
    assert journey_1 == journey_2

    journey_2.name = 'other journey'
    assert journey_1 != journey_2


def test_get_description():
    journey_1 = Journey(name='jour1')
    print(journey_1.get_short_description())
    journey_1.set_start_time_from_datetime(datetime.datetime(2018, 11, 15, 18, 00))
    print(journey_1.get_short_description())
    journey_1.set_end_time_from_datetime(datetime.datetime(2018, 11, 16, 18, 00))
    print(journey_1.get_short_description())


if __name__ == '__main__':
    test_equal_yes()
    test_not_equal()
    test_get_description()
