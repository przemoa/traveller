from traveller.service.prompt_database.prompt_database import PromptDatabase
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor

import paths_for_tests


class TestPrompting:
    def setup_class(self):
        try:
            self.database = PromptDatabase(database_path=paths_for_tests.PROMPT_DATABASE_PATH)
        except IOError:
            print('Prompt database can not be opened at path: {}'
                  .format(paths_for_tests.PROMPT_DATABASE_PATH))
            assert False

    def teardown_class(self):
        pass

    def test_prompt_country(self):
        countries = self.database.get_country_prompts(contained_text='pl')
        print(countries)
        assert len(countries) == 1
        assert countries[0].name == 'Poland'

    def test_prompt_admin_regions_major(self):
        country = self.database.get_country_prompts('pl')[0]
        admin_regions_1_a = self.database.get_admin_region_major_prompts(contained_text="LUB")
        print(*admin_regions_1_a, sep='\n')
        assert len(admin_regions_1_a) == 3

        print()
        admin_regions_1_b = self.database.get_admin_region_major_prompts(
            contained_text="LUB",
            parent_location=country)
        print(*admin_regions_1_b, sep='\n')
        assert len(admin_regions_1_b) == 2

        print()
        admin_regions_1_c = self.database.get_admin_region_major_prompts(
            parent_location=country)
        print(*admin_regions_1_c, sep='\n')
        assert len(admin_regions_1_c) == 16

    def test_prompt_admin_regions_minor(self):
        country = self.database.get_country_prompts('pl')[0]
        admin_region_major = self.database.get_admin_region_major_prompts(contained_text="LUBUSZ")[0]

        print()
        admin_regions_2_a = self.database.get_admin_region_minor_prompts(
            contained_text="iel")
        print(*admin_regions_2_a, sep='\n')
        assert len(admin_regions_2_a) == 37

        print()
        admin_regions_2_b = self.database.get_admin_region_minor_prompts(
            contained_text="iel",
            parent_location=country)
        print(*admin_regions_2_b, sep='\n')
        assert len(admin_regions_2_b) == 14

        print()
        admin_regions_2_c = self.database.get_admin_region_minor_prompts(
            contained_text="iel",
            parent_location=admin_region_major)
        print(*admin_regions_2_c, sep='\n')
        assert len(admin_regions_2_c) == 3


    def test_prompt_admin_regions_minor(self):
        country = self.database.get_country_prompts('pl')[0]
        admin_region_major = self.database.get_admin_region_major_prompts(contained_text="LUBUSZ")[0]
        admin_regions_minor = self.database.get_admin_region_minor_prompts(contained_text="ielonog")[0]

        print()
        cities_a = self.database.get_city_prompts(contained_text='babi')
        print(*cities_a, sep='\n')
        assert len(cities_a) == 12

        print()
        cities_b = self.database.get_city_prompts(
            contained_text='babi',
            parent_location=country)
        print(*cities_b, sep='\n')
        assert len(cities_b) == 6

        print()
        cities_c = self.database.get_city_prompts(
            contained_text='babi',
            parent_location=admin_region_major)
        print(*cities_c, sep='\n')
        assert len(cities_c) == 1

        print()
        cities_d = self.database.get_city_prompts(
            contained_text='babi',
            parent_location=admin_regions_minor)
        print(*cities_d, sep='\n')
        assert len(cities_d) == 1

    def test_get_parent(self):
        country = self.database.get_country_prompts('pl')[0]
        admin_region_major = self.database.get_admin_region_major_prompts(contained_text="LUBUSZ")[0]
        admin_regions_minor = self.database.get_admin_region_minor_prompts(contained_text="ielonog")[0]
        citiy = self.database.get_city_prompts(contained_text='babimo')[0]

        parent = self.database.get_parent_location_for_child(Country, admin_region_major)
        assert parent.iso_code == 'PL'

        parent = self.database.get_parent_location_for_child(Country, admin_regions_minor)
        assert parent.iso_code == 'PL'

        parent = self.database.get_parent_location_for_child(Country, citiy)
        assert parent.iso_code == 'PL'

        parent = self.database.get_parent_location_for_child(AdminRegionMajor, admin_regions_minor)
        assert parent.ascii_name == 'Lubusz'

        parent = self.database.get_parent_location_for_child(AdminRegionMajor, citiy)
        assert parent.ascii_name == 'Lubusz'

        parent = self.database.get_parent_location_for_child(AdminRegionMinor, citiy)
        assert parent.ascii_name == 'Powiat zielonogorski'


if __name__ == '__main__':
    tp = TestPrompting()
    tp.setup_class()
    tp.test_get_parent()
