#!/usr/bin/env bash

set -e
set -x

SOURCE_DIR="$(dirname "$0")"


copy_source_code()
{
    rsync --delete --recursive --exclude="/global_components_creator/" $SOURCE_DIR/src/ src
}


setup_virtual_env()
{
    ENV_CONFIGURED__PATH="venv/bin/.env_configured"
    if [ -f "ENV_CONFIGURED__PATH" ]
    then
        printf "Activating virtual environment...\n"
        source venv/bin/activate
    else
        printf "Creating virtual environment...\n"
        python3 -m venv venv
        source venv/bin/activate

        pushd "$(dirname "$0")"

        printf "Installing requirements...\n"
        pip install --upgrade pip
        pip install --upgrade setuptools
        pip install -r "requirements.txt"
        pip install pyinstaller
        popd

        touch "$ENV_CONFIGURED__PATH"
    fi
}


freeze_to_executable()
{
    rm -rf dist
    mkdir dist

    cd src
    export PYTHONPATH=`pwd`:$PYTHONPATH
    cd clients/gui/
    ./misc/remove_mocs.sh

    printf "Creating mocs...\n"
    echo $PYTHONPATH
    python -c 'from clients.gui import gui_setup as gui_setup ; gui_setup.moc_ui_files()'

    printf "Removing old files...\n"
    rm -rf dist
    rm -rf build

    printf "Creating single executable with pyinstaller...\n"
    pyinstaller --onefile run_traveller_gui.py

    printf "Copy artifact..."
    cp dist/run_traveller_gui ../../../dist/traveller_gui
}


copy_source_code

setup_virtual_env

freeze_to_executable


printf "Done."
