# 0 Traveller contents

Travels and photos.
 - traveller: base package
 - clients:
   - gui
   - cli (console)
   - web 
 - glabal_data_creator: only used to create database with global data
 
Traveller uses database of locations to prompt user input (for countries and cities),
but name can be also typed manually.

 
# 1 Run from sources

## Setup environment
    cd traveller 
    python3 -m virtualenv venv
    . venv/bin/activate
    pip install -r requirements.txt
    
## Run in pycharm
 - mark src directory as source root
 - settings->project interpreter->choose local environment
 - e.g. run GUI: src/clients/gui/run_traveller_gui.py
 
## 2 Tests
### Run
 - ...
### Coverage report
 - ...

# 3 Supported versions
    Linux/Windows
    console and GUI version
    
# 4 Development notes
 - use Qt designer to edit ui files
 - ui files mocking is not necessary before each run, it can be done once

# 5. Obtain google maps API key:
 - https://www.iwdagency.com/help/dealer-store-locator/creation-of-server-api-keybrowser-api-key
 - https://elfsight.com/blog/2018/06/how-to-get-google-maps-api-key-guide/
 - https://github.com/googlemaps/google-maps-services-python
 - console.developers.google.com/apis/
 - place key in file google_map_api_key.py ( key = 'xxxxxxx' )
    
    
# 6. Build as debian package and install
 - ...
 - sudo apt install ./dist/xxx.deb
 - run from terminal 'traveller_gui' or choose from menu 

# 7. Freeze to executable manually:
 ```
 cd src
 export PYTHONPATH=`pwd`:$PYTHONPATH
 python -c 'from clients.gui import gui_setup as gui_setup ; gui_setup.moc_ui_files()'
 cd clients/gui/
 rm -rf dist
 rm -rf build
 pyinstaller --onefile run_traveller_gui.py
 ```
 
# TODO
coordinates for admin regions (as average from child cities)
add note, modification time to item
cache for prompt database

 license, setup.py, setup.cfg
 import traveller.common.consts as consts
 flake8 checks
 from typing import (Any, Optional, TypeVar, Callable, KeysView, Union,  # noqa
                    Iterable, List, Dict, Iterator, Coroutine, MutableSet)
where to put __init__.py
edit gui only from gui thread
jenkins                    

logging 
test coverage
                    
Checking what can item belogn to and what can belong to it, adding common widgets of belongings for each type of widget                    
                    
Easy optimizations:
in serializable objects, create lists and strings once for class, then just use them internally as const (or copy for externals objects requests)


