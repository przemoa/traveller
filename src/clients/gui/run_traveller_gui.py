from clients.gui import gui_setup as gui_setup
from clients.gui.common.package_config import package_config_release


def main():
    gui_setup.setup_logger()
    gui_setup.load_gui_config(package_config=package_config_release)
    gui_setup.open_or_create_traveller_database()
    gui_setup.run_traveller_gui()
    gui_setup.save_gui_config(package_config=package_config_release)


if __name__ == '__main__':
    main()
