from clients.gui import gui_setup as gui_setup
from clients.gui.common.package_config import package_config_devel


def main():
    gui_setup.moc_ui_files()
    gui_setup.setup_logger()
    gui_setup.load_gui_config(package_config=package_config_devel)

    gui_setup.create_temporary_traveller_database()
    gui_setup.run_traveller_gui()
    gui_setup.save_gui_config(package_config=package_config_devel)


if __name__ == '__main__':
    main()

