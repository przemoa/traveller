from PySide2 import QtWidgets


class ViewerWidget(QtWidgets.QWidget):
    """Base class for all widgets displaying item data, which can open another item by emitting signal
    """
    # signal: open item in browser
    # signal: open item in appropriate item_viewer widget
    # signal: open item in wikipedia widget

    def __init__(self):
        super().__init__()