import os

from PySide2.QtCore import Slot, Signal, QUrl
from PySide2 import QtWidgets
from PySide2.QtWebEngineWidgets import QWebEnginePage, QWebEngineView

from traveller.items.item import Item
from traveller.items.standalone.photo import Photo
from traveller.service.map.generate_map import generate_map
from traveller.items.visits.visit import Visit

from clients.gui.common.ui_owner import UiOwner
import clients.gui.common.engine as engine
from clients.gui.common.tab_widget import TabWidget

# use google java script api
# with one free visit for api key
# generate map to file for selected config (show places, cites, images (with icon whole time or after hovering pin)


class MapWidget(TabWidget):

    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()

        self._web_view = QWebEngineView(self)
        self.ui.verticalLayout_fow_web_view.addWidget(self._web_view)
        self._web_view.show()
        self._web_view.setUrl('')
        self.ui.splitter.setStretchFactor(0, 0)
        self.ui.splitter.setStretchFactor(1, 1)

        # self._generate_and_show_map()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.map.moc_map_widget import Ui_map_widget
        ui = Ui_map_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.pushButton_generate.clicked.connect(self._generate_and_show_map)

    @Slot()
    def _generate_and_show_map(self):
        visits = []
        photos = []

        if self.ui.checkBox_visits.isChecked():
            visits = engine.traveller_database.get_all_items_of_type(item_type=Visit)
        if self.ui.checkBox_photos.isChecked():
            photos = engine.traveller_database.get_all_items_of_type(item_type=Photo)

        output_dir = './generated_map'
        map_name = 'traveller_map.html'

        generate_map(visits=visits,
                     images=photos,
                     traveller_database=engine.traveller_database,
                     output_dir=output_dir,
                     map_name=map_name)
        absolute_path = os.path.abspath("./generated_map/traveller_map.html")
        url = QUrl.fromLocalFile(absolute_path)
        self._web_view.setUrl(url)


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    w = MapWidget(None, None)
    w.show()
    sys.exit(app.exec_())
