from PySide2 import QtWidgets

from clients.gui.common.ui_owner import UiOwner


class BrowserWidget(QtWidgets.QWidget, UiOwner):
    """Used to go through items like in browser.
    After opening each element (in browser) current will be closed and new one will be opened instead.
    Navigate (back, forward) buttons for navigation
    Will display inside this navigation buttons and one of item_viewers.
    Will catch signalas from active item_viewer, to change to next one after click

    """
    def __init__(self):
        pass