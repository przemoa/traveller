import copy

from PySide2 import QtWidgets, QtCore
from PySide2.QtCore import Slot, Signal, Qt
import PySide2
from PySide2.QtWidgets import QMessageBox

from traveller.common.types import TravellerException
from traveller.items.item import Item

from clients.gui.common.tab_widget import TabWidget
from clients.gui.tab_widgets.common_widgets.selectors.location.select_city_widget import SelectCityWidget
import clients.gui.common.engine as engine


class EditWidget(TabWidget):
    def __init__(self,
                 parent,
                 tab_id: str,
                 details_widget_class,
                 select_widget_class,
                 item_class):
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()

        self._item_details_widget = details_widget_class()
        self.ui.verticalLayout_details.addWidget(self._item_details_widget)

        self._select_item_widget = select_widget_class()
        self.ui.horizontalLayout_select.addWidget(self._select_item_widget)
        self._current_item = item_class()
        self._set_buttons_state(allow_delete=False, allow_update=False)
        self._item_details_widget.display_item(self._current_item)
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.edit.moc_edit_widget import Ui_edit_widget
        ui = Ui_edit_widget()
        ui.setupUi(self)
        return ui

    @Slot(Item)
    def selected_item_changed(self, item):
        self._current_item = item
        self._item_details_widget.display_item(self._current_item)
        allow_delete = bool(self._current_item.db_id is not None)
        self._set_buttons_state(allow_delete=allow_delete, allow_update=False)

    @Slot()
    def _item_edited(self):
        is_in_db = bool(self._current_item.db_id is not None)
        self._set_buttons_state(allow_delete=is_in_db, allow_update=is_in_db)

    def create_ui_connections(self):
        self._select_item_widget.signal_item_changed.connect(self.selected_item_changed)
        self.ui.pushButton_add_as_new.clicked.connect(self._add_as_new_item_clicked)
        self.ui.pushButton_delete_item.clicked.connect(self._delete_item_clicked)
        self.ui.pushButton_update_item.clicked.connect(self._update_item_clicked)
        self._item_details_widget.signal_item_edited.connect(self._item_edited)

    def _delete_item_clicked(self):
        assert self._current_item.db_id is not None
        if not self._check_if_allowed_to_modify():
            QMessageBox.warning(self, "Warning!", 'Can not delete item!')
            return

        engine.traveller_database.delete_item(self._current_item)
        self._current_item.db_id = None
        self._current_item.item_id = None
        self._select_item_widget.update_all_items()
        self._set_buttons_state(allow_delete=False, allow_update=False)

    def _update_item_clicked(self):
        assert self._current_item.db_id is not None
        if not self._check_if_allowed_to_modify():
            QMessageBox.warning(self, "Warning!", 'Can not update item!')
            return

        self._set_and_write_before_updating_item()
        engine.traveller_database.update_item(self._current_item)
        self._set_buttons_state(allow_delete=True, allow_update=False)
        self._select_item_widget.update_all_items(
            item_to_select_db_id=self._current_item.db_id)

    def _set_and_write_before_updating_item(self):
        pass

    def _check_if_allowed_to_modify(self):
        return True

    def _check_if_allowed_to_add(self):
        return True

    def _add_as_new_item_clicked(self):
        if not self._check_if_allowed_to_add():
            QMessageBox.warning(self, "Warning!", 'Can not add item (item not valid)!')
            return

        last_item = copy.deepcopy(self._current_item)
        self._current_item.db_id = None  # so it can be added to database
        self._current_item.item_id = None

        try:
            self._set_and_write_before_updating_item()
            engine.traveller_database.add_item(self._current_item)
            self._select_item_widget.update_all_items(
                item_to_select_db_id=self._current_item.db_id)
            self._set_buttons_state(allow_delete=True, allow_update=False)
        except TravellerException as e:
            self._current_item.db_id = last_item.db_id
            self._current_item.item_id = last_item.item_id
            QMessageBox.warning(self, "Warning!", 'Failed to add item ({}).'.format(str(e)))

    def _set_buttons_state(self, allow_delete, allow_update, allow_new=True):
        self.ui.pushButton_delete_item.setEnabled(allow_delete)
        self.ui.pushButton_update_item.setEnabled(allow_update)
        self.ui.pushButton_add_as_new.setEnabled(allow_new)

