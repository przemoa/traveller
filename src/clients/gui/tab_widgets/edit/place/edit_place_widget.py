from traveller.items.spots.locations.location_selection import LocationSelection
from traveller.items.spots.places.place import Place

from clients.gui.tab_widgets.edit.edit_widget import EditWidget
import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.place.place_details_widget import PlaceDetailsWidget
from clients.gui.tab_widgets.common_widgets.selectors.place.select_place_widget import SelectPlaceWidget


class EditPlaceWidget(EditWidget):
    def __init__(self, parent, tab_id: str):
        EditWidget.__init__(self,
                            parent=parent,
                            tab_id=tab_id,
                            details_widget_class=PlaceDetailsWidget,
                            select_widget_class=SelectPlaceWidget,
                            item_class=Place)

    def _check_if_allowed_to_modify(self):
        return True

    def _check_if_allowed_to_add(self):
        self._current_item = self._item_details_widget.get_edited_item()
        if not self._current_item.name:
            return False
        return True

    def _set_and_write_before_updating_item(self):
        self._current_item = self._item_details_widget.get_edited_item()
        self._current_item.parent_location_id = None
        current_selection = self._item_details_widget.get_currently_selected_selection()
        if isinstance(current_selection, LocationSelection):
            engine.traveller_database.add_selected_location_to_database_if_not_existing(current_selection)
            youngest_location = current_selection.get_youngest_selection()
            if youngest_location is not None:
                engine.traveller_database.get_item_id_for_item(youngest_location)
                self._current_item.parent_location_id = youngest_location.item_id
        if isinstance(current_selection, Place):
            engine.traveller_database.get_item_id_for_item(current_selection)
            assert current_selection.item_id is not None
            self._current_item.parent_location_id = current_selection.item_id


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = EditPlaceWidget(parent=None, tab_id=None)
    w.show()
    sys.exit(app.exec_())
