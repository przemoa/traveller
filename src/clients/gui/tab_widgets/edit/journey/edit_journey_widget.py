from traveller.items.standalone.journey import Journey

from clients.gui.tab_widgets.edit.edit_widget import EditWidget
import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.journey.journey_details_widget import JourneyDetailsWidget
from clients.gui.tab_widgets.common_widgets.selectors.journey.select_journey_widget import SelectJourneyWidget


class EditJourneyWidget(EditWidget):
    def __init__(self, parent, tab_id: str):
        EditWidget.__init__(self,
                            parent=parent,
                            tab_id=tab_id,
                            details_widget_class=JourneyDetailsWidget,
                            select_widget_class=SelectJourneyWidget,
                            item_class=Journey)

    def _check_if_allowed_to_modify(self):
        if self._current_item.name == Journey.UNSPECIFIED_JOURNEY_NAME:
            return False
        return True

    def _check_if_allowed_to_add(self):
        if self._current_item.name == '':
            return False
        if self._current_item.name == Journey.UNSPECIFIED_JOURNEY_NAME:
            return False
        return True

    def _set_and_write_before_updating_item(self):
        start_location_selection, end_location_selection = \
            self._item_details_widget.get_start_and_end_location_selections()
        engine.traveller_database.add_selected_location_to_database_if_not_existing(start_location_selection)
        engine.traveller_database.add_selected_location_to_database_if_not_existing(end_location_selection)
        yongest_start_location = start_location_selection.get_youngest_selection()
        yongest_end_location = end_location_selection.get_youngest_selection()
        if yongest_start_location is not None:
            engine.traveller_database.get_item_id_for_item(yongest_start_location)
            self._current_item.start_location_item_id = yongest_start_location.item_id
        else:
            self._current_item.start_location_item_id = None
        if yongest_end_location is not None:
            engine.traveller_database.get_item_id_for_item(yongest_end_location)
            self._current_item.end_location_item_id = yongest_end_location.item_id
        else:
            self._current_item.end_location_item_id = None


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = EditJourneyWidget(parent=None, tab_id=None)
    w.show()
    sys.exit(app.exec_())
