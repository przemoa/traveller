import collections
from typing import List
import os

from PySide2.QtWidgets import QMessageBox, QTableWidget, QTableWidgetItem, QFileDialog
from PySide2 import QtWidgets, QtGui
from PySide2.QtCore import Slot, Signal

from traveller.items.standalone.photo import Photo
from traveller.common.types import ItemAlreadyInDatabaseError

from clients.gui.common.tab_widget import TabWidget
import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.photo.single_photo_details_widget import SinglePhotoDetailsWidget
from clients.gui.tab_widgets.common_widgets.subviewers.photo.multiple_photos_details_widget import MultiplePhotosDetailsWidget


class DirsTableWidget(QTableWidget):
    signal_selected_dirs_changed = Signal()

    def __init__(self, parent):
        QTableWidget.__init__(self, parent)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.setColumnCount(2)
        self.setRowCount(0)

        self.setHorizontalHeaderItem(0, QTableWidgetItem('Directory'))
        self.setHorizontalHeaderItem(1, QTableWidgetItem('Count'))
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self._dirs_list = []
        self._dirs_counts = []
        self._all_photos = []
        self.create_ui_connections()

    def create_ui_connections(self):
        self.itemSelectionChanged.connect(self._selection_changed)

    @Slot()
    def _selection_changed(self):
        self.signal_selected_dirs_changed.emit()

    def get_photos_in_selected_dirs(self):
        photos_in_selected_dirs = []
        selection_model = self.selectionModel()
        if not selection_model.hasSelection():
            return photos_in_selected_dirs
        required_dirs = []
        required_elements = 0
        for row in selection_model.selectedRows():
            dir_name = self._dirs_list[row.row()]
            photos_count_in_dir = self._dirs_counts[row.row()]
            required_elements += photos_count_in_dir
            required_dirs.append(dir_name)

        photos_in_selected_dirs = [None] * required_elements
        i = 0
        for photo in self._all_photos:
            if photo.directory_name in required_dirs:
                assert i < required_elements
                photos_in_selected_dirs[i] = photo
                i += 1
        return photos_in_selected_dirs

    def display_dirs_for_photos(self, all_photos: List[Photo]):
        self._dirs_list.clear()
        self._dirs_counts.clear()
        self._all_photos = all_photos
        dirs_dict = collections.Counter([photo.directory_name for photo in all_photos])
        self.setRowCount(len(dirs_dict))
        i = 0
        for dir_name, count in dirs_dict.items():
            self.setItem(i, 0, QTableWidgetItem(dir_name))
            self.setItem(i, 1, QTableWidgetItem(str(count)))
            self._dirs_list.append(dir_name)
            self._dirs_counts.append(count)
            i += 1
        self.resizeColumnsToContents()


class PhotosTableWidget(QTableWidget):
    signal_selected_photos_changed = Signal()

    def __init__(self, parent):
        QTableWidget.__init__(self, parent)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.setColumnCount(3)
        self.setRowCount(0)

        self.setHorizontalHeaderItem(0, QTableWidgetItem('Directory'))
        self.setHorizontalHeaderItem(1, QTableWidgetItem('File'))
        self.setHorizontalHeaderItem(2, QTableWidgetItem('Date'))

        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self._all_photos = []
        self.create_ui_connections()

    def create_ui_connections(self):
        self.itemSelectionChanged.connect(self._selection_changed)

    @Slot()
    def _selection_changed(self):
        self.signal_selected_photos_changed.emit()

    def get_selected_photos(self):
        selected_photos = []
        selection_model = self.selectionModel()
        if not selection_model.hasSelection():
            return selected_photos
        if len(selection_model.selectedRows()) == len(self._all_photos):
            return self._all_photos

        for row in selection_model.selectedRows():
            selected_photos.append(self._all_photos[row.row()])
        return selected_photos

    def display_photos(self, all_photos: List[Photo]):
        self._all_photos = all_photos
        self.setRowCount(len(all_photos))
        for i, photo in enumerate(all_photos):
            self.setItem(i, 0, QTableWidgetItem(photo.directory_name))
            self.setItem(i, 1, QTableWidgetItem(photo.file_name))
            try:
                self.setItem(i, 2, QTableWidgetItem(photo.get_photo_time_as_full_string()))
            except:
                pass
        self.resizeColumnsToContents()


class PhotosEditWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()
        self._displayed_photos = []
        self._single_image_detail_widget = SinglePhotoDetailsWidget(parent=self)
        self._multiple_images_detail_widget = MultiplePhotosDetailsWidget(parent=self)
        self.ui.verticalLayout_image_details.addWidget(self._single_image_detail_widget)
        self.ui.verticalLayout_image_details.addWidget(self._multiple_images_detail_widget)
        self._dirs_table_widget = DirsTableWidget(parent=self)
        self.ui.verticalLayout_dirs_table.addWidget(self._dirs_table_widget)
        self._all_photos = None  # type: List[Photo]
        self._refresh_all_photos()
        self._set_details_visibility_and_buttons_for_number_of_selected_photos(0)

        self.ui.splitter.setStretchFactor(0, 0.1)
        self.ui.splitter.setStretchFactor(1, 0.1)
        self.ui.splitter.setStretchFactor(2, 1)

        self._photos_table_widget = PhotosTableWidget(parent=self)
        self.ui.verticalLayout_photos_table.addWidget(self._photos_table_widget)
        self.create_ui_connections()

    def _refresh_all_photos(self):
        self._all_photos = engine.traveller_database.get_all_items_of_type(item_type=Photo)
        self._dirs_table_widget.display_dirs_for_photos(all_photos=self._all_photos)

    def create_ui_connections(self):
        self._dirs_table_widget.signal_selected_dirs_changed.connect(self._photos_in_dirs_changed)
        self._photos_table_widget.signal_selected_photos_changed.connect(self._photos_to_display_changed)
        self.ui.pushButton_add.clicked.connect(self._add_photos_clicked)
        self.ui.pushButton_update.clicked.connect(self._update_photos)
        self.ui.pushButton_delete.clicked.connect(self._delete_photos)
        self._single_image_detail_widget.signal_item_edited.connect(self._displayed_image_edited)
        self._multiple_images_detail_widget.signal_item_edited.connect(self._displayed_image_edited)

    @Slot()
    def _displayed_image_edited(self):
        self.ui.pushButton_update.setEnabled(True)

    @Slot()
    def _update_photos(self):
        if len(self._displayed_photos) == 1:
            photos = [self._single_image_detail_widget.get_edited_item()]
        elif len(self._displayed_photos) > 1:
            photos = self._multiple_images_detail_widget.get_edited_items()

        for photo in photos:
            engine.traveller_database.update_item(item=photo)
        self._refresh_all_photos()
        self._photos_in_dirs_changed()  # to refresh all from database

    def _delete_photos(self):
        for photo in self._displayed_photos:
            engine.traveller_database.delete_item(item=photo)
        self._refresh_all_photos()

    @Slot()
    def _photos_in_dirs_changed(self):
        images_in_dirs = self._dirs_table_widget.get_photos_in_selected_dirs()
        self._photos_table_widget.display_photos(images_in_dirs)
        self._photos_to_display_changed()

    @Slot()
    def _photos_to_display_changed(self):
        self.ui.pushButton_update.setEnabled(False)
        photos_to_display = self._photos_table_widget.get_selected_photos()
        self._set_details_visibility_and_buttons_for_number_of_selected_photos(len(photos_to_display))
        self._displayed_photos = photos_to_display

        if len(self._displayed_photos) == 1:
            self._single_image_detail_widget.display_item(self._displayed_photos[0])
        if len(self._displayed_photos) > 1:
            self._multiple_images_detail_widget.display_multiple_items(self._displayed_photos)

    def _set_details_visibility_and_buttons_for_number_of_selected_photos(self, number_of_photos):
        self.ui.pushButton_delete.setVisible(bool(number_of_photos))
        self.ui.pushButton_update.setVisible(bool(number_of_photos))
        if number_of_photos == 0:
            self._single_image_detail_widget.setVisible(False)
            self._multiple_images_detail_widget.setVisible(False)
        elif number_of_photos == 1:
            self._single_image_detail_widget.setVisible(True)
            self._multiple_images_detail_widget.setVisible(False)
        else:
            self._single_image_detail_widget.setVisible(False)
            self._multiple_images_detail_widget.setVisible(True)

    def generate_ui(self):
        from clients.gui.tab_widgets.edit.photos.moc_edit_photos_widget import Ui_edit_photos_widget
        ui = Ui_edit_photos_widget()
        ui.setupUi(self)
        return ui

    def select_photos_to_add_from_dialog(self):
        images_to_add = QFileDialog.getOpenFileNames(self,
                                                 "Select Photos",
                                                 os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop'),
                                                 'Images (*.jpg *.jpeg);; All files (*.*)')
        return images_to_add[0]

    @Slot()
    def _add_photos_clicked(self):
        images_to_add = self.select_photos_to_add_from_dialog()
        items_already_in_database = []
        for image_path in images_to_add:
            try:
                photo = Photo.create_from_file(image_path)
                engine.traveller_database.add_item(photo)
            except ItemAlreadyInDatabaseError:
                items_already_in_database.append(image_path)
        if len(items_already_in_database):
            skipped_photos_txt = '\n'.join(
                ['{}. {}'.format(i, file_path) for i, file_path in enumerate(items_already_in_database[:10])])
            if len(items_already_in_database) > 10:
                skipped_photos_txt += '\n and {} more...'.format(len(items_already_in_database) - 10)
            QMessageBox.warning(self, "Warning!", 'Can not add some photos to database, '
                                                  'because photo with same name and date already exists. '
                                                  'Following photos were not added:\n'
                                                  + skipped_photos_txt)
        if len(images_to_add):
            self._refresh_all_photos()

    # add delete update
    # display associated item
    # select associated item


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    path = '/home/przemek/Projects/traveller/tests/test_files/img/IMG_20180812_131416.jpg'


    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = PhotosEditWidget(parent=None, tab_id='pw')
    w.show()
    sys.exit(app.exec_())
