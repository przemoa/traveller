from PySide2.QtWidgets import QMessageBox

from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.items.spots.locations.location_selection import LocationSelection

from clients.gui.common.tab_widget import TabWidget
from clients.gui.tab_widgets.common_widgets.selectors.location.select_city_widget import SelectCityWidget
import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.coordinates.edit_coordinates_widget import EditCoordinatesWidget

class CurrentCityType:
    NEW, EXISTING_IN_DATABSE, EXISTING_IN_PROMPTS_DATABSE, NONE = range(4)


# class EditCityWidget(TabWidget, EditWidget):
class EditCityWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        # EditWidget.__init__(self)  # TODO consider adjusting to EditWidget interface
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()
        self._select_city_widget = SelectCityWidget(self)
        self.ui.horizontalLayout_for_city_selecion.addWidget(self._select_city_widget)
        self._selected_locations = self._select_city_widget.get_selected_locations()  # type: LocationSelection
        self._coordinate_widget = EditCoordinatesWidget(parent=self)
        self.ui.verticalLayout_coordinates.addWidget(self._coordinate_widget)
        self._current_city_type = CurrentCityType.NONE
        self._update_city_details()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.edit.city.moc_edit_city_widget import Ui_edit_city_widget
        ui = Ui_edit_city_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self._select_city_widget.signal_selected_city_changed.connect(self._selected_city_changed)
        self.ui.pushButton_add_city.clicked.connect(self._add_city_clicked)
        self.ui.pushButton_delete_city.clicked.connect(self._delete_city_clicked)
        self.ui.pushButton_update_city.clicked.connect(self._update_city_clicked)

    def _selected_city_changed(self):
        self._selected_locations = self._select_city_widget.get_selected_locations()
        self._update_city_details()

    def _update_city_details(self):
        # was_new_city_inputed = isinstance(self._selected_locations['city'], str) \
        #                        and self._selected_locations['city'] != ''
        if not self._selected_locations.city:
            self._current_city_type = CurrentCityType.NONE
            self._reset_city_details_edits()
        elif isinstance(self._selected_locations.city, str):
            # only text selected, not known city
            if self._current_city_type != CurrentCityType.NEW:
                self._reset_city_details_edits()
            self._current_city_type = CurrentCityType.NEW
            self.ui.lineEdit_ascii_name.setText(self._selected_locations.city)
        else:
            selected_city = self._selected_locations.city
            if selected_city.db_id is not None:
                self._current_city_type = CurrentCityType.EXISTING_IN_DATABSE
            else:
                self._current_city_type = CurrentCityType.EXISTING_IN_PROMPTS_DATABSE
            self._display_city_details_edits(selected_city)

        self.set_edits_state_for_current_city_type()

    def _reset_city_details_edits(self):
        self.ui.lineEdit_ascii_name.setText('')
        self.ui.lineEdit_non_ascii_name.setText('')
        self.ui.spinBox_population.setValue(-1)
        self._coordinate_widget.reset_item_details_edits()

    def _display_city_details_edits(self, city: City):
        self.ui.lineEdit_ascii_name.setText(city.ascii_name)
        if city.non_ascii_name:
            self.ui.lineEdit_non_ascii_name.setText(city.non_ascii_name)
        else:
            self.ui.lineEdit_non_ascii_name.setText('')

        if city.population is not None:
            self.ui.spinBox_population.setValue(city.population)
        else:
            self.ui.spinBox_population.setValue(-1)
        self._coordinate_widget.display_item_details_edits(item=city)

    def set_edits_state_for_current_city_type(self):
        self.ui.lineEdit_ascii_name.setEnabled(
            not bool(self._current_city_type == CurrentCityType.NEW))

        if self._current_city_type == CurrentCityType.NEW:
            self.ui.lineEdit_ascii_name.setEnabled(False)

        self.ui.pushButton_delete_city.setEnabled(
            bool(self._current_city_type == CurrentCityType.EXISTING_IN_DATABSE))

        self.ui.pushButton_update_city.setEnabled(
            bool(self._current_city_type == CurrentCityType.EXISTING_IN_DATABSE))

        self.ui.pushButton_add_city.setEnabled(
            bool(self._current_city_type == CurrentCityType.NEW)
            or bool(self._current_city_type == CurrentCityType.EXISTING_IN_PROMPTS_DATABSE))

        self.ui.widget_edit_city_details.setEnabled(
            bool(self._current_city_type == CurrentCityType.NEW)
            or bool(self._current_city_type == CurrentCityType.EXISTING_IN_DATABSE))

    def _add_city_clicked(self):
        error_txt = ''

        if isinstance(self._selected_locations.country, Country):
            country = self._selected_locations.country
        elif self._selected_locations.country == '':
            error_txt = 'Select country first!'
        else:
            country = Country(name=self._selected_locations.country)

        if isinstance(self._selected_locations.admin_major, AdminRegionMajor):
            admin_major = self._selected_locations.admin_major
        elif self._selected_locations.admin_major == '':
            error_txt = 'Select admin region major first!'
        else:
            admin_major = AdminRegionMajor(ascii_name=self._selected_locations.admin_major)

        if isinstance(self._selected_locations.admin_minor, AdminRegionMinor):
            admin_minor = self._selected_locations.admin_minor
        elif self._selected_locations.admin_minor == '':
            error_txt = 'Select admin region minor first!'
        else:
            admin_minor = AdminRegionMinor(ascii_name=self._selected_locations.admin_minor)

        if error_txt:
            QMessageBox.warning(self, "Warning!", error_txt)
            return
        city = self._get_current_city()
        engine.traveller_database.add_city(
            city=city,
            admin_region_major=admin_major,
            admin_region_minor=admin_minor,
            country=country
        )
        self._select_city_widget.update_all()
        self._selected_city_changed()

    def _update_city_from_details_edits(self, city_to_update):
        ascii_name = self.ui.lineEdit_ascii_name.text()
        non_ascii_name = self.ui.lineEdit_non_ascii_name.text()
        population = self.ui.spinBox_population.value()
        if population == -1:
            population = None

        self._coordinate_widget.update_item_from_ui(item=city_to_update)
        city_to_update.ascii_name = ascii_name
        city_to_update.non_ascii_name = non_ascii_name
        city_to_update.population = population

    def _get_current_city(self):
        error_txt = ''
        assert self._current_city_type != CurrentCityType.NONE
        if self._current_city_type == CurrentCityType.NEW:
            city = City()
            self._update_city_from_details_edits(city)
        elif self._current_city_type == CurrentCityType.EXISTING_IN_PROMPTS_DATABSE:
            city = self._selected_locations.city
        elif self._current_city_type == CurrentCityType.EXISTING_IN_DATABSE:
            city = self._selected_locations.city
            self._update_city_from_details_edits(city)
        else:
            assert False
        return city

    def _delete_city_clicked(self):
        city = self._get_current_city()
        assert city.db_id is not None
        engine.traveller_database.delete_item(city)
        self._select_city_widget.update_all()
        self._selected_city_changed()

    def _update_city_clicked(self):
        city = self._get_current_city()
        assert city.db_id is not None
        engine.traveller_database.update_item(city)
        self._select_city_widget.update_all()
        self._selected_city_changed()


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = EditCityWidget(None, None)
    w.show()
    sys.exit(app.exec_())
