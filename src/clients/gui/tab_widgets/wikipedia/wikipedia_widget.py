"""
Widget displaying wikipedia informations for current item (in separate tab)
"""

from PySide2 import QtWidgets

from clients.gui.common.ui_owner import UiOwner


class WikipediaWidget(QtWidgets.QWidget, UiOwner):
    def __init__(self):
        pass