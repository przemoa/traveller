import re

from PySide2 import QtWidgets

from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.location_selection import LocationSelection, SelectionType

from clients.gui.common.ui_owner import UiOwner
from clients.gui.tab_widgets.common_widgets.selectors.location.select_city_widget import SelectCityWidget
import clients.gui.common.engine as engine


class SelectExistingRegionOrCityDialog(UiOwner, QtWidgets.QDialog):
    # TODO Add map with cities, allow to pick, autoshowing from text

    def __init__(self,
                 selection_type: SelectionType = SelectionType.ANY,
                 selected_location: LocationSelection = LocationSelection()):
        QtWidgets.QDialog.__init__(self)
        UiOwner.__init__(self)
        self.ui = self.generate_ui()
        self._selection_type = selection_type
        self._select_city_widget = SelectCityWidget(parent=self)
        self.ui.horizontalLayout_for_city_selecion.addWidget(self._select_city_widget)
        self._selected_locations = self._select_city_widget.get_selected_locations()  # type: LocationSelection
        self._update_selection_details()
        self._youngest_selection = None
        self.deselection_clicked = False
        self.was_canceled = False
        self._set_title(self._selection_type)
        self.create_ui_connections()
        self._select_city_widget.set_current_location_selection(selected_location=selected_location)

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.selectors.location.dialog.moc_select_existing_region_or_city_dialog \
            import Ui_select_existing_region_or_city_dialog
        ui = Ui_select_existing_region_or_city_dialog()
        ui.setupUi(self)
        return ui

    def _set_title(self, selection_type):
        if selection_type == SelectionType.ANY:
            txt = 'Select Region or City Dialog'
        elif selection_type == SelectionType.ONLY_CITY:
            txt = 'Select City Dialog'
        else:
            raise NotImplementedError
        self.setWindowTitle(txt)

    def create_ui_connections(self):
        self._select_city_widget.signal_selected_location_changed.connect(self._selected_location_changed)
        self.ui.pushButton_select.clicked.connect(self._select_clicked)
        self.ui.pushButton_deselect.clicked.connect(self._deselect_clicked)
        self.ui.pushButton_cancel.clicked.connect(self._cancel_clicked)

    def _selected_location_changed(self):
        self._selected_locations = self._select_city_widget.get_selected_locations()
        self._update_selection_details()

    def _update_selection_details(self):
        self._youngest_selection = self._selected_locations.get_youngest_selection(selection_type=self._selection_type)

        if isinstance(self._youngest_selection, City):
            self._display_city_details_edits(self._youngest_selection)
        else:
            self._reset_city_details_edits()

        if self._youngest_selection:
            self._display_common_details(self._youngest_selection)

        self._update_select_button()

    def _update_select_button(self):
        if self._youngest_selection is None:
            self.ui.pushButton_select.setEnabled(False)
            self.ui.pushButton_select.setText('Select ...')
        else:
            self.ui.pushButton_select.setEnabled(True)
            selected_class_name = self._youngest_selection.__class__.__name__
            selected_type_string = re.sub(r"(\w)([A-Z])", r"\1 \2", selected_class_name)
            self.ui.pushButton_select.setText('Select {}'.format(selected_type_string))

    def _display_common_details(self, selection):
        if hasattr(selection, 'non_ascii_name') and selection.non_ascii_name:
            self.ui.lineEdit_non_ascii_name.setText(selection.non_ascii_name)
        else:
            self.ui.lineEdit_non_ascii_name.setText('')

    def _reset_city_details_edits(self):
        self.ui.spinBox_population.setValue(-1)
        self.ui.doubleSpinBox_latitude.setValue(0)
        self.ui.doubleSpinBox_longitude.setValue(0)
        self.ui.spinBox_population.setEnabled(False)
        self.ui.doubleSpinBox_latitude.setEnabled(False)
        self.ui.doubleSpinBox_longitude.setEnabled(False)

    def _display_city_details_edits(self, city: City):
        if city.population is not None:
            self.ui.spinBox_population.setValue(city.population)
            self.ui.spinBox_population.setEnabled(True)
        else:
            self.ui.spinBox_population.setValue(-1)
            self.ui.spinBox_population.setEnabled(False)
        if city.latitude is not None and city.longitude is not None:
            self.ui.doubleSpinBox_latitude.setValue(city.latitude)
            self.ui.doubleSpinBox_longitude.setValue(city.longitude)
            self.ui.doubleSpinBox_latitude.setEnabled(True)
            self.ui.doubleSpinBox_longitude.setEnabled(True)
        else:
            self.ui.doubleSpinBox_latitude.setValue(0)
            self.ui.doubleSpinBox_longitude.setValue(0)
            self.ui.doubleSpinBox_latitude.setEnabled(False)
            self.ui.doubleSpinBox_longitude.setEnabled(False)

    def _select_clicked(self):
        assert self._youngest_selection is not None
        self.close()

    def _deselect_clicked(self):
        self.deselection_clicked = True
        self.close()

    def _cancel_clicked(self):
        self.was_canceled = True
        self.close()

    def get_current_selection(self):
        return self._selected_locations


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = SelectExistingRegionOrCityDialog(selection_type = SelectionType.ONLY_CITY)
    # w = SelectExistingRegionOrCityDialog()
    w.show()
    sys.exit(app.exec_())
