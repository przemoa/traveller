from PySide2 import QtCore
from PySide2.QtCore import Slot, Signal
from PySide2.QtGui import QPalette
from PySide2 import QtWidgets

from traveller.traveller_engine import TravellerEngine, PromptSourceType
from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.items.spots.locations.location_selection import LocationSelection

import clients.gui.common.engine as engine
from clients.gui.common.ui_owner import UiOwner
import clients.gui.common.colors as colors


"""
actions for each combobox selection:
A) text_changed_by_user
B) update_list_for_existing_parent (or None parent, if none) [remember last update state, skip if not necessary]
C) validate current text in list
D) set_from_child (called if child is valid) = enter text and validate

    Country     |   Admin 1     |   Admin 2     |   City
    ___________________________________________________________     
    1) A
    2) C            
                    3) B & C
                                    4) B & C
                                                    5) B & C        
    ___________________________________________________________
                    1) A
                    2) C
    3) D     
                    4) B            
                                    5) B & C
                                                    6) B & C
    ___________________________________________________________     
                                    1) A
                                    2) C
    3) D
                    4) B & D
                                    5) B
                                                    6) B & C
    ___________________________________________________________
                                                    1) A
                                                    2) C
    3) D
                    4) B & D
                                    5) B & D 
                                                    6) B    
    ___________________________________________________________     


"""


class SelectCityWidget(UiOwner, QtWidgets.QWidget):
    """
    TODO: consider adjusting to select_item_widget interface
    And rename to select location
    """

    INFINITE_NUMBER_OF_PROMPTS = 999999
    signal_selected_city_changed = Signal()
    signal_selected_location_changed = Signal()

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent)
        UiOwner.__init__(self)
        self.ui = self.generate_ui()
        self._prompt_type = self._determine_selected_prompt_type()
        self._last_selected_city = ''

        self._country_selection = self._prepare_country_selection()
        self._admin_region_major_selection = self._prepare_admin_region_major_selection()
        self._admin_region_minor_selection = self._prepare_admin_region_minor_selection()
        self._city_selection = self._prepare_city_selection()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.selectors.location.moc_select_city_widget import Ui_select_city_widget
        ui = Ui_select_city_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.comboBox_countries.editTextChanged.connect(self._country_text_changed)
        self.ui.comboBox_admin_regions_major.editTextChanged.connect(self._admin_region_major_text_changed)
        self.ui.comboBox_admin_regions_minor.editTextChanged.connect(self._admin_region_minor_text_changed)
        self.ui.comboBox_cities.editTextChanged.connect(self._city_text_changed)

        self.ui.radioButton_prompt_database.clicked.connect(self.update_all)
        self.ui.radioButton_own_database.clicked.connect(self.update_all)
        self.ui.radioButton_both_databases.clicked.connect(self.update_all)

    def set_current_location_selection(self, selected_location):
        all_location_types = [
            ('country', self._country_selection['combobox']),
            ('admin_major', self._admin_region_major_selection['combobox']),
            ('admin_minor', self._admin_region_minor_selection['combobox']),
            ('city', self._city_selection['combobox']),
        ]
        for location_type, selection_combobox in all_location_types:
            if selected_location[location_type]:
                selection_text = selected_location[location_type].get_short_description()
            else:
                selection_text = ''
            selection_combobox.lineEdit().setText(selection_text)

    @staticmethod
    def _set_items_for_combobox_and_completer(selection, items):
        selection['items'] = items
        combobox = selection['combobox']
        txt_entries = [item.get_short_description() for item in items]
        selection['txt_entries'] = txt_entries
        combobox.clear()
        combobox.addItems([''])
        combobox.addItems(txt_entries)
        completer = QtWidgets.QCompleter(txt_entries)
        selection['completer'] = completer
        combobox.setCompleter(completer)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)


    @staticmethod
    def _create_common_selection_and_setup(combobox, items, info_label):
        selection = {
            'items': None,
            'txt_entries': [],
            'completer': None,
            'combobox': combobox,
            'selected': None,
            'info_label': info_label,
            'last_update_config': (None, None),
        }
        SelectCityWidget._set_items_for_combobox_and_completer(selection, items)
        combobox.setStyleSheet("QComboBox {background: white;}")
        return selection

    @staticmethod
    def _validate_selection_text(selection):
        text = selection['combobox'].lineEdit().text()
        palette = QPalette()
        try:
            item_index = selection['txt_entries'].index(text)
            selected_item = selection['items'][item_index]
            if isinstance(selected_item, Country):
                if selected_item.continent_code:
                    additional_info = 'continent ' + selected_item.continent_code
                else:
                    additional_info = ''
            else:
                non_ascii_name = selected_item.non_ascii_name
                if non_ascii_name != text:
                    additional_info = selected_item.non_ascii_name
                else:
                    additional_info = ''
            selection['info_label'].setText(additional_info)
            selection['selected'] = selected_item
            if selected_item.db_id:
                palette.setColor(QPalette.Base, colors.LineEditColors.EXISTING_ITEM_COLOR)
            else:
                palette.setColor(QPalette.Base, colors.LineEditColors.ITEM_FROM_PROMPT_COLOR)
        except ValueError:
            if text != '':
                selection['info_label'].setText('New item...')
            else:
                selection['info_label'].setText('')
            selection['selected'] = None
            palette.setColor(QPalette.Base, colors.LineEditColors.NEW_ITEM_COLOR)
        selection['combobox'].setPalette(palette)

    def get_youngest_parent_for_type(self, location_type):
        parent_is_new = False
        parent = None
        if self._country_selection['selected'] is not None:
            parent_is_new = False
            parent = self._country_selection['selected']
        elif self._country_selection['combobox'].lineEdit().text() != '':
            parent_is_new = True
            parent = None
        if location_type == AdminRegionMajor:
            return parent_is_new, parent

        if self._admin_region_major_selection['selected'] is not None:
            parent_is_new = False
            parent = self._admin_region_major_selection['selected']
        elif self._admin_region_major_selection['combobox'].lineEdit().text() != '':
            parent_is_new = True
            parent = None
        if location_type == AdminRegionMinor:
            return parent_is_new, parent

        if self._admin_region_minor_selection['selected'] is not None:
            parent_is_new = False
            parent = self._admin_region_minor_selection['selected']
        elif self._admin_region_minor_selection['combobox'].lineEdit().text() != '':
            parent_is_new = True
            parent = None
        return parent_is_new, parent

    def _selected_location_changed(self):
        self.signal_selected_location_changed.emit()
        self._check_if_selected_city_changed_and_emit()

    #country
    def _prepare_country_selection(self):
        combobox = self.ui.comboBox_countries
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=engine.engine.get_country_prompts(
                limit=self.INFINITE_NUMBER_OF_PROMPTS,
                prompt_type=self._prompt_type),
            info_label=self.ui.label_selected_country_info)
        return selection

    @Slot(str)
    def _country_text_changed(self, text):
        selection = self._country_selection
        self._validate_selection_text(selection=selection)

        self._update_admin_regions_major()
        self._validate_selection_text(self._admin_region_major_selection)
        self._update_admin_regions_minor()
        self._validate_selection_text(self._admin_region_minor_selection)
        self._update_city()
        self._validate_selection_text(self._city_selection)
        self._selected_location_changed()

    def _set_country_from_child(self, child):
        selection = self._country_selection
        selection['combobox'].blockSignals(True)
        for i, country in enumerate(selection['items']):
            if country.iso_code == child.country_code:
                selection['combobox'].setCurrentIndex(i + 1)  # add empty line
                self._validate_selection_text(selection=selection)
                break
        selection['combobox'].blockSignals(False)

    #admin major
    def _prepare_admin_region_major_selection(self):
        combobox = self.ui.comboBox_admin_regions_major
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=engine.engine.get_admin_region_major_prompts(
                limit=self.INFINITE_NUMBER_OF_PROMPTS,
                prompt_type=self._prompt_type),
            info_label=self.ui.label_selected_admin_major_info)
        return selection

    def _update_admin_regions_major(self):
        selection = self._admin_region_major_selection
        selection['combobox'].blockSignals(True)
        text = selection['combobox'].lineEdit().text()
        parent_is_new, parent = self.get_youngest_parent_for_type(AdminRegionMajor)
        if selection['last_update_config'] != (parent_is_new, parent):
            if parent_is_new:
                items = []
            else:
                items = engine.engine.get_admin_region_major_prompts(
                    parent_location=parent,
                    limit=self.INFINITE_NUMBER_OF_PROMPTS,
                    prompt_type=self._prompt_type)
            selection['last_update_config'] = parent_is_new, parent
            self._set_items_for_combobox_and_completer(selection, items)
        selection['combobox'].lineEdit().setText(text)
        selection['combobox'].blockSignals(False)

    @Slot()
    def _admin_region_major_text_changed(self):
        selection = self._admin_region_major_selection
        self._validate_selection_text(selection=selection)
        if selection['selected'] is not None:
            self._set_country_from_child(selection['selected'])
            self._update_admin_regions_major()
        self._update_admin_regions_minor()
        self._validate_selection_text(self._admin_region_minor_selection)
        self._update_city()
        self._validate_selection_text(self._city_selection)
        self._selected_location_changed()

    def _set_admin_region_major_from_child(self, child_location):
        admin_major = engine.engine.get_parent_location_for_child(
            parent_type=AdminRegionMajor,
            child_location=child_location)
        if admin_major is not None:
            selection = self._admin_region_major_selection
            selection['combobox'].blockSignals(True)
            selection['combobox'].lineEdit().setText(admin_major.get_short_description())
            self._validate_selection_text(selection=selection)
            selection['combobox'].blockSignals(False)
        self._set_country_from_child(child_location)

    # admin region minor
    def _prepare_admin_region_minor_selection(self):
        combobox = self.ui.comboBox_admin_regions_minor
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=engine.engine.get_admin_region_minor_prompts(
                limit=self.INFINITE_NUMBER_OF_PROMPTS,
                prompt_type=self._prompt_type),
            info_label=self.ui.label_selected_admin_minor_info)
        return selection

    def _update_admin_regions_minor(self):
        selection = self._admin_region_minor_selection
        selection['combobox'].blockSignals(True)

        text = selection['combobox'].lineEdit().text()
        parent_is_new, parent = self.get_youngest_parent_for_type(AdminRegionMinor)
        if selection['last_update_config'] != (parent_is_new, parent):
            if parent_is_new:
                items = []
            else:
                items = engine.engine.get_admin_region_minor_prompts(
                    parent_location=parent,
                    limit=self.INFINITE_NUMBER_OF_PROMPTS,
                    prompt_type=self._prompt_type)
            selection['last_update_config'] = parent_is_new, parent
            self._set_items_for_combobox_and_completer(selection, items)
        selection['combobox'].lineEdit().setText(text)
        selection['combobox'].blockSignals(False)

    @Slot()
    def _admin_region_minor_text_changed(self):
        selection = self._admin_region_minor_selection
        text = selection['combobox'].lineEdit().text()
        self._validate_selection_text(selection=selection)
        if selection['selected'] is not None:
            self._set_admin_region_major_from_child(selection['selected'])
            self._update_admin_regions_minor()
        self._update_city()
        self._validate_selection_text(self._city_selection)
        self._selected_location_changed()

    def _set_admin_region_minor_from_child(self, child: City):
        admin_minor = engine.engine.get_parent_location_for_child(
                parent_type=AdminRegionMinor,
                child_location=child)
        if admin_minor is not None:
            selection = self._admin_region_minor_selection
            selection['combobox'].blockSignals(True)
            selection['combobox'].lineEdit().setText(admin_minor.get_short_description())
            self._validate_selection_text(selection=selection)
            selection['combobox'].blockSignals(False)
        self._set_admin_region_major_from_child(child)

    # city
    def _prepare_city_selection(self):
        combobox = self.ui.comboBox_cities
        items = []
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=items,
            info_label=self.ui.label_selected_city_info)
        return selection

    def _update_city(self):
        selection = self._city_selection
        selection['combobox'].blockSignals(True)
        text = selection['combobox'].lineEdit().text()
        parent_is_new, parent = self.get_youngest_parent_for_type(City)
        if selection['last_update_config'] != (parent_is_new, parent):
            if parent_is_new or not parent:
                items = []
            else:
                items = engine.engine.get_city_prompts(
                    parent_location=parent,
                    limit=self.INFINITE_NUMBER_OF_PROMPTS,
                    prompt_type=self._prompt_type)
            selection['last_update_config'] = parent_is_new, parent
            self._set_items_for_combobox_and_completer(selection, items)
        selection['combobox'].lineEdit().setText(text)
        selection['combobox'].blockSignals(False)

    @Slot()
    def _city_text_changed(self):
        selection = self._city_selection
        self._validate_selection_text(selection=selection)
        text = selection['combobox'].lineEdit().text()
        if selection['selected'] is not None:
            self._set_admin_region_minor_from_child(selection['selected'])
            self._update_city()
        self._selected_location_changed()

    # general
    def _check_if_selected_city_changed_and_emit(self):
        if self._city_selection['selected'] is not None:
            selected_city = self._city_selection['selected']
        else:
            selected_city = self._city_selection['combobox'].lineEdit().text()
        if self._last_selected_city != selected_city:
            self._last_selected_city = selected_city
            self.signal_selected_city_changed.emit()
        else:
            pass  # same as previously

    def get_selected_locations(self) -> LocationSelection:
        """
        # TODO change to class LocationSelection
        :return: disct with keys like below and values of specific selection type or string.
        e.g. selection['city'] can be City object or string with name of not existing location
        """
        selected_locations = LocationSelection()
        all_selection_types = [
            ('country', self._country_selection),
            ('admin_major', self._admin_region_major_selection),
            ('admin_minor', self._admin_region_minor_selection),
            ('city', self._city_selection),
        ]
        for selection_type in all_selection_types:
            selection_name = selection_type[0]
            selection_dict = selection_type[1]
            if selection_dict['selected']:
                selected_locations[selection_name] = selection_dict['selected']
            else:
                selected_locations[selection_name] = selection_dict['combobox'].lineEdit().text()
        return selected_locations

    def _determine_selected_prompt_type(self):
        if self.ui.radioButton_own_database.isChecked():
            return PromptSourceType.TRAVELLER_DATABASE
        elif self.ui.radioButton_prompt_database.isChecked():
            return PromptSourceType.PROMPT_DATABASE
        else:
            return PromptSourceType.ALL_AVAILABLE

    def update_all(self):
        self._prompt_type = self._determine_selected_prompt_type()

        self._admin_region_major_selection['last_update_config'] = None, None
        self._admin_region_minor_selection['last_update_config'] = None, None
        self._city_selection['last_update_config'] = None, None

        current_country_text = self.ui.comboBox_countries.lineEdit().text()
        self._country_selection['combobox'].blockSignals(True)
        self._country_selection = self._prepare_country_selection()
        self._country_selection['combobox'].blockSignals(False)
        self.ui.comboBox_countries.lineEdit().setText(current_country_text)

        self._admin_region_major_text_changed()
        self._admin_region_minor_text_changed()
        self._city_text_changed()


def main():
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup(create_mocs=True)
    app = QtWidgets.QApplication(sys.argv)
    import clients.gui.common.engine as engine
    w = SelectCityWidget(parent=None)
    w.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
