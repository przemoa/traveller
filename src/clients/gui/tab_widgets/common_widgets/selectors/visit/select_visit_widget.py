from traveller.items.visits.visit import Visit

import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.selectors.select_item_widget import SelectItemWidget


class SelectVisitWidget(SelectItemWidget):
    def __init__(self):
        SelectItemWidget.__init__(self, item_class=Visit)
        self.ui = self.generate_ui()

        self._display_all_items()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.selectors.visit.moc_select_visit_widget \
            import Ui_select_visit_widget
        ui = Ui_select_visit_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        SelectItemWidget.create_ui_connections(self)


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    # w = JourneyDetailsWidget(editability=Editability.NOT_EDITABLE)
    w = SelectVisitWidget()
    w.show()
    sys.exit(app.exec_())
