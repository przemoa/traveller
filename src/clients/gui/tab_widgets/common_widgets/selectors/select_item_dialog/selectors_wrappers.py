from typing import Union, List
from traveller.items.spots.locations.location_selection import LocationSelection
from traveller.items.item import Item
from traveller.items.standalone.journey import Journey
from traveller.items.visits.visit import Visit


def select_location_or_none_from_dialog(selected_location: LocationSelection):
    from clients.gui.tab_widgets.common_widgets.selectors.location.dialog.select_existing_region_or_city_dialog import \
        SelectExistingRegionOrCityDialog
    """
    # TODO adjust to select_item_from_dialog interface and merge with it?
    :param selected_location: Location selected at start
    :return: selected location or None if canceled
    """
    select_dialog = SelectExistingRegionOrCityDialog(selected_location=selected_location)
    select_dialog.exec_()
    if select_dialog.deselection_clicked:
        selected_location = LocationSelection()
    elif select_dialog.was_canceled:
        selected_location = None
    else:
        selected_location = select_dialog.get_current_selection()
    return selected_location


def select_item_from_dialog(item_class: Union[List[type], type], currently_selected_item: Item = None):
    from clients.gui.tab_widgets.common_widgets.selectors.select_item_dialog.select_any_item_dialog import \
        SelectAnyItemDialog
    allowed_item_types = item_class if isinstance(item_class, list) else [item_class]
    select_dialog = SelectAnyItemDialog(
        allowed_item_types=allowed_item_types,
        selected_at_start_item=currently_selected_item)
    select_dialog.exec_()

    if select_dialog.was_canceled:
        selected_item = currently_selected_item
    else:
        selected_item = select_dialog.get_selected_item()
    return selected_item


def select_item_for_photo(currently_selected_item: Item = None):
    return select_item_from_dialog(item_class=[Visit, Journey], currently_selected_item=currently_selected_item)


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    select_item_from_dialog(item_class=Journey)
    sys.exit(app.exec_())

