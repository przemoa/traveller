from typing import List

from PySide2 import QtWidgets
from PySide2.QtCore import Slot

from traveller.items.item import Item
from traveller.items.standalone.journey import Journey
from traveller.items.visits.visit import Visit

from clients.gui.tab_widgets.common_widgets.selectors.select_item_widget import SelectItemWidget
from clients.gui.tab_widgets.common_widgets.selectors.journey.select_journey_widget import SelectJourneyWidget
from clients.gui.tab_widgets.common_widgets.subviewers.journey.journey_details_widget import JourneyDetailsWidget
from clients.gui.tab_widgets.common_widgets.selectors.visit.select_visit_widget import SelectVisitWidget
from clients.gui.tab_widgets.common_widgets.subviewers.visit.visit_details_widget import VisitDetailsWidget
from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import ItemDetailsWidget, Editability
from clients.gui.common.ui_owner import UiOwner

# TODO implement more
ALL_ALLOWED_TYPES_WIDGETS = {
    Journey: {'select': SelectJourneyWidget, 'display': JourneyDetailsWidget},
    Visit: {'select': SelectVisitWidget, 'display': VisitDetailsWidget},
}


class SelectAnyItemDialog(UiOwner, QtWidgets.QDialog):
    # TODO hide widgets instead deleting them (and later load cached widgets instead of creating new ones)

    def __init__(self,
                 allowed_item_types: List,
                 selected_at_start_item: Item = None):
        QtWidgets.QDialog.__init__(self)
        UiOwner.__init__(self)
        self.ui = self.generate_ui()
        self.was_canceled = False
        self._selected_item = selected_at_start_item
        for item_type in allowed_item_types:
            assert item_type in ALL_ALLOWED_TYPES_WIDGETS
        self._allowed_item_types = allowed_item_types
        self._currently_selected_item_type = None
        if len(allowed_item_types) == 1:
            self._currently_selected_item_type = allowed_item_types[0]
        elif selected_at_start_item is not None:
            assert selected_at_start_item.__class__ in ALL_ALLOWED_TYPES_WIDGETS
            self._currently_selected_item_type = selected_at_start_item.__class__
        self._current_select_item_widget = None  # type: SelectItemWidget
        self._current_item_details_widget = None  # type: ItemDetailsWidget
        self._items_in_item_type_combobox = self._prepare_allowed_type_combobox()
        self._set_title()
        self._set_new_allowed_item_type(self._currently_selected_item_type)
        self.create_ui_connections()

    def _prepare_allowed_type_combobox(self):
        if len(self._allowed_item_types) == 1:
            self.ui.widget_select_item_type.setVisible(False)
            return
        items_in_item_type_combobox = [None] + self._allowed_item_types
        self.ui.comboBox_item_type.clear()
        self.ui.comboBox_item_type.addItems([''])
        txt_entries = [item_type.__name__ for item_type in self._allowed_item_types]
        self.ui.comboBox_item_type.addItems(txt_entries)
        if self._currently_selected_item_type is not None:
            index = items_in_item_type_combobox.index(self._currently_selected_item_type)
            self.ui.comboBox_item_type.setCurrentIndex(index)
        return items_in_item_type_combobox

    def _set_new_allowed_item_type(self, item_type):
        # update widgets and show, remove old ones
        self._currently_selected_item_type = item_type
        if self._current_select_item_widget is not None:
            self.ui.horizontalLayout_for_item_selection.removeWidget(self._current_select_item_widget)
            self._current_select_item_widget.deleteLater()
            self._current_select_item_widget = None
        if self._current_item_details_widget is not None:
            self.ui.horizontalLayout_widget_for_selected_item_details.removeWidget(self._current_item_details_widget)
            self._current_item_details_widget.deleteLater()
            self._current_item_details_widget = None

        if item_type is not None:
            assert item_type in ALL_ALLOWED_TYPES_WIDGETS
            self._current_select_item_widget = ALL_ALLOWED_TYPES_WIDGETS[item_type]['select']()
            self._current_select_item_widget.signal_item_changed.connect(self._selected_item_changed)
            self._current_item_details_widget = ALL_ALLOWED_TYPES_WIDGETS[item_type]['display'](editability=Editability.NOT_EDITABLE)

            self.ui.horizontalLayout_for_item_selection.addWidget(self._current_select_item_widget)
            self.ui.horizontalLayout_widget_for_selected_item_details.addWidget(self._current_item_details_widget)

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.selectors.select_item_dialog.moc_select_item_dialog import Ui_select_any_item_dialog
        ui = Ui_select_any_item_dialog()
        ui.setupUi(self)
        return ui

    def _set_title(self):
        if len(self._allowed_item_types) == 1:
            txt = 'Select {}...'.format(self._allowed_item_types[0].__name__)
        else:
            txt = 'Select Item...'
        self.setWindowTitle(txt)

    @Slot(Item)
    def _selected_item_changed(self, item):
        self._selected_item = item
        if self._current_item_details_widget is not None:
            self._current_item_details_widget.display_item(item)

    def get_selected_item(self) -> Item:
        return self._selected_item

    @Slot(int)
    def _selected_index_in_combobox_changed(self, index):
        new_item_type = self._items_in_item_type_combobox[index]
        self._selected_item = None
        self._set_new_allowed_item_type(new_item_type)

    def create_ui_connections(self):
        self.ui.pushButton_select.clicked.connect(self._select_clicked)
        self.ui.pushButton_deselect.clicked.connect(self._deselect_clicked)
        self.ui.pushButton_cancel.clicked.connect(self._cancel_clicked)
        self.ui.comboBox_item_type.currentIndexChanged.connect(self._selected_index_in_combobox_changed)

    def _select_clicked(self):
        self.close()

    def _deselect_clicked(self):
        self._selected_item = None
        self.close()

    def _cancel_clicked(self):
        self.was_canceled = True
        self.close()


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = SelectAnyItemDialog(allowed_item_types=[Journey])
    # w = SelectAnyItemDialog(allowed_item_types=[Journey, Visit])
    w.show()
    sys.exit(app.exec_())
