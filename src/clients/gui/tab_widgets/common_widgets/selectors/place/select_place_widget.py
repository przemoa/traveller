from traveller.items.spots.places.place import Place

import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.selectors.select_item_widget import SelectItemWidget


class SelectPlaceWidget(SelectItemWidget):
    def __init__(self):
        SelectItemWidget.__init__(self, item_class=Place)
        self.ui = self.generate_ui()

        self._display_all_items()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.selectors.place.moc_select_place_widget import Ui_select_place_widget
        ui = Ui_select_place_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        SelectItemWidget.create_ui_connections(self)


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    # w = JourneyDetailsWidget(editability=Editability.NOT_EDITABLE)
    w = SelectPlaceWidget()
    w.show()
    sys.exit(app.exec_())
