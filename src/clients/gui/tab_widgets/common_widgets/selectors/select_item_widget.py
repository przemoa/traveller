from PySide2.QtCore import Slot, Signal
from PySide2 import QtWidgets

from traveller.items.item import Item

from clients.gui.common.ui_owner import UiOwner
import clients.gui.common.engine as engine


class SelectItemWidget(UiOwner, QtWidgets.QWidget):
    """
    Base class for item selectors
    """
    signal_item_changed = Signal(Item)

    def __init__(self, item_class):
        QtWidgets.QWidget.__init__(self)
        UiOwner.__init__(self)
        self._item_class = item_class
        self._items_in_combobox = [None]
        self._selected_item = item_class()

    def get_class_of_item(self):
        return self._item_class

    def _display_all_items(self):
        items = engine.traveller_database.get_all_items_of_type(item_type=self._item_class)
        self._items_in_combobox = [None] + items
        self.ui.comboBox_all_items.clear()
        self.ui.comboBox_all_items.addItems([''])
        txt_entries = [item.get_short_description_with_engine(traveller_database=engine.traveller_database) for item in items]
        self.ui.comboBox_all_items.addItems(txt_entries)

    def update_all_items(self, item_to_select_db_id=None):
        self.ui.comboBox_all_items.blockSignals(True)
        self._display_all_items()
        index_to_select = 0
        for i, item in enumerate(self._items_in_combobox):
            if item is None:
                continue
            if item.db_id == item_to_select_db_id:
                index_to_select = i
                break
        self.ui.comboBox_all_items.setCurrentIndex(index_to_select)
        self.ui.comboBox_all_items.blockSignals(False)

    @Slot(int)
    def _selected_index_in_combobox_changed(self, index):
        if index == 0:
            self._selected_item = self._item_class()
        else:
            self._selected_item = self._items_in_combobox[index]
            engine.traveller_database.get_item_id_for_item(self._selected_item)
        self.signal_item_changed.emit(self._selected_item)

    def create_ui_connections(self):
        self.ui.comboBox_all_items.currentIndexChanged.connect(self._selected_index_in_combobox_changed)

    def set_selected_item(self, item: Item):
        raise NotImplementedError
