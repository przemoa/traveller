import copy
from typing import List

from PySide2.QtCore import Slot, Signal

from traveller.items.standalone.photo import Photo
from traveller.items.item import Item

from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import ItemDetailsWidget, Editability
from clients.gui.util import gui
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.image.display_image_widget import DisplayMultipleImagesWidget
import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.photo.common import get_associated_item_description, set_associate_item_id_from_dialog


class MultiplePhotosDetailsWidget(ItemDetailsWidget):
    def __init__(self, parent, editability: Editability = Editability.EDITABLE):
        ItemDetailsWidget.__init__(self, editability)
        self.ui = self.generate_ui()
        self._display_image_widget = DisplayMultipleImagesWidget(parent=self)
        self.ui.verticalLayout_for_thumbnail.addWidget(self._display_image_widget)
        self._prepare_ui()

        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.photo.moc_multiple_photos_details_widget \
            import Ui_multiple_photos_details_widget
        ui = Ui_multiple_photos_details_widget()
        ui.setupUi(self)
        return ui

    def _prepare_ui(self):
        self.ui.splitter_thumbnail_info.setStretchFactor(0, 1)
        self.ui.splitter_thumbnail_info.setStretchFactor(1, 0)
        gui.set_editability_for_edit(self.ui.dateTimeEdit_eldest, False)
        gui.set_editability_for_edit(self.ui.dateTimeEdit_youngest, False)
        gui.set_editability_for_edit(self.ui.lineEdit_camera, False)
        gui.set_editability_for_edit(self.ui.lineEdit_original_dir, False)
        gui.prepare_datetime_widget(self.ui.dateTimeEdit_eldest)
        gui.prepare_datetime_widget(self.ui.dateTimeEdit_youngest)

    def create_ui_connections(self):
        self.ui.pushButton_select_item.clicked.connect(self._select_associated_item_clicked)

    def _set_edits_signals_enabled(self, enable):
        pass

    def display_multiple_items(self, items: List[Photo]):
        self._current_item = copy.deepcopy(items)
        assert len(items) > 0
        self._display_image_widget.display_multiple_images(items)
        self._set_edits_signals_enabled(False)
        dirs = set()
        cameras = set()
        associated_items_id = set()

        eldest_time = None
        youngest_time = None
        for photo in items:
            if photo.photo_time is not None:
                eldest_time = photo.photo_time
                youngest_time = photo.photo_time
                break

        for photo in items:
            if photo.photo_time is None:
                continue

            dirs.add(photo.directory_name)
            cameras.add(photo.camera_model)
            associated_items_id.add(photo.associated_item_id)
            eldest_time = min(eldest_time, photo.photo_time)
            youngest_time = max(youngest_time, photo.photo_time)

        if None in cameras:
            cameras.remove(None)
            cameras.add('Unknown')

        associated_items_str = []
        for associated_item_id in associated_items_id:
            associated_items_str.append(get_associated_item_description(associated_item_id))

        ENTRIES_LIMIT = 5

        def join_strings(entries) -> str:
            txt = ', '.join(list(entries)[:ENTRIES_LIMIT])
            if len(entries) > ENTRIES_LIMIT:
                txt += ', ...'
            return txt

        dirs_txt = join_strings(dirs)
        cameras_txt = join_strings(cameras)
        associated_items_txt = join_strings(associated_items_str)

        self.ui.lineEdit_original_dir.setText(dirs_txt)
        self.ui.lineEdit_camera.setText(cameras_txt)
        gui.set_date_time_for_widget(
            qdatetime_widget=self.ui.dateTimeEdit_youngest,
            unix_time=youngest_time)
        gui.set_date_time_for_widget(
            qdatetime_widget=self.ui.dateTimeEdit_eldest,
            unix_time=eldest_time)
        self.ui.label_belongs_to_item.setText(associated_items_txt)
        self._set_edits_signals_enabled(True)

    @Slot()
    def _select_associated_item_clicked(self):
        first_item = self._current_item[0]
        set_associate_item_id_from_dialog(first_item)
        txt = get_associated_item_description(first_item.associated_item_id)
        self.ui.label_belongs_to_item.setText(txt)
        for item in self._current_item:
            item.associated_item_id = first_item.associated_item_id
        self.signal_item_edited.emit()

    def get_edited_items(self) -> List[Item]:
        return self._current_item


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = MultiplePhotosDetailsWidget(parent=None)
    w.show()
    import clients.gui.common.engine as engine
    photos = engine.traveller_database.get_all_items_of_type(Photo)
    w.display_multiple_items(photos)
    sys.exit(app.exec_())

