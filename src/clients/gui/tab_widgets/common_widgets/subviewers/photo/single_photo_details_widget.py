import copy

from PySide2.QtCore import Slot, Signal

from traveller.items.standalone.photo import Photo
from traveller.items.item import Item

from clients.gui.tab_widgets.common_widgets.subviewers.common_members.coordinates.edit_coordinates_widget import EditCoordinatesWidget
from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import ItemDetailsWidget, Editability
from clients.gui.util import gui
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.image.display_image_widget import DisplaySingleImageWidget
from clients.gui.tab_widgets.common_widgets.subviewers.photo.common import get_associated_item_description, set_associate_item_id_from_dialog


class SinglePhotoDetailsWidget(ItemDetailsWidget):
    def __init__(self, parent, editability: Editability = Editability.EDITABLE):
        ItemDetailsWidget.__init__(self, editability)
        self.ui = self.generate_ui()
        self._coordinate_widget = EditCoordinatesWidget(parent=self)
        self.ui.verticalLayout_coordinates.addWidget(self._coordinate_widget)
        self._display_image_widget = DisplaySingleImageWidget(parent=self)
        self.ui.verticalLayout_for_thumbnail.addWidget(self._display_image_widget)
        self._prepare_ui()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.photo.moc_single_photo_details_widget import Ui_single_poto_details_widget
        ui = Ui_single_poto_details_widget()
        ui.setupUi(self)
        return ui

    def _prepare_ui(self):
        self.ui.splitter_thumbnail_info.setStretchFactor(0, 1)
        self.ui.splitter_thumbnail_info.setStretchFactor(1, 0)
        gui.set_editability_for_edit(self.ui.dateTimeEdit_time_from_file, False)
        gui.set_editability_for_edit(self.ui.lineEdit_file_name, False)
        gui.set_editability_for_edit(self.ui.lineEdit_original_dir, False)
        gui.prepare_datetime_widget(self.ui.dateTimeEdit_time_from_file)
        gui.prepare_datetime_widget(self.ui.dateTimeEdit_photo_time)

    def create_ui_connections(self):
        self.ui.dateTimeEdit_photo_time.dateTimeChanged.connect(self.signal_item_edited)
        self.ui.lineEdit_camera.textChanged.connect(self.signal_item_edited)
        self._coordinate_widget.signal_item_edited.connect(self.signal_item_edited)
        self.ui.pushButton_select_item.clicked.connect(self._select_associated_item_clicked)

    def _set_edits_signals_enabled(self, enable):
        self.ui.dateTimeEdit_photo_time.blockSignals(enable)
        self.ui.lineEdit_camera.blockSignals(enable)
        self._coordinate_widget.set_edits_signals_enabled(enable)

    def display_item(self, item: Photo):
        self._current_item = copy.deepcopy(item)
        self._set_edits_signals_enabled(False)
        self._coordinate_widget.display_item_details_edits(item=item)
        self.ui.lineEdit_file_name.setText(item.file_name)
        self.ui.lineEdit_original_dir.setText(item.directory_name)
        self.ui.lineEdit_camera.setText(item.camera_model)
        gui.set_date_time_for_widget(
            qdatetime_widget=self.ui.dateTimeEdit_photo_time,
            unix_time=item.photo_time)
        gui.set_date_time_for_widget(
            qdatetime_widget=self.ui.dateTimeEdit_time_from_file,
            unix_time=item.photo_time_from_metadata)
        self._display_associated_item_description(item.associated_item_id)
        self._display_image_widget.display_image(item.thumbnail)
        self._set_edits_signals_enabled(True)

    def _display_associated_item_description(self, associated_item_id):
        txt = get_associated_item_description(associated_item_id)
        self.ui.label_belongs_to_item.setText(txt)

    @Slot()
    def _select_associated_item_clicked(self):
        set_associate_item_id_from_dialog(self._current_item)
        self._display_associated_item_description(self._current_item.associated_item_id)
        self.signal_item_edited.emit()

    def get_edited_item(self) -> Item:
        self._coordinate_widget.update_item_from_ui(self._current_item)
        self._current_item.camera_model = self.ui.lineEdit_camera.text()
        self._current_item.photo_time = self.ui.dateTimeEdit_photo_time.dateTime().toTime_t()
        return self._current_item


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = SinglePhotoDetailsWidget(parent=None)
    w.show()
    photo = Photo.create_from_file(file_path='/home/przemek/Projects/traveller/tests/test_files/img/IMG_20180812_131416.jpg')
    w.display_item(photo)
    sys.exit(app.exec_())

