import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.selectors.select_item_dialog import selectors_wrappers


def get_associated_item_description(associated_item_id) -> str:
    if associated_item_id is not None:
        associated_item = engine.traveller_database.get_item_by_item_id(item_id=associated_item_id)
        txt = associated_item.__class__.__name__ + ': ' + \
              associated_item.get_short_description_with_engine(traveller_database=engine.traveller_database)
    else:
        txt = '-'
    return txt


def set_associate_item_id_from_dialog(item):
    if item.associated_item_id is not None:
        currently_associated_item = engine.traveller_database.get_item_by_item_id(
            item_id=item.associated_item_id)
    else:
        currently_associated_item = None
    new_associated_item = selectors_wrappers.select_item_for_photo(currently_selected_item=currently_associated_item)
    if new_associated_item is None:
        item.associated_item_id = None
    else:
        item.associated_item_id = new_associated_item.item_id
