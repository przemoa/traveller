from PySide2.QtCore import Slot, Signal

from traveller.items.visits.visit import Visit
from traveller.items.visits.visitable_items import VisitedItemType
from traveller.items.visits import visitable_items
from traveller.items.spots.locations.location_selection import LocationSelection
from traveller.items.standalone.journey import Journey

import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import ItemDetailsWidget, Editability
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.start_end_time_viewer import StartEndTimeViewer
from clients.gui.tab_widgets.common_widgets.selectors.select_item_dialog import selectors_wrappers
from clients.gui.util import gui


class VisitDetailsWidget(ItemDetailsWidget, StartEndTimeViewer):

    def __init__(self, editability: Editability = Editability.EDITABLE):
        ItemDetailsWidget.__init__(self, editability)
        StartEndTimeViewer.__init__(self)
        self.ui = self.generate_ui()
        self._current_item = Visit()
        self._set_editability(self._editability)
        self._current_journey = None

        self._allowed_for_selection_visited_item_type = None  # type: VisitedItemType  # None is not valid, will be overwritten soon in next function
        self._location_selection = LocationSelection()  # never None. If not selected, _location_selection.get_youngest_selection will be None
        self._place_selection = None  # Place item if selected, None otherwise
        self._visited_item_selected = False

        self._populate_visited_item_type_combobox()
        self.create_ui_connections()
        self._visited_item_type_changed(0)
        self._set_visit_name()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.visit.moc_visit_details_widget \
            import Ui_visit_details_widget
        ui = Ui_visit_details_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.pushButton_select_location.clicked.connect(self._select_location_clicked)
        self.ui.comboBox_item_type.currentIndexChanged.connect(self._visited_item_type_changed)
        self.ui.pushButton_select_journey.clicked.connect(self._select_journey_clicked)
        StartEndTimeViewer.create_ui_connections(self)
        self.signal_item_edited.connect(self._set_visit_name)
        self.ui.textEdit_note.textChanged.connect(self._note_changed)

    def _populate_visited_item_type_combobox(self):
        for item in VisitedItemType.ALL_VISIT_TYPES:
            self.ui.comboBox_item_type.addItem(item)

    def _set_item_type_combobox_for_type(self, visited_item_type):
        index = VisitedItemType.ALL_VISIT_TYPES.index(visited_item_type)
        self.ui.comboBox_item_type.setCurrentIndex(index)

    def is_visited_item_selected(self) -> bool:
        return bool(self._visited_item_selected is not None)

    @Slot()
    def _select_journey_clicked(self):
        self._current_journey = selectors_wrappers.select_item_from_dialog(
            item_class=Journey,
            currently_selected_item=self._current_journey)
        if self._current_journey is not None:
            self._current_item.journey_db_id = self._current_journey.db_id
        else:
            self._current_item.journey_db_id = None
        self._display_journey_info()
        self.signal_item_edited.emit()

    @Slot(int)
    def _visited_item_type_changed(self, index):
        visited_item_type = VisitedItemType.ALL_VISIT_TYPES[index]
        self._set_item_type_combobox_for_type(visited_item_type)
        select_location = bool(visited_item_type == VisitedItemType.LOCATION)
        self.ui.widget_location.setEnabled(select_location)
        self.ui.widget_place.setEnabled(not select_location)
        self._allowed_for_selection_visited_item_type = visited_item_type
        self._display_place_selection()
        self._display_location_selection
        if visited_item_type == VisitedItemType.LOCATION:
            self._visited_item_selected = bool(self._location_selection.get_youngest_selection() is not None)
        elif visited_item_type == VisitedItemType.PLACE:
            self._visited_item_selected = bool(self._place_selection is not None)
        else:
            raise NotImplementedError
        self._set_visit_name()

    def _display_location_selection(self):
        if self._allowed_for_selection_visited_item_type == VisitedItemType.LOCATION:
            location_description = self._location_selection.get_short_description_text_or_empty_for_field()
        else:
            location_description = '-'
        self.ui.label_location.setText(location_description)

    def _display_place_selection(self):
        if self._allowed_for_selection_visited_item_type == VisitedItemType.PLACE:
            place_description = 'TODO'
            # place_description = self._place_selection.get_short_description()
        else:
            place_description = '-'
        self.ui.label_place.setText(place_description)

    @Slot()
    def _select_location_clicked(self):
        selected_location_or_none = selectors_wrappers.select_location_or_none_from_dialog(selected_location=self._location_selection)  # type: LocationSelection
        if selected_location_or_none is not None:
            self._location_selection = selected_location_or_none
            if self._location_selection.get_youngest_selection() is not None:
                self._visited_item_selected = True
            else:
                self._visited_item_selected = False
            self._display_location_selection()
            self.signal_item_edited.emit()
            self._set_visit_name()

    @Slot()
    def _select_place_clicked(self):
        self._display_place_selection()

    def get_location_selection(self):
        return self._location_selection

    def get_place_selection(self):
        return self._place_selection

    def get_currently_selected_selection(self):
        if self._allowed_for_selection_visited_item_type == VisitedItemType.LOCATION and \
                self._location_selection is not None:
            return self._location_selection
        elif self._allowed_for_selection_visited_item_type == VisitedItemType.PLACE and \
                self._place_selection is not None:
            return self._place_selection
        else:
            return None

    def _set_editability(self, editability: Editability):
        gui.set_editability_for_edit(self.ui.lineEdit_name, False)
        self._editability = editability
        is_editable = bool(editability == Editability.EDITABLE)

        gui.set_editability_for_edit(self.ui.textEdit_note, is_editable)
        self.ui.pushButton_select_location.setVisible(is_editable)
        StartEndTimeViewer._set_editability(self, is_editable)

    def _set_edits_signals_enabled(self, enable):
        self.ui.lineEdit_name.blockSignals(enable)
        self.ui.textEdit_note.blockSignals(enable)
        StartEndTimeViewer._set_edits_signals_enabled(self, enable)

    def display_item(self, visit: Visit):
        self._set_edits_signals_enabled(True)
        self._current_item = visit
        if self._current_item.db_id is None and self._current_journey is None:
            # if visit is not from database, and still has no journey assigned, set default jouney ('Unspecified')
            self._current_item.journey_db_id = Journey.UNSPECIFIED_JOURNEY_DB_ID

        if visit is None:
            self.ui.lineEdit_name.setText('')
        else:
            self._set_visit_name()

        if visit is None:
            self._current_item.journey_db_id = None
            self._current_journey = None
        else:
            self._current_item.journey_db_id = visit.journey_db_id
            if self._current_item.journey_db_id is not None:
                self._current_journey = engine.traveller_database.get_item_by_db_id(item_type=Journey,
                                                                                    db_id=self._current_item.journey_db_id)
            else:
                self._current_journey = None
        self._display_journey_info()
        if visit.note:
            self.ui.textEdit_note.setHtml(visit.note)
        else:
            self.ui.textEdit_note.setHtml('')

        StartEndTimeViewer.display_item(self, visit)

        self._set_location_and_place_from_existing_visit(visit)

        self._set_edits_signals_enabled(False)

    def _display_journey_info(self):
        if self._current_journey is not None:
            self._current_item.journey_db_id = self._current_journey.db_id
            journey_txt = self._current_journey.get_short_description()
        else:
            self._current_item.journey_db_id = None
            journey_txt = '-'
        self.ui.label_journey_description.setText(journey_txt)

    def _set_location_and_place_from_existing_visit(self, visit):
        self._place_selection = None
        self._location_selection = LocationSelection()
        self._visited_item_selected = False

        if visit is not None and visit.visited_item_id is not None:
            self._visited_item_selected = True
            item = engine.traveller_database.get_item_by_item_id(item_id=visit.visited_item_id)
            assert item.__class__ in visitable_items.VISITABLE_ITEMS
            if item.__class__ in visitable_items.LOCATION_TYPES:
                self._location_selection = \
                    engine.traveller_database.get_location_selection_from_youngest_selected_location_id(item.item_id)
                self._allowed_for_selection_visited_item_type = VisitedItemType.LOCATION
            elif item.__class__ in visitable_items.PLACE_TYPES:
                self._place_selection = item
                self._allowed_for_selection_visited_item_type = VisitedItemType.PLACE
            else:
                raise NotImplementedError
        self._display_location_selection()
        self._display_place_selection()
        self._set_visit_name()

    @Slot()
    def _set_visit_name(self):
        if self._visited_item_selected:
            if self._allowed_for_selection_visited_item_type == VisitedItemType.LOCATION:
                visited_item = self._location_selection.get_youngest_selection()
            elif self._allowed_for_selection_visited_item_type == VisitedItemType.PLACE:
                raise NotImplementedError
            else:
                raise NotImplementedError
            visit_name = self._current_item.get_visit_name_for_selected_spot(visited_item=visited_item)
        else:
            visit_name = '-'
        self.ui.lineEdit_name.setText(visit_name)

    @Slot()
    def _note_changed(self):
        self._current_item.note = self.ui.textEdit_note.toHtml()
        self.signal_item_edited.emit()


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    # w = JourneyDetailsWidget(editability=Editability.NOT_EDITABLE)
    w = VisitDetailsWidget(editability=Editability.EDITABLE)
    w.show()
    sys.exit(app.exec_())
