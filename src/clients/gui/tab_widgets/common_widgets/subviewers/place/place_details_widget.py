from PySide2.QtCore import Slot, Signal

from traveller.items.spots.places.place import Place
from traveller.items.spots.locations.location import Location
from traveller.items.spots.locations.location_selection import LocationSelection
from traveller.items.standalone.journey import Journey

import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.coordinates.edit_coordinates_widget import EditCoordinatesWidget
from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import ItemDetailsWidget, Editability
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.start_end_time_viewer import StartEndTimeViewer
from clients.gui.tab_widgets.common_widgets.selectors.select_item_dialog import selectors_wrappers
from clients.gui.util import gui


class PlaceDetailsWidget(ItemDetailsWidget):

    def __init__(self, editability: Editability = Editability.EDITABLE):
        ItemDetailsWidget.__init__(self, editability)
        self.ui = self.generate_ui()
        self._current_item = Place()
        self._coordinate_widget = EditCoordinatesWidget(parent=self)
        self.ui.verticalLayout_coordinates.addWidget(self._coordinate_widget)
        self._location_selection = LocationSelection()
        self._set_editability(self._editability)

        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.place.moc_place_details_widget \
            import Ui_place_details_widget
        ui = Ui_place_details_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.pushButton_select_location.clicked.connect(self._select_location_clicked)
        self.ui.lineEdit_name.textChanged.connect(self.signal_item_edited)
        self._coordinate_widget.signal_item_edited.connect(self.signal_item_edited)

    @Slot()
    def _select_location_clicked(self):
        selected_location_or_none = selectors_wrappers.select_location_or_none_from_dialog(selected_location=self._location_selection)  # type: LocationSelection
        if selected_location_or_none is not None:
            self._location_selection = selected_location_or_none
            self._display_location_selection()
            self.signal_item_edited.emit()

    def _display_location_selection(self):
        location_description = self._location_selection.get_short_description_text_or_empty_for_field()
        self.ui.label_selected_location.setText(location_description)

    def get_location_selection(self):
        return self._location_selection

    def get_currently_selected_selection(self):
        return self._location_selection

    def _set_editability(self, editability: Editability):
        self._editability = editability
        is_editable = bool(editability == Editability.EDITABLE)
        gui.set_editability_for_edit(self.ui.lineEdit_name, is_editable)
        self._coordinate_widget.set_editability(editability)
        self.ui.pushButton_select_location.setVisible(is_editable)

    def _set_edits_signals_enabled(self, enable):
        self.ui.lineEdit_name.blockSignals(enable)

    def display_item(self, place: Place):
        self._set_edits_signals_enabled(True)
        self._current_item = place
        self.ui.lineEdit_name.setText(place.name)
        self._coordinate_widget.display_item_details_edits(place)

        self._update_selected_location_from_current_item()

        self._set_edits_signals_enabled(False)

    def _update_selected_location_from_current_item(self):
        self._location_selection = LocationSelection()
        if self._current_item is not None and self._current_item.parent_location_id is not None:
            item = engine.traveller_database.get_item_by_item_id(item_id=self._current_item.parent_location_id)
            assert isinstance(item, Location)
            self._location_selection = \
                engine.traveller_database.get_location_selection_from_youngest_selected_location_id(item.item_id)
        self._display_location_selection()

    def get_edited_item(self) -> Place:
        self._coordinate_widget.update_item_from_ui(self._current_item)
        self._current_item.name = self.ui.lineEdit_name.text()
        return self._current_item



if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    # w = JourneyDetailsWidget(editability=Editability.NOT_EDITABLE)
    w = PlaceDetailsWidget(editability=Editability.EDITABLE)
    w.show()
    sys.exit(app.exec_())
