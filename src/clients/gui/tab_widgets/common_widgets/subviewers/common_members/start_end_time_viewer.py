import datetime

from PySide2.QtCore import Slot, Signal, Qt, QDateTime, QTime

from clients.gui.util import gui


class StartEndTimeViewer:
    """
    This class only extends other item details widgets - packs function for time into common class
    """
    def __init__(self):
        pass

    def prepare_ui(self):
        gui.prepare_datetime_widget(self.ui.dateTimeEdit_start_time)
        gui.prepare_datetime_widget(self.ui.dateTimeEdit_end_time)

    def create_ui_connections(self):
        self.ui.checkBox_start_time.stateChanged.connect(self._start_time_checkbox_clicked)
        self.ui.checkBox_end_time.stateChanged.connect(self._end_time_checkbox_clicked)
        self.ui.dateTimeEdit_start_time.dateTimeChanged.connect(self._start_time_date_changed)
        self.ui.dateTimeEdit_end_time.dateTimeChanged.connect(self._end_time_date_changed)

    def _start_time_date_changed(self):
        self._current_item.start_unix_time = self.ui.dateTimeEdit_start_time.dateTime().toTime_t()
        if not self.ui.checkBox_end_time.isChecked():
            qdatetime = self.ui.dateTimeEdit_start_time.dateTime()
            qdatetime.addDays(7)
            qtime = QTime(20, 0, 0)
            qdatetime.setTime(qtime)
            self.ui.dateTimeEdit_end_time.setDateTime(qdatetime)
        self.signal_item_edited.emit()

    def _end_time_date_changed(self):
        self._current_item.end_unix_time = self.ui.dateTimeEdit_end_time.dateTime().toTime_t()
        self.signal_item_edited.emit()

    def _start_time_checkbox_clicked(self, state):
        self.ui.dateTimeEdit_start_time.setEnabled(state)
        self._start_time_date_changed()

    def _end_time_checkbox_clicked(self, state):
        self.ui.dateTimeEdit_end_time.setEnabled(state)
        self._end_time_date_changed()

    def _set_edits_signals_enabled(self, enable):
        self.ui.dateTimeEdit_start_time.blockSignals(enable)
        self.ui.dateTimeEdit_end_time.blockSignals(enable)
        self.ui.checkBox_start_time.blockSignals(enable)
        self.ui.checkBox_end_time.blockSignals(enable)

    def _set_editability(self, is_editable):
        gui.set_editability_for_edit(self.ui.dateTimeEdit_start_time, is_editable)
        gui.set_editability_for_edit(self.ui.dateTimeEdit_end_time, is_editable)

        self.ui.checkBox_start_time.setVisible(is_editable)
        self.ui.checkBox_end_time.setVisible(is_editable)

    def display_item(self, item):
        start_qdatetime = QDateTime()
        if item is None or item.start_unix_time is None:
            self.ui.checkBox_start_time.setChecked(False)
            self.ui.dateTimeEdit_start_time.setEnabled(False)
            start_qdatetime.setTime_t(datetime.datetime.now().replace(hour=8, minute=0).timestamp())
        else:
            self.ui.checkBox_start_time.setChecked(True)
            self.ui.dateTimeEdit_start_time.setEnabled(True)
            start_qdatetime.setTime_t(item.start_unix_time)
        self.ui.dateTimeEdit_start_time.setDateTime(start_qdatetime)

        end_qdatetime = QDateTime()
        if item is None or item.end_unix_time is None:
            self.ui.checkBox_end_time.setChecked(False)
            self.ui.dateTimeEdit_end_time.setEnabled(False)
            end_qdatetime.setTime_t(datetime.datetime.now().replace(hour=20, minute=0).timestamp())
        else:
            self.ui.checkBox_end_time.setChecked(True)
            self.ui.dateTimeEdit_end_time.setEnabled(True)
            end_qdatetime.setTime_t(item.end_unix_time)
        self.ui.dateTimeEdit_end_time.setDateTime(end_qdatetime)
