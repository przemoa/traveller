from PySide2 import QtWidgets
from PySide2.QtCore import Slot, Signal

from clients.gui.common.ui_owner import UiOwner
from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import Editability


class EditCoordinatesWidget(UiOwner, QtWidgets.QWidget):
    signal_item_edited = Signal()

    def __init__(self, parent):
        UiOwner.__init__(self)
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = self.generate_ui()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.common_members.coordinates.moc_edit_coordinates_widget import Ui_edit_coordinates_widget
        ui = Ui_edit_coordinates_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.doubleSpinBox_longitude.valueChanged.connect(self.signal_item_edited)
        self.ui.doubleSpinBox_latitude.valueChanged.connect(self.signal_item_edited)
        self.ui.groupBox_coordinates.toggled.connect(self.signal_item_edited)

    def set_edits_signals_enabled(self, enable):
        self.ui.doubleSpinBox_longitude.blockSignals(enable)
        self.ui.doubleSpinBox_latitude.blockSignals(enable)
        self.ui.groupBox_coordinates.blockSignals(enable)

    def update_item_from_ui(self, item):
        if self.ui.groupBox_coordinates.isChecked():
            item.latitude = self.ui.doubleSpinBox_latitude.value()
            item.longitude = self.ui.doubleSpinBox_longitude.value()
        else:
            item.latitude = None
            item.longitude = None

    def reset_item_details_edits(self):
        self.ui.groupBox_coordinates.setChecked(False)
        self.ui.doubleSpinBox_latitude.setValue(0)
        self.ui.doubleSpinBox_longitude.setValue(0)

    def display_item_details_edits(self, item):
        self.set_edits_signals_enabled(False)
        if item.latitude is not None and item.longitude is not None:
            self.ui.groupBox_coordinates.setChecked(True)
            self.ui.doubleSpinBox_latitude.setValue(item.latitude)
            self.ui.doubleSpinBox_longitude.setValue(item.longitude)
        else:
            self.ui.groupBox_coordinates.setChecked(False)
            self.ui.doubleSpinBox_latitude.setValue(0)
            self.ui.doubleSpinBox_longitude.setValue(0)
        self.set_edits_signals_enabled(True)

    def set_editability(self, editability: Editability):
        self.ui.groupBox_coordinates.setEnabled(editability == Editability.EDITABLE)


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = EditCoordinatesWidget(parent=None)
    w.show()
    sys.exit(app.exec_())
