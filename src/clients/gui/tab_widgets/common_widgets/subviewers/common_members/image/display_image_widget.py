from typing import List

from traveller.items.standalone.photo import Photo
from traveller.common import config

from PySide2 import QtWidgets
from PySide2.QtWidgets import QGraphicsPixmapItem, QGraphicsScene
from PySide2.QtGui import QPixmap, QImage
from PySide2 import QtCore
from PySide2.QtCore import QByteArray


class DisplaySingleImageWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.ui = self.generate_ui()
        self.create_ui_connections()
        self._scene = QGraphicsScene(self)
        self.ui.graphicsView.setScene(self._scene)
        self._item = QGraphicsPixmapItem()
        self._scene.addItem(self._item)

    def display_image(self, image_bytes: bytes):
        qbyte_array = QByteArray(image_bytes)
        qimage = QImage()
        qimage.loadFromData(qbyte_array)
        pixmap = QPixmap(qimage)
        pixmap.convertFromImage(qimage)
        self._item.setPixmap(pixmap)
        self.fit_image()

    def resizeEvent(self, event):
        self.fit_image()

    def showEvent(self, event):
        self.fit_image()

    def fit_image(self):
        self.ui.graphicsView.fitInView(self._item, QtCore.Qt.KeepAspectRatio)

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.common_members.image.moc_display_image_widget import Ui_display_image_widget
        ui = Ui_display_image_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        pass


class DisplayMultipleImagesWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.ui = self.generate_ui()
        self.create_ui_connections()
        self._scene = QGraphicsScene(self)
        self.ui.graphicsView.setScene(self._scene)

    def display_multiple_images(self, photos: List[Photo]):
        self._scene.clear()
        NUMBER_OF_COLUMNS = 5
        current_y_offset = 0
        current_row_height = 0
        for i, photo in enumerate(photos):
            item = QGraphicsPixmapItem()
            if i % NUMBER_OF_COLUMNS == 0:
                current_y_offset += current_row_height
                current_row_height = 0
            column = i % NUMBER_OF_COLUMNS
            row = i // NUMBER_OF_COLUMNS
            item.setOffset(
                column * (config.THUMBNAIL_SIZE_IN_PIXELS + 5),
                current_y_offset)
            qbyte_array = QByteArray(photo.thumbnail)
            qimage = QImage()
            qimage.loadFromData(qbyte_array)
            current_row_height = max(current_row_height, qimage.height())
            pixmap = QPixmap(qimage)
            pixmap.convertFromImage(qimage)
            item.setPixmap(pixmap)
            self._scene.addItem(item)
        self.fit_image()

    def resizeEvent(self, event):
        self.fit_image()

    def showEvent(self, event):
        self.fit_image()

    def fit_image(self):
        self.ui.graphicsView.fitInView(self._scene.sceneRect(), QtCore.Qt.KeepAspectRatioByExpanding)

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.common_members.image.moc_display_image_widget import Ui_display_image_widget
        ui = Ui_display_image_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        pass


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    if 0:
        w = DisplaySingleImageWidget(parent=None)
        w.show()
        photo = Photo.create_from_file(
            file_path='/home/przemek/Projects/traveller/tests/test_files/img/IMG_20180812_131416.jpg')
        w.display_image(image_bytes=photo.thumbnail)
    if 1:
        w = DisplayMultipleImagesWidget(parent=None)
        w.show()

        import clients.gui.common.engine as engine
        photos = engine.traveller_database.get_all_items_of_type(Photo)
        w.display_multiple_images(photos*100)

    sys.exit(app.exec_())
