from PySide2.QtCore import Slot, Signal

from traveller.items.standalone.journey import Journey
from traveller.items.spots.locations.location_selection import LocationSelection

import clients.gui.common.engine as engine
from clients.gui.tab_widgets.common_widgets.subviewers.item_details_widget import ItemDetailsWidget, Editability
from clients.gui.tab_widgets.common_widgets.subviewers.common_members.start_end_time_viewer import StartEndTimeViewer
from clients.gui.tab_widgets.common_widgets.selectors.select_item_dialog import selectors_wrappers
from clients.gui.util import gui


class JourneyDetailsWidget(ItemDetailsWidget, StartEndTimeViewer):
    # signal_item_edited = Signal()

    def __init__(self, editability: Editability = Editability.EDITABLE):
        ItemDetailsWidget.__init__(self, editability)
        StartEndTimeViewer.__init__(self)
        self.ui = self.generate_ui()
        self._current_item = Journey()
        self._set_editability(self._editability)
        self._start_location_selection = LocationSelection()
        self._end_location_selection = LocationSelection()
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.tab_widgets.common_widgets.subviewers.journey.moc_journey_details_widget \
            import Ui_journey_details_widget
        ui = Ui_journey_details_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.pushButton_select_start_location.clicked.connect(self._select_start_location_clicked)
        self.ui.pushButton_select_end_location.clicked.connect(self._select_end_location_clicked)
        self.ui.lineEdit_name.textChanged.connect(self.journey_name_changed)
        StartEndTimeViewer.create_ui_connections(self)
        self.ui.textEdit_note.textChanged.connect(self._note_changed)

    @Slot()
    def journey_name_changed(self):
        self._current_item.name = self.ui.lineEdit_name.text()
        self.signal_item_edited.emit()

    def _display_start_location_selection(self):
        location_description = self._start_location_selection.get_short_description_text_or_empty_for_field()
        self.ui.label_start_location.setText(location_description)

    @Slot()
    def _select_start_location_clicked(self):
        selected_location_or_none = selectors_wrappers.select_location_or_none_from_dialog(selected_location=self._start_location_selection)  # type: LocationSelection
        if selected_location_or_none is not None:
            self._start_location_selection = selected_location_or_none
            self._display_start_location_selection()
            self.signal_item_edited.emit()

    def _display_end_location_selection(self):
        location_description = self._end_location_selection.get_short_description_text_or_empty_for_field()
        self.ui.label_end_location.setText(location_description)

    @Slot()
    def _select_end_location_clicked(self):
        selected_location_or_none = selectors_wrappers.select_location_or_none_from_dialog(selected_location=self._end_location_selection)
        if selected_location_or_none is not None:
            self._end_location_selection = selected_location_or_none
            self._display_end_location_selection()
            self.signal_item_edited.emit()

    def get_start_and_end_location_selections(self):
        return self._start_location_selection, self._end_location_selection

    def _set_editability(self, editability: Editability):
        self._editability = editability
        is_editable = bool(editability == Editability.EDITABLE)

        gui.set_editability_for_edit(self.ui.lineEdit_name, is_editable)

        self.ui.pushButton_select_start_location.setVisible(is_editable)
        self.ui.pushButton_select_end_location.setVisible(is_editable)
        gui.set_editability_for_edit(self.ui.textEdit_note, is_editable)
        StartEndTimeViewer._set_editability(self, is_editable)

    def _set_edits_signals_enabled(self, enable):
        self.ui.lineEdit_name.blockSignals(enable)
        self.ui.textEdit_note.blockSignals(enable)
        StartEndTimeViewer._set_edits_signals_enabled(self, enable)

    def display_item(self, journey: Journey):
        self._set_edits_signals_enabled(True)
        self._current_item = journey
        self._start_location_selection = LocationSelection()
        self._end_location_selection = LocationSelection()

        if journey is None:
            self.ui.lineEdit_name.setText('')
        else:
            self.ui.lineEdit_name.setText(journey.name)

        StartEndTimeViewer.display_item(self, journey)

        if journey is None or journey.start_location_item_id is None:
            self._start_location_selection = LocationSelection()
        else:
            self._start_location_selection = \
                engine.engine.get_location_selection_from_youngest_selected_location_id(journey.start_location_item_id)
        self._display_start_location_selection()

        if journey is None or journey.end_location_item_id is None:
            self._end_location_selection = LocationSelection()
        else:
            self._end_location_selection = \
                engine.engine.get_location_selection_from_youngest_selected_location_id(
                    journey.end_location_item_id)
        self._display_end_location_selection()

        if journey.note:
            self.ui.textEdit_note.setHtml(journey.note)
        else:
            self.ui.textEdit_note.setHtml('')

        self._set_edits_signals_enabled(False)

    @Slot()
    def _note_changed(self):
        self._current_item.note = self.ui.textEdit_note.toHtml()
        self.signal_item_edited.emit()

if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    # w = JourneyDetailsWidget(editability=Editability.NOT_EDITABLE)
    w = JourneyDetailsWidget(editability=Editability.EDITABLE)
    w.show()
    sys.exit(app.exec_())
