from enum import Enum
from typing import List

from PySide2 import QtWidgets
from PySide2.QtCore import Slot, Signal

from traveller.items.spots.locations.location_selection import LocationSelection
from traveller.items.item import Item

from clients.gui.tab_widgets.common_widgets.selectors.location.dialog.select_existing_region_or_city_dialog import SelectExistingRegionOrCityDialog
from clients.gui.common.ui_owner import UiOwner


class Editability(Enum):
    EDITABLE, NOT_EDITABLE = range(2)


class ItemDetailsWidget(QtWidgets.QWidget):
    # probably same signals as ItemViewerWidget
    # signal: open item in browser
    # signal: open item in appropriate item_viewer widget
    # signal: open item in wikipedia widget

    # smallest displaying unit. For displaying informations

    signal_item_edited = Signal()

    def __init__(self, editability: Editability):
        """

        :rtype: object
        """
        QtWidgets.QWidget.__init__(self)
        UiOwner.__init__(self)
        self._editability = editability
        self._current_item = None  # used also for items displaying multiple items at once

    def _set_editability(self, editability: Editability):
        raise NotImplementedError

    def display_item(self, item: Item):
        raise NotImplementedError

    def display_multiple_items(self, items: List[Item]):
        raise NotImplementedError

    def _set_edits_signals_enabled(self, enable):
        raise NotImplementedError

    def get_edited_item(self) -> Item:
        # TODO add this to all ItemDetailsWidgets - widget should copy item during display, edit it, and return if requested.
        # It should not edit item given to display
        # TODO for all classes
        raise NotImplementedError

    def get_edited_items(self) -> List[Item]:
        raise NotImplementedError
