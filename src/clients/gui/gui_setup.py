import os
import subprocess
import sys
import logging

from PySide2 import QtWidgets
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QApplication

from traveller.traveller_engine import TravellerEngine
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.common import config
from traveller.items.standalone.photo import Photo
from traveller.items.visits.visit import Visit
from traveller.items.standalone.journey import Journey

import clients.gui.util.naming as naming
import clients.gui.common.engine as engine
from clients.gui.main_window.main_window import MainWindow
from clients.gui.common import user_settings
from clients.gui.common.package_config import ProjectConfig, package_config_devel


def debug_setup(create_mocs=True):
    setup_logger()
    if create_mocs:
        moc_ui_files()
    load_gui_config(package_config=package_config_devel)
    create_temporary_traveller_database()


def _create_engine(traveller_database_path, prompts_datatabase_path):
    engine.engine = TravellerEngine(
        traveller_database_path,
        prompts_datatabase_path)
    engine.traveller_database = engine.engine.get_traveller_database()


def create_temporary_traveller_database():
    traveller_database_path = user_settings.settings.database_file_path
    prompts_datatabase_path = user_settings.settings.prompts_database_file_path

    try:
        os.remove(traveller_database_path)
    except OSError:
        pass
    print('temporary database:', traveller_database_path)
    _create_engine(traveller_database_path=traveller_database_path, prompts_datatabase_path=prompts_datatabase_path)
    add_some_data_to_temporary_database()


def open_or_create_traveller_database():
    traveller_database_path = user_settings.settings.database_file_path
    prompts_datatabase_path = user_settings.settings.prompts_database_file_path

    print('traveller database:', traveller_database_path)
    _create_engine(traveller_database_path=traveller_database_path, prompts_datatabase_path=prompts_datatabase_path)


def add_some_data_to_temporary_database():
    poland = Country(iso_code='PL', name='Poland')
    italy = Country(iso_code='IT', name='Italy')

    engine.traveller_database.add_country(poland)
    engine.traveller_database.add_country(italy)

    lubusz = AdminRegionMajor(
        country_code='PL',
        ascii_name='Lubusz',
        geoname_admin1_code='76')
    engine.traveller_database.add_admin_region_major(lubusz, country=poland)

    greater_poland = AdminRegionMajor(
        ascii_name='Greater Poland')
    engine.traveller_database.add_admin_region_major(greater_poland, country=poland)

    lombardy = AdminRegionMajor(
        ascii_name='Lombardy')
    engine.traveller_database.add_admin_region_major(lombardy, country=italy)

    zielona_gora_powiat = AdminRegionMinor(
        ascii_name='Zielona Gora powiat')
    engine.traveller_database.add_admin_region_minor(
        zielona_gora_powiat,
        admin_region_major=lubusz,
        country=poland)

    zielonogorski = AdminRegionMinor(
        ascii_name='Zielonogorski')
    engine.traveller_database.add_admin_region_minor(
        zielonogorski,
        admin_region_major=lubusz,
        country=poland)

    poznanski = AdminRegionMinor(
        ascii_name='Powiat poznanski')
    engine.traveller_database.add_admin_region_minor(
        poznanski,
        admin_region_major=greater_poland,
        country=poland)

    metrpolitana_milano = AdminRegionMinor(
        ascii_name='metropolitana di Milano')
    engine.traveller_database.add_admin_region_minor(
        metrpolitana_milano,
        admin_region_major=lombardy,
        country=italy)

    zielona_gora = City(
        ascii_name='Zielona Gora',
        population=150123,
        latitude=34.12,
        longitude=43.23
    )
    engine.traveller_database.add_city(
        zielona_gora,
        admin_region_minor=zielona_gora_powiat,
        admin_region_major=lubusz,
        country=poland)

    kargowa = City(
        ascii_name='Kargowa',
        latitude=48, longitude=8)
    engine.traveller_database.add_city(
        kargowa,
        admin_region_minor=zielonogorski,
        admin_region_major=lubusz,
        country=poland)

    babimost = City(
        ascii_name='Babimost',
        latitude=48, longitude=13)
    engine.traveller_database.add_city(
        babimost,
        admin_region_minor=zielonogorski,
        admin_region_major=lubusz,
        country=poland)

    komorniki = City(
        ascii_name='Komorniki',
        latitude=52, longitude=10)
    engine.traveller_database.add_city(
        komorniki,
        admin_region_minor=poznanski,
        admin_region_major=greater_poland,
        country=poland)

    milan = City(
        ascii_name='Milan',
        latitude=48, longitude=10)
    engine.traveller_database.add_city(
        milan,
        admin_region_minor=metrpolitana_milano,
        admin_region_major=lombardy,
        country=italy)

    journey = Journey(
        name='around world',
        start_location_item_id=kargowa.item_id,
        end_location_item_id=komorniki.item_id,
        start_unix_time=123456789,
        end_unix_time=123456789,
    )
    engine.traveller_database.add_item(journey)

    visit1 = Visit(
        start_unix_time=4444444,
        visited_item_id=milan.item_id
    )
    engine.traveller_database.add_item(visit1)

    visit1 = Visit(
        start_unix_time=554444444,
        visited_item_id=milan.item_id
    )
    engine.traveller_database.add_item(visit1)

    visit2 = Visit(
        start_unix_time=3600*24*365*40,
        end_unix_time=None,
        journey_db_id=journey.db_id,
        visited_item_id=kargowa.item_id
    )
    engine.traveller_database.add_item(visit2)

    visit3 = Visit(
        start_unix_time=4444444,
        visited_item_id=zielona_gora.item_id
    )
    engine.traveller_database.add_item(visit3)

    images_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), '../../../tests/test_files/img/')
    photo = Photo.create_from_file(
        file_path=os.path.join(images_path, 'IMG_20180812_131416.jpg'))
    engine.traveller_database.add_item(photo)

    photo = Photo.create_from_file(
        file_path=os.path.join(images_path, 'IMG_20180813_132048.jpg'))
    engine.traveller_database.add_item(photo)

    photo = Photo.create_from_file(
        file_path=os.path.join(images_path, 'img2/IMG_20180809_140552.jpg'))
    engine.traveller_database.add_item(photo)


def _moc_ui_file(ui_file_path: str, moc_file_path: str):
    if os.path.isfile(moc_file_path):
        ui_file_modification_time = os.path.getmtime(ui_file_path)
        moc_file_modification_time = os.path.getmtime(moc_file_path)
        if moc_file_modification_time > ui_file_modification_time:
            logging.debug('Skipping mocking of file {}'.format(ui_file_path))
            return

    logging.debug('Mocking file {} to {}'.format(ui_file_path, moc_file_path))
    try:
        os.remove(moc_file_path)
    except OSError:
        pass
    moc_command = 'pyside2-uic {0} -o {1} -x'.format(ui_file_path, moc_file_path)

    process = subprocess.Popen(moc_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdout, stderr) = process.communicate(timeout=10)
    return_code = process.returncode
    if return_code is not 0:
        exception_message = 'Mocing file {0} failed. stdout: {1}, stderr: {2}'.format(
            ui_file_path, str(stdout), str(stderr))
        raise Exception(exception_message)


def moc_ui_files():
    logging.debug('Mocking started...')
    gui_dir_path = os.path.dirname(os.path.realpath(__file__))
    for (dir_path, dir_names, file_names) in os.walk(gui_dir_path):
        for file_name in file_names:
            if file_name.endswith('.ui'):
                ui_file_path = os.path.join(dir_path, file_name)
                moc_module_name = naming.get_moc_module_name_for_ui(ui_name=file_name[:-3])
                moc_file_path = os.path.join(dir_path, moc_module_name + '.py')
                _moc_ui_file(ui_file_path=ui_file_path, moc_file_path=moc_file_path)
    logging.debug('Mocking finished')


def load_gui_config(package_config: ProjectConfig):
    user_settings.load_settings(package_config=package_config)


def save_gui_config(package_config: ProjectConfig):
    user_settings.save_settings(package_config=package_config)


def run_traveller_gui():
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    app.exec_()


def logger_exception_hook(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.fatal("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


def setup_logger(log_file_path=None, log_level=logging.DEBUG):
    console_formatter = logging.Formatter("%(asctime)s.%(msecs)03d [%(levelname)-7s]:  "
                                          "%(message)s [%(funcName)s]", datefmt="%H:%M:%S")
    file_formatter = logging.Formatter("%(asctime)s.%(msecs)03d [%(levelname)-7s]:  "
                                       "%(message)s [%(funcName)s]", datefmt="%Y-%m-%d %H:%M:%S")

    if log_file_path:
        file_handler = logging.FileHandler(log_file_path)
        file_handler.setFormatter(file_formatter)
        logging.getLogger().addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(console_formatter)
    logging.getLogger().addHandler(stream_handler)

    logging.getLogger().setLevel(log_level)
    if config.DEBUG_MODE:
        sys.excepthook = logger_exception_hook
    logging.info('Logger started')


if __name__ == '__main__':
    moc_ui_files()
