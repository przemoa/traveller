from PySide2 import QtWidgets

from clients.gui.common.ui_owner import UiOwner


class TabWidget(UiOwner, QtWidgets.QWidget):
    def __init__(self, parent, tab_id: str):
        UiOwner.__init__(self)
        QtWidgets.QWidget.__init__(self, parent)
        self.tab_id = tab_id


class InvalidTabWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent, tab_id)
        self.generate_ui()

    def generate_ui(self):
        layout = QtWidgets.QVBoxLayout(self)
        label = QtWidgets.QLabel(self)
        label.setText('ERROR! Failed to setup "{}" tab!'.format(self.tab_id))
        layout.addWidget(label)
