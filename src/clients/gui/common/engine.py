from traveller.traveller_engine import TravellerEngine
from traveller.service.traveller_database.traveller_database import TravellerDatabase

engine = None  # type: TravellerEngine
traveller_database = None  # type: TravellerDatabase
