class TabId:
    EDIT_CITY_WIDGET = 'Edit City'
    EDIT_JOURNEY_WIDGET = 'Edit Journey'
    EDIT_VISIT = 'Edit Visit'
    EDIT_PHOTOS_WIDGET = 'Edit Photos'
    MAP_WIDGET = 'Map'
    EDIT_PLACE_WIDGET = 'Edit Place'
