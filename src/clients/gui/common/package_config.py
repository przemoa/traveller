import getpass
import os


GUI_ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')


class ProjectConfig:
    def __init__(self,
                 prompts_database_path: str,
                 database_path: str,
                 config_file_path: str,
                 ):
        self.prompts_database_path = os.path.abspath(prompts_database_path)
        self.database_path = os.path.abspath(database_path)
        self.config_file_path = os.path.abspath(config_file_path)


package_config_devel = ProjectConfig(
    prompts_database_path=os.path.join(
        GUI_ROOT_DIR, '..', '..', 'traveller', 'prompts', 'data', 'location_prompts.sqlite'),
    database_path=os.path.join(
        GUI_ROOT_DIR, '{}_traveller_devel.sqlite'.format(getpass.getuser())),
    config_file_path=os.path.join(
        GUI_ROOT_DIR, 'traveller_config_devel.json'),
)

package_config_release = ProjectConfig(
    prompts_database_path=os.path.join(
        GUI_ROOT_DIR, '..', '..', 'traveller', 'prompts', 'data', 'location_prompts.sqlite'),  # TODO change to installed with package file
    database_path=os.path.join(
        '/media/przemek/dane/tmp_private', '{}_traveller.sqlite'.format(getpass.getuser())),  # TODO change to installed with package file
    config_file_path = os.path.join(
        GUI_ROOT_DIR, 'traveller_config.json'),  # TODO change to installed with package file
)
