import logging
import json
import os
from json import JSONEncoder

from clients.gui.common.package_config import ProjectConfig


settings = None  # type: UserSettings


class JsonEncoderToDict(JSONEncoder):
    def default(self, o):
        return o.__dict__


class UserSettings:
    def __init__(self,
                 prompts_database_path: str,
                 database_path: str,
                 photo_root_dir_path: str = os.path.expanduser("~"),
                 ):
        self.prompts_database_file_path = prompts_database_path
        self.database_file_path = database_path
        self.photo_root_dir_path = photo_root_dir_path

    def load_from_file(self, package_config: ProjectConfig):
        config_dict = None
        file_path = package_config.config_file_path
        try:
            with open(file_path, "r", encoding="utf8") as infile:
                config_dict = json.load(infile)
            self._load_from_dict(config_dict)
            logging.info('Configuration read from file %s', file_path)
        except (IOError, ValueError, KeyError, TypeError) as e:
            logging.warning('Can not load configuration from json file: %s. Using default configuration' % file_path)

    def save_to_file(self, package_config: ProjectConfig):
        file_path = package_config.config_file_path
        try:
            with open(file_path, 'w') as outfile:
                json.dump(self, outfile, indent=4, cls=JsonEncoderToDict)
            logging.info('Configuration saved to file %s', file_path)
        except (IOError, ValueError) as e:
            logging.exception('Can not save configuration to json file: %s' % file_path)

    def _load_from_dict(self, dict_from_file):
        for key, value in self.__dict__.items():
            if key in dict_from_file:
                value_from_file = dict_from_file[key]
                if type(value) == type(value_from_file):
                    self.__dict__[key] = value_from_file
                else:
                    logging.warning('Invalid data type in settings file for field: {}'.format(key))
            else:
                logging.warning('Missing field in settings file: {}'.format(key))


def load_settings(package_config: ProjectConfig):
    global settings
    settings = UserSettings(
        prompts_database_path=package_config.prompts_database_path,
        database_path=package_config.database_path,
    )
    settings.load_from_file(package_config)


def save_settings(package_config: ProjectConfig):
    global settings
    settings.save_to_file(package_config)


if __name__ == '__main__':
    s = UserSettings()
    s.load_from_file()
