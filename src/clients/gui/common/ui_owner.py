import clients.gui.util.naming as naming


class UiOwner:
    """Base class for all widget and dialogs witch contain ui.

    Each derived class should overwrite ui generation (load from file or create manually).
    Additionally some typical functions should be overwritten for convenience.

    Attributes:
        ui_name (str): name of this ui
        ui: container for major ui elements (except for big child widgets)
    """

    def __init__(self):
        self.ui_name = self.__class__.__name__
        self.ui = None

    def generate_ui(self):
        """This function should load from moc file or generate manually ui.
        Called from widget internally, e.g. in init.
        """
        raise NotImplementedError('Ui generation has to be overwritten in child class!')

    def create_ui_connections(self):
        """Connections for events from ui should be done here.
        Called from widget internally, e.g. in init.
        """
        pass

    def create_general_connection(self):
        """Connections for general, no ui based events, should be done here.
        Called from widget internally, e.g. in init.
        """
        pass

    def load_from_config(self):
        """Configuration for widget and ui should be loaded here (from global config).
        Called from widget internally, e.g. in init.
        """
        pass

    def save_to_config(self):
        """Configuration for widget and ui should be saved here (to global config).
        Called from parent.
        """
        pass

    def setup_after_ui_creation(self):
        """Any time consuming setup should be done here,
        as well as setup witch should be done after gui creation of all widgets potentially interested with signals
        from this widget.
        Called from parent widget.
        """
        pass


