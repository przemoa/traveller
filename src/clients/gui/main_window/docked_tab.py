from PySide2 import QtWidgets, QtGui

from clients.gui.common.tab_widget import TabWidget


class DockedTab(QtWidgets.QDockWidget):
    def __init__(self, parent, tab_id):
        QtWidgets.QDockWidget.__init__(self, tab_id, parent)
        self._scroll_area_widget = QtWidgets.QScrollArea(parent)
        self._scroll_area_widget.setWidgetResizable(True)
        self.setWidget(self._scroll_area_widget)
        self.tab_id = tab_id
        self.widget = None
        self.setVisible(False)
        self.setFeatures(QtWidgets.QDockWidget.DockWidgetFloatable |
                         QtWidgets.QDockWidget.DockWidgetMovable |
                         QtWidgets.QDockWidget.DockWidgetClosable)
        self.view_action = self._create_view_tab_action(parent)

    def _create_view_tab_action(self, parent):
        action = QtWidgets.QAction(parent)
        action.setObjectName(self.tab_id + '_view_action')
        action.setText(self.tab_id)
        action.setCheckable(True)
        action.toggled.connect(self._toggle_visibility)
        return action

    def _toggle_visibility(self, state):
        self.setVisible(state)
        if state:
            self.raise_()

    def closeEvent(self, *args, **kwargs):
        self.view_action.setChecked(False)

    def set_widget(self, widget):
        assert isinstance(widget, TabWidget)
        self.widget = widget
        self._scroll_area_widget.setWidget(widget)
        self.setObjectName(widget.tab_id + '_dock')
