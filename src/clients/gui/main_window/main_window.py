import logging
import collections
from typing import Union

from PySide2 import QtWidgets
from PySide2.QtCore import Qt

from clients.gui.common.ui_owner import UiOwner
from clients.gui.dialogs.settings_dialog.settings_dialog import SettingsDialog
from clients.gui.tab_widgets.edit.city import edit_city_widget
from clients.gui.tab_widgets.edit.journey import edit_journey_widget
from clients.gui.tab_widgets.edit.visit import edit_visit_widget
from clients.gui.tab_widgets.edit.photos import edit_photos_widget
from clients.gui.tab_widgets.edit.place import edit_place_widget
from clients.gui.tab_widgets.map import map_widget
from clients.gui.common.types import TabId
from clients.gui.main_window.docked_tab import DockedTab
from clients.gui.common.tab_widget import InvalidTabWidget
from clients.gui.dialogs.about_dialog.about_dialog import AboutDialog


_ALL_TABS_LIST = collections.OrderedDict(
    [
        (TabId.EDIT_CITY_WIDGET, edit_city_widget.EditCityWidget),  # TODO change to inherit from EditWidget
        (TabId.EDIT_JOURNEY_WIDGET, edit_journey_widget.EditJourneyWidget),
        (TabId.EDIT_PLACE_WIDGET, edit_place_widget.EditPlaceWidget),
        (TabId.EDIT_VISIT, edit_visit_widget.EditVisitWidget),
        (TabId.EDIT_PHOTOS_WIDGET, edit_photos_widget.PhotosEditWidget),
        (TabId.MAP_WIDGET, map_widget.MapWidget),
    ]
)


class MainWindow(UiOwner, QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        UiOwner.__init__(self)
        self.ui = self.generate_ui()
        self._docked_tabs = self._create_and_dock_all_docked_tabs()
        self._clip_all_docked_tabs()
        self._show_tabs(_ALL_TABS_LIST)
        self.create_ui_connections()

    def generate_ui(self):
        from clients.gui.main_window.moc_main_window import Ui_main_window
        ui = Ui_main_window()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.actionAbout.triggered.connect(self._show_about)
        self.ui.actionSettings.triggered.connect(self._show_settings)

    # Create and setup tabs - START #
    def _create_and_dock_all_docked_tabs(self):
        docked_tabs = collections.OrderedDict()
        self.setDockNestingEnabled(True)
        self.setTabPosition(Qt.TopDockWidgetArea, QtWidgets.QTabWidget.TabPosition.North)
        for tab_id, tab_class in _ALL_TABS_LIST.items():
            docked_tabs[tab_id] = self._create_docked_tab(tab_id=tab_id, tab_class=tab_class)
            self.addDockWidget(Qt.TopDockWidgetArea, docked_tabs[tab_id])
        return docked_tabs

    def _clip_all_docked_tabs(self):
        first_tab = None
        for docked_tab in self._docked_tabs.values():
            if not first_tab:
                first_tab = docked_tab
                continue
            self.tabifyDockWidget(first_tab, docked_tab)

    def _create_docked_tab(self, tab_id, tab_class):
        try:
            docked_tab = DockedTab(parent=self, tab_id=tab_id)
            tab_widget = tab_class(parent=docked_tab, tab_id=tab_id)
            docked_tab.set_widget(widget=tab_widget)
        except Exception:
            docked_tab = DockedTab(parent=self, tab_id=tab_id)
            logging.exception('Error during setup of tab "{}"'.format(tab_id))
            tab_widget = InvalidTabWidget(parent=docked_tab, tab_id=tab_id)
            docked_tab.set_widget(widget=tab_widget)
        self.ui.menuTabs.addAction(docked_tab.view_action)
        return docked_tab

    def _show_tabs(self, ids_to_show: Union[list, dict]):
        for tab_id in _ALL_TABS_LIST:
            state = bool(tab_id in ids_to_show)
            self._docked_tabs[tab_id].view_action.setChecked(state)

    # Create and setup tabs - END #

    def _show_about(self):
        about_dialog = AboutDialog()
        about_dialog.exec_()

    def _show_settings(self):
        settings_dialog = SettingsDialog()
        settings_dialog.exec_()


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
