def get_moc_module_name_for_ui(ui_name: str) -> str:
    return 'moc_' + ui_name
