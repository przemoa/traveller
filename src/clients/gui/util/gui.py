from PySide2.QtCore import QDateTime, QTime

def set_editability_for_edit(edit_widget, is_editable):
    if not is_editable:
        edit_widget.setStyleSheet("background-color: rgb(211, 211, 211);")
    else:
        edit_widget.setStyleSheet("")
    edit_widget.setReadOnly(not is_editable)


def set_date_time_for_widget(qdatetime_widget, unix_time):
    start_qdatetime = QDateTime()
    if unix_time is not None:
        start_qdatetime.setTime_t(unix_time)
    else:
        start_qdatetime.setTime_t(0)

    qdatetime_widget.setDateTime(start_qdatetime)


def prepare_datetime_widget(datetime_widget):
    datetime_widget.setCalendarPopup(True)
    datetime_widget.setDisplayFormat("dd/MM/yyyy hh:mm")
