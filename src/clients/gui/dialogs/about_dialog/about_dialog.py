from PySide2 import QtWidgets
from clients.gui.common.ui_owner import UiOwner
from clients.gui._version import __version__ as gui_version


class AboutDialog(UiOwner, QtWidgets.QDialog):
    def __init__(self):
        UiOwner.__init__(self)
        QtWidgets.QDialog.__init__(self)
        self.ui = self.generate_ui()
        self._display_info()

    def generate_ui(self):
        from clients.gui.dialogs.about_dialog.moc_about_dialog import Ui_about_dialog
        ui = Ui_about_dialog()
        ui.setupUi(self)
        return ui

    def _display_info(self):
        html_source = '<p><b>Traveller GUI</b> v{}</p>'.format(gui_version)
        html_source += '<p>Based on Traveller engine v{}</p>'.format(gui_version)
        self.ui.textBrowser_about.append(html_source)


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = AboutDialog()
    w.show()
    sys.exit(app.exec_())
