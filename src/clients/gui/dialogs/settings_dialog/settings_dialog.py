from functools import partial

from PySide2 import QtWidgets
from PySide2.QtWidgets import QLabel, QFileDialog, QMessageBox

from clients.gui.common.ui_owner import UiOwner
from clients.gui._version import __version__ as gui_version
from clients.gui.common import user_settings


class SettingsDialog(UiOwner, QtWidgets.QDialog):
    """ Generate dialog with settings for user_settings class.
    Special attributes (allowed characters/ranges/types) can be set additionally.
    Some attributes (e.g. field type) can be determined automatically.

    By default:
     - names ending with 'file_path' will be generated in the settings as file browser
     - names ending with 'dir_path' will be generated in the settings as directory browser
    """
    def __init__(self):
        UiOwner.__init__(self)
        QtWidgets.QDialog.__init__(self)
        self.ui = self.generate_ui()
        self._content = self._generate_content()
        self.ui.buttonBox.accepted.connect(self._ok_clicked)

    def generate_ui(self):
        from clients.gui.dialogs.settings_dialog.moc_settings_dialog import Ui_settings_dialog
        ui = Ui_settings_dialog()
        ui.setupUi(self)
        return ui

    def _generate_content(self):
        content = {}
        for i, field_name in enumerate(vars(user_settings.settings)):
            field_content = {}
            field_value = user_settings.settings.__getattribute__(field_name)
            readable_field_name = field_name.capitalize().replace('_', ' ')
            label = QLabel(self)
            label.setText(readable_field_name + ':')

            ui_edit = self._get_ui_for_type(field_value)

            self.ui.gridLayout_settings.addWidget(label, i, 0)
            self.ui.gridLayout_settings.addWidget(ui_edit, i, 1)
            field_content['label'] = label
            field_content['ui_edit'] = ui_edit
            field_content['readable_field_name'] = readable_field_name

            # TODO here add additional processing for manually defined attributes...
            if field_name.endswith('_path'):
                browse_button = QtWidgets.QPushButton(self)
                browse_button.setText('Browse')
                field_content['browse_button'] = browse_button
                self.ui.gridLayout_settings.addWidget(browse_button, i, 2)
                field_content['browse_button_callback'] = partial(self._browse_clicked, field_name)
                browse_button.clicked.connect(field_content['browse_button_callback'])

            content[field_name] = field_content

        return content

    def _browse_clicked(self, field_name: str):
        field_content = self._content[field_name]
        start_path = field_content['ui_edit'].text()
        select_title = f'Select {field_content["readable_field_name"]}...'
        new_path = None

        if field_name.endswith('_dir_path'):
            new_path = QtWidgets.QFileDialog.getExistingDirectory(
                self,
                select_title,
                start_path,
                QFileDialog.ShowDirsOnly)
        else:
            file_dialog = QFileDialog(
                self,
                select_title,
                start_path,
                "All (*)")
            if file_dialog.exec_():
                new_path = file_dialog.selectedFiles()[0]

        if new_path:
            field_content['ui_edit'].setText(new_path)

    def _get_ui_for_type(self, field_value):
        if isinstance(field_value, int):
            ui_edit = QtWidgets.QSpinBox(self)
            ui_edit.setValue(field_value)
        elif isinstance(field_value, float):
            ui_edit = QtWidgets.QDoubleSpinBox(self)
            ui_edit.setValue(field_value)
        elif isinstance(field_value, str):
            ui_edit = QtWidgets.QLineEdit(self)
            ui_edit.setText(field_value)
        elif isinstance(field_value, bool):
            ui_edit = QtWidgets.QCheckBox(self)
            ui_edit.setChecked(field_value)
        else:
            raise NotImplementedError(f'Settings Dialog - not implemented type "{type(field_value)}"')

        return ui_edit

    def _get_value_from_ui_edit(self, field_name, field_content, settings_value):
        ui_edit = field_content['ui_edit']
        if isinstance(settings_value, (int, float)):
            ui_value = ui_edit.getValue()
        elif isinstance(settings_value, str):
            ui_value = ui_edit.text()
        elif isinstance(settings_value, bool):
            ui_value = ui_edit.isChecked()
        else:
            raise NotImplementedError(f'Settings Dialog - not implemented type "{type(settings_value)}"')

        return ui_value

    def _ok_clicked(self):
        something_changed = False
        for field_name, field_content in self._content.items():
            settings_value = user_settings.settings.__getattribute__(field_name)
            ui_value = self._get_value_from_ui_edit(field_name=field_name,
                                                    field_content=field_content,
                                                    settings_value=settings_value)
            if ui_value != settings_value:
                setattr(user_settings.settings, field_name, ui_value)
                something_changed = True

        if something_changed:
            QMessageBox.warning(self, "Warning!", 'Some changes may be not visible until restart')


if __name__ == '__main__':
    import sys
    from clients.gui.gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = SettingsDialog()
    w.show()
    sys.exit(app.exec_())
