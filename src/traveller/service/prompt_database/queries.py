from traveller.prompts.locations.country_prompt import CountryPrompt


#location
REQUEST_GET_PROMPTS_COUNTRY = \
'''
SELECT *
FROM countries_prompts
WHERE name LIKE "%{text}%"
    OR iso_code LIKE "%{text}%"
ORDER BY name ASC
LIMIT {limit};
'''

REQUEST_GET_COUNTRY_BY_ISO_CODE = \
'''
SELECT *
FROM countries_prompts
WHERE iso_code = "{iso_code}"
'''



#admin regions major
REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR = \
'''
SELECT *
FROM admin_regions_major_prompts
WHERE ascii_name LIKE "%{text}%"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR_IN_COUNTRY = \
'''
SELECT *
FROM admin_regions_major_prompts
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''



#admin regions minor
REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR = \
'''
SELECT *
FROM admin_regions_minor_prompts
WHERE ascii_name LIKE "%{text}%"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_COUNTRY = \
'''
SELECT *
FROM admin_regions_minor_prompts
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_ADMIN_REGION_MINOR = \
'''
SELECT *
FROM admin_regions_minor_prompts
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
    AND geoname_admin1_code_parent = "{admin_region_major}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''



#cities
REQUEST_GET_PROMPTS_CITY_IN_COUNTRY = \
'''
SELECT *
FROM cities_prompts
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MAJOR = \
'''
SELECT *
FROM cities_prompts
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
    AND geoname_admin1_code = "{admin_region_major}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MINOR = \
'''
SELECT *
FROM cities_prompts
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
    AND geoname_admin1_code = "{admin_region_major}"
    AND geoname_admin2_code = "{admin_region_minor}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_CITY = \
'''
SELECT *
FROM cities_prompts
WHERE ascii_name LIKE "%{text}%"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

