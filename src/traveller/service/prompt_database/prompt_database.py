import sqlite3
from typing import List
from functools import lru_cache

from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.prompts.locations.country_prompt import CountryPrompt
from traveller.prompts.locations.city_prompt import CityPrompt
from traveller.prompts.locations.admin_region_major_prompt import AdminRegionMajorPrompt
from traveller.prompts.locations.admin_region_minor_prompt import AdminRegionMinorPrompt
import traveller.service.prompt_database.queries as pd_queries
from traveller.service.prompt_service import PromptService
from traveller.common import config


class PromptDatabase(PromptService):
    def __init__(self, database_path: str):
        super().__init__()        # if file cannot be opened (e.g. does not exists, raise IOError instead of creating empty prompt database)
        with open(database_path) as file:
            pass
        self._connection = sqlite3.connect(database_path)  # TODO open read only instead above?
        self._cursor = self._connection.cursor()

    def __del__(self):
        if hasattr(self, '_connection'):
            self._connection.close()

    @staticmethod
    def convert_prompt_to_underlying_type(prompt, base_class):
        required_names = base_class.get_database_column_names()
        del required_names[required_names.index('db_id')]
        init_dict = {key: prompt.__dict__[key] for key in required_names}
        return base_class(**init_dict)

    @lru_cache(maxsize=32)
    def get_country_prompts(self,
                            contained_text: str = '',
                            limit=config.PROMPTS_LIMIT) -> List[Country]:
        request = pd_queries.REQUEST_GET_PROMPTS_COUNTRY.format(
            text=contained_text, limit=limit)
        self._cursor.execute(request)
        all_data = self._cursor.fetchall()
        all_data_parsed = [self.convert_prompt_to_underlying_type(
            CountryPrompt.create_from_database_row(row), Country) for row in all_data]
        return all_data_parsed

    @lru_cache(maxsize=32)
    def get_city_prompts(self,
                         parent_location=None,
                         contained_text='',
                         limit=config.PROMPTS_LIMIT) -> List[City]:
        if parent_location is None:
            request = pd_queries.REQUEST_GET_PROMPTS_CITY.format(
                text=contained_text, limit=limit)
        elif isinstance(parent_location, Country):
            request = pd_queries.REQUEST_GET_PROMPTS_CITY_IN_COUNTRY.format(
                text=contained_text,
                country_code=parent_location.iso_code,
                limit=limit)
        elif isinstance(parent_location, AdminRegionMajor):
            request = pd_queries.REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MAJOR.format(
                text=contained_text,
                admin_region_major=parent_location.geoname_admin1_code,
                country_code=parent_location.country_code,
                limit=limit)
        elif isinstance(parent_location, AdminRegionMinor):
            request = pd_queries.REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MINOR.format(
                text=contained_text,
                admin_region_major=parent_location.geoname_admin1_code_parent,
                admin_region_minor=parent_location.geoname_admin2_code,
                country_code=parent_location.country_code,
                limit=limit)
        else:
            raise TypeError('Parent location for city not recognized')
        self._cursor.execute(request)
        all_data = self._cursor.fetchall()
        all_data_parsed = [self.convert_prompt_to_underlying_type(
            CityPrompt.create_from_database_row(row), City) for row in all_data]
        return all_data_parsed

    @lru_cache(maxsize=32)
    def get_admin_region_major_prompts(self,
                                       parent_location=None,
                                       contained_text='',
                                       limit=config.PROMPTS_LIMIT) -> List[AdminRegionMajor]:
        if parent_location is None:
            request = pd_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR.format(
                text=contained_text,
                limit=limit)
        elif isinstance(parent_location, Country):
            request = pd_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR_IN_COUNTRY.format(
                text=contained_text,
                country_code=parent_location.iso_code,
                limit=limit)
        else:
            raise TypeError('Parent location for admin major region not recognized')
        self._cursor.execute(request)
        all_data = self._cursor.fetchall()
        all_data_parsed = [self.convert_prompt_to_underlying_type(
            AdminRegionMajorPrompt.create_from_database_row(row), AdminRegionMajor) for row in all_data]
        return all_data_parsed

    @lru_cache(maxsize=32)
    def get_admin_region_minor_prompts(self,
                                       parent_location=None,
                                       contained_text='',
                                       limit=config.PROMPTS_LIMIT) -> List[AdminRegionMinor]:
        if parent_location is None:
            request = pd_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR.format(
                text=contained_text,
                limit=limit)
        elif isinstance(parent_location, Country):
            request = pd_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_COUNTRY.format(
                text=contained_text,
                country_code=parent_location.iso_code,
                limit=limit)
        elif isinstance(parent_location, AdminRegionMajor):
            request = pd_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_ADMIN_REGION_MINOR.format(
                text=contained_text,
                admin_region_major=parent_location.geoname_admin1_code,
                country_code=parent_location.country_code,
                limit=limit)
        else:
            raise TypeError('Parent location for admin minor region not recognized')
        self._cursor.execute(request)
        all_data = self._cursor.fetchall()
        all_data_parsed = [self.convert_prompt_to_underlying_type(
            AdminRegionMinorPrompt.create_from_database_row(row), AdminRegionMinor) for row in all_data]
        return all_data_parsed

    def get_country_by_iso_code(self, iso_code):
        request = pd_queries.REQUEST_GET_COUNTRY_BY_ISO_CODE.format(iso_code=iso_code)
        self._cursor.execute(request)
        rows = self._cursor.fetchall()
        if len(rows) == 0:
            return None
        return Country.create_from_database_row(rows[0])

    def get_parent_location_for_child(self, parent_type, child_location):
        if parent_type == Country:
            country = self.get_country_by_iso_code(iso_code=child_location.country_code)
            return self.convert_prompt_to_underlying_type(country, parent_type)
        else:
            if parent_type == AdminRegionMajor:
                if isinstance(child_location, AdminRegionMinor):
                    geoname_admin1_code = child_location.geoname_admin1_code_parent
                else:
                    geoname_admin1_code = child_location.geoname_admin1_code
                request = 'SELECT * FROM {table}\n' \
                          'where geoname_admin1_code = "{admin1}"\n' \
                          'AND country_code = "{iso_code}"'.format(
                    table=AdminRegionMajorPrompt.get_table_name(),
                    admin1=geoname_admin1_code,
                    iso_code=child_location.country_code)
                parent_prompt_type = AdminRegionMajorPrompt
            elif parent_type == AdminRegionMinor:
                request = 'SELECT * FROM {table}\n' \
                          'where geoname_admin1_code_parent = "{admin1}"\n' \
                          'AND geoname_admin2_code = "{admin2}"\n' \
                          'AND country_code = "{iso_code}"'.format(
                    table=AdminRegionMinorPrompt.get_table_name(),
                    admin1=child_location.geoname_admin1_code,
                    admin2=child_location.geoname_admin2_code,
                    iso_code=child_location.country_code)
                parent_prompt_type = AdminRegionMinorPrompt
            else:
                raise ValueError('Can not get parent location for type {}'.format(parent_type))

            self._cursor.execute(request)
            rows = self._cursor.fetchall()
            if len(rows) == 0:
                return None
            row_item = parent_prompt_type.create_from_database_row(rows[0])
            return self.convert_prompt_to_underlying_type(row_item, parent_type)


if __name__ == '__main__':
    pd = PromptDatabase(database_path='/home/przemek/Projects/traveller/src/traveller/prompts/data/location_prompts.sqlite')
    import time
    x = pd.get_admin_region_minor_prompts(limit=99999999)
    t = time.process_time()
    x = pd.get_admin_region_minor_prompts(limit=99999999)
    print(time.process_time() - t)
