import os

from typing import List
from traveller.items.standalone.photo import Photo
from traveller.items.visits.visit import Visit
from traveller.items.standalone.journey import Journey
from traveller.service.map.map_template import map_template
from traveller.common.custom_datetime import CustomDateTime


def _get_visits_text(visits: List[Visit], traveller_database):

    visits_with_count = {}
    for visit in visits:
        if visit.visited_item_id is None:
            continue
        visit_in_same_item_with_count = visits_with_count.get(visit.visited_item_id, None)  # type: tuple or None
        if visit_in_same_item_with_count is None:
            visit_in_same_item_with_count = {'last_visit': visit, 'count': 1}  # last visit and count of visits
        else:
            last_visit = visit_in_same_item_with_count['last_visit']  # type: Visit
            if last_visit.start_unix_time < visit.start_unix_time:
                visit_in_same_item_with_count['last_visit'] = visit
            visit_in_same_item_with_count['count'] += 1

        visits_with_count[visit.visited_item_id] = visit_in_same_item_with_count

    visits_strings = []
    for visited_item_id, visit_with_count in visits_with_count.items():
        last_visit = visit_with_count['last_visit']
        visited_item = traveller_database.get_item_by_item_id(visited_item_id)
        if not hasattr(visited_item, 'latitude'):
            continue

        if visited_item.latitude is None:
            return

        if last_visit.journey_db_id is not None:
            journey = traveller_database.get_item_by_db_id(item_type=Journey, db_id=last_visit.journey_db_id)
            journey_str = journey.get_short_description()
        else:
            journey_str = '-'

        visit_string = '''{{"lat": {lat}, "lon": {lon}, "item_type": "{item_type}", "desc": ["{name}", "{journey}", {visits_count}]}}''' \
            .format(
            lat=visited_item.latitude,
            lon=visited_item.longitude,
            item_type=visited_item.__class__.__name__[0].upper(),
            name=last_visit.get_short_description_with_engine(traveller_database),
            journey=journey_str,
            visits_count=visit_with_count['count'])
        visits_strings.append(visit_string)

    visits_str = '[\n' + ',\n'.join(visits_strings) + '\n]'
    return visits_str


def _get_images_text(images: List[Photo], traveller_database, output_dir):
    # {"lat": 52, "lon": 15, "item_type": 'P', "img": "kot.jpg", "desc": ['kot.jpg', 'Around World', '18.12.2018']},
    img_dir = os.path.join(output_dir, 'img')
    os.makedirs(img_dir, exist_ok=True)

    images_strings = []

    for i, image in enumerate(images):
        if image.latitude is None:
            continue

        if image.associated_item_id is not None:
            associated_item = traveller_database.get_item_by_item_id(item_id=image.associated_item_id )
            associated_item_str = associated_item.get_short_description_with_engine(traveller_database)
        else:
            associated_item_str = ''

        if image.photo_time is not None:
            date_str = CustomDateTime(image.photo_time).to_chronological_date_time_string()
        else:
            date_str = 'Unknown time'

        item_string = '''{{"lat": {lat}, "lon": {lon}, "item_type": "{item_type}", "img": "{img}", "desc": ["{assoc_item}", "{date}"]}}'''\
            .format(
               lat=image.latitude,
               lon=image.longitude,
               item_type='P',
               img=image.file_name,
               assoc_item=associated_item_str,
               date=date_str)
        images_strings.append(item_string)

        output_img_path = os.path.join(img_dir, image.file_name)
        with open(output_img_path, "wb") as file:
            file.write(image.thumbnail)

    images_str = '[\n' + ',\n'.join(images_strings) + '\n]'
    return images_str


def generate_map(
        traveller_database,
        images: List[Photo] = [],
        visits: List[Visit] = [],
        output_dir: str = './generated_map',
        map_name: str = 'traveller_map.html',
):
    os.makedirs(output_dir, exist_ok=True)
    output_html_file_path = os.path.join(output_dir, map_name)

    images_list_str = _get_images_text(images=images, traveller_database=traveller_database, output_dir=output_dir)
    visits_list_str = _get_visits_text(visits=visits, traveller_database=traveller_database)
    working_directory = '.'

    txt = map_template.format(images_list_str=images_list_str, visits_list_str=visits_list_str, working_directory=working_directory)
    with open(output_html_file_path, "w") as text_file:
        text_file.write(txt)
