from typing import List

from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor


class PromptService:
    def __init__(self):
        pass

    def get_country_prompts(self, contained_text: str, limit: int) -> List[Country]:
        raise NotImplementedError('Incomplete Prompt Service!')

    def get_city_prompts(self, parent_location, contained_text, limit: int) -> List[City]:
        raise NotImplementedError('Incomplete Prompt Service!')

    def get_admin_region_major_prompts(self, parent_location, contained_text, limit: int) -> List[AdminRegionMajor]:
        raise NotImplementedError('Incomplete Prompt Service!')

    def get_admin_region_minor_prompts(self, parent_location, contained_text, limit: int) -> List[AdminRegionMinor]:
        raise NotImplementedError('Incomplete Prompt Service!')

    def get_parent_location_for_child(self, parent_type, child_location):
        raise NotImplementedError('Incomplete Prompt Service!')

