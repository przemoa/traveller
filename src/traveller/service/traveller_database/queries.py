from traveller.items.item import Item


# countries
REQUEST_GET_PROMPTS_COUNTRY = \
'''
SELECT *
FROM countries
WHERE name LIKE "%{text}%"
    OR iso_code LIKE "%{text}%"
ORDER BY name ASC
LIMIT {limit};
'''

REQUEST_GET_COUNTRY_BY_ISO_CODE = \
'''
SELECT *
FROM countries
WHERE iso_code = "{iso_code}"
'''


#admin regions major
REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR_IN_COUNTRY = \
'''
SELECT *
FROM admin_regions_major
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR = \
'''
SELECT *
FROM admin_regions_major
WHERE ascii_name LIKE "%{text}%"
ORDER BY ascii_name ASC
LIMIT {limit};
'''




#admin regions minor
REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_COUNTRY = \
'''
SELECT *
FROM admin_regions_minor
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_ADMIN_REGION_MINOR = \
'''
SELECT *
FROM admin_regions_minor
WHERE ascii_name LIKE "%{text}%"
    AND admin_region_major_db_id = {admin_region_major}
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR = \
'''
SELECT *
FROM admin_regions_minor
WHERE ascii_name LIKE "%{text}%"
ORDER BY ascii_name ASC
LIMIT {limit};
'''



#cities
REQUEST_GET_PROMPTS_CITY_IN_COUNTRY = \
'''
SELECT *
FROM cities
WHERE ascii_name LIKE "%{text}%"
    AND country_code = "{country_code}"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MAJOR = \
'''
SELECT *
FROM cities
WHERE ascii_name LIKE "%{text}%"
    AND admin_region_major_db_id = {admin_region_major}
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MINOR = \
'''
SELECT *
FROM cities
WHERE ascii_name LIKE "%{text}%"
    AND admin_region_minor_db_id = {admin_region_minor}
ORDER BY ascii_name ASC
LIMIT {limit};
'''

REQUEST_GET_PROMPTS_CITY = \
'''
SELECT *
FROM cities
WHERE ascii_name LIKE "%{text}%"
ORDER BY ascii_name ASC
LIMIT {limit};
'''

##################################################
# Genearal reads

REQUEST_GET_ALL_ITEMS_FROM_DATABASE = \
'''
SELECT *
FROM {db_name};
'''

REQUEST_GET_ITEM_BY_ITEM_ID = \
'''
SELECT *
FROM {}
WHERE item_id = {{item_id}};
'''.format(Item.get_table_name())

REQUEST_GET_ITEM_ENTRY_BY_DB_ID = \
'''
SELECT *
FROM {}
WHERE db_id = {{db_id}}
    AND item_type = {{item_type}};
'''.format(Item.get_table_name())
