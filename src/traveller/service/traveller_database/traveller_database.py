import sqlite3
import os
import time
import datetime
from typing import List

from traveller.items.item import Item
from traveller.items.item_types import TYPES_OF_ITEMS_IN_DATABASE, PROMPTS_ONLY_TYPES, INV_TYPES_OF_ITEMS_IN_DATABASE
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.items.standalone.journey import Journey
from traveller.items import item_types
import traveller.service.traveller_database.queries as td_queries
from traveller.service.prompt_service import PromptService
import traveller.common.config as config
from traveller.common.types import TravellerException, ItemAlreadyInDatabaseError
from traveller.items.spots.locations.location_selection import LocationSelection


class TravellerDatabase(PromptService):
    def __init__(self, database_path: str):
        super().__init__()
        self._create_database_if_not_existing(database_path)
        self._connection = sqlite3.connect(database_path)
        self._cursor = self._connection.cursor()  # remove "sqlite3.OperationalError: disk I/O error"
        self._cursor.execute('PRAGMA journal_mode = OFF')

    def __del__(self):
        if hasattr(self, '_connection'):
            self._connection.commit()
            self._connection.close()

    @staticmethod
    def _create_database_if_not_existing(database_path):
        if os.path.isfile(database_path):
            return
        TravellerDatabase._create_database(database_path)

    @staticmethod
    def _create_database(database_path):
        connection = sqlite3.connect(database_path)
        cursor = connection.cursor()
        cursor.execute('PRAGMA journal_mode = OFF')
        for item_class in item_types.TYPES_OF_ITEMS_IN_DATABASE:
            item_class.create_table(db_cursor=cursor)
        Item.create_table(db_cursor=cursor)
        TravellerDatabase._add_global_items_to_new_database(cursor=cursor)
        connection.commit()
        connection.close()

    @classmethod
    def _add_global_items_to_new_database(cls, cursor):
        """ Adds items, which always should be in database. Added only during database creation.
        e.g. 'unspecified' journey
        """
        unspecified_journey = Journey(name=Journey.UNSPECIFIED_JOURNEY_NAME)
        cls._add_item_and_item_entry_for_it_to_database(item=unspecified_journey, cursor=cursor)
        assert unspecified_journey.db_id == Journey.UNSPECIFIED_JOURNEY_DB_ID

    def get_country_prompts(self,
                            contained_text,
                            limit=config.PROMPTS_LIMIT) -> List[Country]:
        request = td_queries.REQUEST_GET_PROMPTS_COUNTRY.format(
            text=contained_text,
            limit=limit)
        return self._execute_request_and_parse_to_item_list(request, Country)

    def get_admin_region_major_prompts(self,
                                       parent_location=None,
                                       contained_text='',
                                       limit=config.PROMPTS_LIMIT) -> List[AdminRegionMajor]:
        if parent_location is None:
            request = td_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR.format(
                text=contained_text,
                limit=limit)
        elif parent_location.db_id is None:
            return []
        elif isinstance(parent_location, Country):
            request = td_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MAJOR_IN_COUNTRY.format(
                text=contained_text,
                country_code=parent_location.iso_code,
                limit=limit)
        else:
            raise TravellerException('Parent location for admin major region not recognized ({})'.format(type(parent_location)))
        return self._execute_request_and_parse_to_item_list(request, AdminRegionMajor)

    def get_admin_region_minor_prompts(self,
                                       parent_location=None,
                                       contained_text='',
                                       limit=config.PROMPTS_LIMIT) -> List[AdminRegionMinor]:
        if parent_location is None:
            request = td_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR.format(
                text=contained_text,
                limit=limit)
        elif parent_location.db_id is None:
            return []
        elif isinstance(parent_location, Country):
            request = td_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_COUNTRY.format(
                text=contained_text,
                country_code=parent_location.iso_code,
                limit=limit)
        elif isinstance(parent_location, AdminRegionMajor):
            if not parent_location.country_code:
                raise TravellerException('Invalid AdminRegionMajor')
            request = td_queries.REQUEST_GET_PROMPTS_ADMIN_REGION_MINOR_IN_ADMIN_REGION_MINOR.format(
                text=contained_text,
                admin_region_major=parent_location.db_id,
                country_code=parent_location.country_code,
                limit=limit)
        else:
            raise TravellerException('Parent location for admin minor region not recognized')
        return self._execute_request_and_parse_to_item_list(request, AdminRegionMinor)

    def get_city_prompts(self,
                         parent_location=None,
                         contained_text='',
                         limit=config.PROMPTS_LIMIT) -> List[City]:
        if parent_location is None:
            request = td_queries.REQUEST_GET_PROMPTS_CITY.format(
                text=contained_text,
                limit=limit)
        elif parent_location.db_id is None:
            return []
        elif isinstance(parent_location, Country):
            request = td_queries.REQUEST_GET_PROMPTS_CITY_IN_COUNTRY.format(
                text=contained_text,
                country_code=parent_location.iso_code,
                limit=limit)
        elif isinstance(parent_location, AdminRegionMajor):
            if not parent_location.country_code:
                raise TravellerException('Invalid AdminRegionMajor')
            request = td_queries.REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MAJOR.format(
                text=contained_text,
                admin_region_major=parent_location.db_id,
                country_code=parent_location.country_code,
                limit=limit)
        elif isinstance(parent_location, AdminRegionMinor):
            if not parent_location.country_code or not parent_location.admin_region_major_db_id:
                raise TravellerException('Invalid AdminRegionMinor')
            request = td_queries.REQUEST_GET_PROMPTS_CITY_IN_ADMIN_MINOR.format(
                text=contained_text,
                admin_region_major=parent_location.admin_region_major_db_id,
                admin_region_minor=parent_location.db_id,
                country_code=parent_location.country_code,
                limit=limit)
        else:
            raise TravellerException('Parent location for city not recognized')
        return self._execute_request_and_parse_to_item_list(request, City)

    def flush(self):
        self._connection.commit()

    def _add_item_to_database(self, item, raise_for_duplicate):
        if type(item) not in TYPES_OF_ITEMS_IN_DATABASE:
            raise TravellerException('Can not write item do database. Invalid type of item')
        if type(item) in PROMPTS_ONLY_TYPES:
            raise TravellerException('Can not write item do database, item is prompt only')
        if item.db_id is not None:
            raise TravellerException('Can not write item do database,'
                             ' item contains db_id so probably is already written to database')
        if item.item_id is not None:
            raise TravellerException('Can not write item do database,'
                             ' item contains item_id so probably is already written to database')

        # TODO validate, that item does not exist in database. If exists skip add, just set id
        # TODO add flag for this, because it can be time consuming
        existing_item = item.get_same_element_from_database_if_exists(self._cursor)
        if existing_item is not None:
            if raise_for_duplicate:
                raise ItemAlreadyInDatabaseError('Item already exists in database')
            else:
                item.db_id = existing_item.db_id
                item.item_id = existing_item.item_id
                return

        self._add_item_and_item_entry_for_it_to_database(item=item, cursor=self._cursor)

    @staticmethod
    def _add_item_and_item_entry_for_it_to_database(item, cursor):
        item.modification_time = int(datetime.datetime.now().timestamp())
        item.creation_time = item.modification_time
        item.db_id = item.add_to_database(cursor)
        entry_in_table_of_all_items = Item(
            db_id=item.db_id,
            item_type=TYPES_OF_ITEMS_IN_DATABASE[item.__class__])
        item.item_id = entry_in_table_of_all_items.add_to_database(cursor)

    def add_country(self,
                    country: Country,
                    raise_for_duplicate: bool = True):
        self._add_item_to_database(country, raise_for_duplicate)

    def add_admin_region_major(self,
                               admin_region_major: AdminRegionMajor,
                               country: Country,
                               raise_for_duplicate: bool = True):
        if country.db_id is None:
            self._add_item_to_database(country, raise_for_duplicate)
        admin_region_major.country_code = country.iso_code
        self._add_item_to_database(admin_region_major, raise_for_duplicate)

    def add_admin_region_minor(self,
                               admin_region_minor: AdminRegionMinor,
                               admin_region_major: AdminRegionMajor,
                               country: Country,
                               raise_for_duplicate: bool = True):
        if admin_region_major.db_id is None:
            self.add_admin_region_major(
                admin_region_major=admin_region_major,
                country=country,
                raise_for_duplicate=raise_for_duplicate)
        admin_region_minor.country_code = admin_region_major.country_code
        admin_region_minor.admin_region_major_db_id = admin_region_major.db_id
        self._add_item_to_database(admin_region_minor, raise_for_duplicate)

    def add_city(self,
                 city: City,
                 admin_region_minor: AdminRegionMinor,
                 admin_region_major: AdminRegionMajor,
                 country: Country,
                 raise_for_duplicate: bool = True):
        if admin_region_minor is not None:
            if admin_region_minor.db_id is None:
                self.add_admin_region_minor(admin_region_minor=admin_region_minor,
                                            admin_region_major=admin_region_major,
                                            country=country,
                                            raise_for_duplicate=raise_for_duplicate)
            city.country_code = admin_region_minor.country_code
            city.admin_region_major_db_id = admin_region_minor.admin_region_major_db_id
            city.admin_region_minor_db_id = admin_region_minor.db_id
        elif admin_region_major is not None:
            if admin_region_major.db_id is None:
                self.add_admin_region_major(
                    admin_region_major=admin_region_major,
                    country=country,
                    raise_for_duplicate=raise_for_duplicate)
            city.country_code = admin_region_major.country_code
            city.admin_region_major_db_id = admin_region_major.db_id
            city.admin_region_minor_db_id = None
        else:
            if country.db_id is None:
                self._add_item_to_database(country, raise_for_duplicate)
            city.country_code = country.iso_code
            city.admin_region_major_db_id = None
            city.admin_region_minor_db_id = None

        self._add_item_to_database(city, raise_for_duplicate)

    def update_item(self, item: Item):
        item.modification_time = int(datetime.datetime.now().timestamp())
        item.update_in_database(db_cursor=self._cursor)

    def delete_item(self, item: Item):
        # raise NotImplementedError # TODO
        if type(item) not in TYPES_OF_ITEMS_IN_DATABASE:
            raise TravellerException('Can not delete item in database. Invalid type of item')
        item.delete_from_databse(db_cursor=self._cursor)
        # TODO remove also from items table?.
        # How to handle this correctly? Remove all references or leave,
        # and during reading in future show warning,
        # that some related item was deleted?

    def get_item_id_for_item(self, item):
        if item.item_id is not None:
            return
        if item.db_id is None:
            raise TravellerException('Can not get item id, because db_id is not known')
        if item.__class__ not in TYPES_OF_ITEMS_IN_DATABASE:
            raise TravellerException('Can not het item id. Invalid type of item')
        type_id = TYPES_OF_ITEMS_IN_DATABASE[item.__class__]
        request = td_queries.REQUEST_GET_ITEM_ENTRY_BY_DB_ID.format(
            db_id=item.db_id,
            item_type=type_id)
        self._cursor.execute(request)
        rows = self._cursor.fetchall()
        if len(rows) == 0:
            raise TravellerException('Item with specified item_id does not exist in database!')
        assert len(rows) == 1
        item_entry = Item.create_from_database_row(rows[0])
        item.item_id = item_entry.item_id

    def get_item_by_db_id(self, item_type, db_id):
        if item_type not in TYPES_OF_ITEMS_IN_DATABASE:
            raise TravellerException('Can not read item from database. Invalid type of item')
        if db_id is None:
            raise TravellerException('Can not read item from database. Invalid item id')
        return item_type.get_from_table_by_db_id(db_cursor=self._cursor, db_id=db_id)

    def get_item_by_item_id(self, item_id) -> Item:
        request = td_queries.REQUEST_GET_ITEM_BY_ITEM_ID.format(item_id=item_id)
        self._cursor.execute(request)
        rows = self._cursor.fetchall()
        if len(rows) == 0:
            raise TravellerException('Item with specified item_id does not exist in database!')
        assert len(rows) == 1
        item_entry = Item.create_from_database_row(rows[0])
        item_type = INV_TYPES_OF_ITEMS_IN_DATABASE[item_entry.item_type]
        item = self.get_item_by_db_id(item_type=item_type, db_id=item_entry.db_id)
        item.item_id = item_id
        return item

    def get_country_by_iso_code(self, iso_code):
        request = td_queries.REQUEST_GET_COUNTRY_BY_ISO_CODE.format(iso_code=iso_code)
        self._cursor.execute(request)
        row = self._cursor.fetchall()[0]
        return Country.create_from_database_row(row)

    def get_parent_location_for_child(self, parent_type, child_location):
        if parent_type == Country:
            return self.get_country_by_iso_code(iso_code=child_location.country_code)
        elif parent_type == AdminRegionMajor:
            db_id = child_location.admin_region_major_db_id
            return self.get_item_by_db_id(AdminRegionMajor, db_id)
        elif parent_type == AdminRegionMinor:
            db_id = child_location.admin_region_minor_db_id
            return self.get_item_by_db_id(AdminRegionMinor, db_id)
        raise TravellerException('Can not get parent location for type {}'.format(parent_type))

    def read_item_id(self, item: Item) -> int:
        """
        Returns id (position) of item (e.g. City) in table of all items
        :param item:
        :return:
        """
        if item.db_id is None:
            raise TravellerException('Item does not contain db_id!')
        if item.item_id is not None:
            return item.item_id
        raise NotImplementedError

    def _execute_request_and_parse_to_item_list(self, request, item_type: type) -> List[Item]:
        self._cursor.execute(request)
        all_data = self._cursor.fetchall()
        return [item_type.create_from_database_row(row) for row in all_data]

    def _get_all_items_of_type(self, item_type) -> List[Item]:
        request = td_queries.REQUEST_GET_ALL_ITEMS_FROM_DATABASE.\
            format(db_name=item_type.get_table_name())
        return self._execute_request_and_parse_to_item_list(request, item_type)

    # TODO remove or add filters?
    def get_journeys(self) -> List[Journey]:
        return self._get_all_items_of_type(Journey)

    # TODO add generic filters (filed-value)
    def get_all_items_of_type(self, item_type: type) -> List[Item]:
        return self._get_all_items_of_type(item_type=item_type)

    def add_item(self, item: Item, raise_for_duplicate: bool = True):
        self._add_item_to_database(item, raise_for_duplicate)

    # TODO remove?
    def add_journey(self, journey: Journey, raise_for_duplicate: bool = True):
        self._add_item_to_database(journey, raise_for_duplicate)

    def add_selected_location_to_database_if_not_existing(self, selected_locations: LocationSelection):
        country = None
        admin_major = None
        admin_minor = None
        if isinstance(selected_locations['country'], Country):
            country = selected_locations['country']
            if country.db_id is None:
                self.add_country(country)
        if isinstance(selected_locations['admin_major'], AdminRegionMajor):
            admin_major = selected_locations['admin_major']
            if admin_major.db_id is None:
                self.add_admin_region_major(
                    admin_region_major=admin_major,
                    country=country)
        if isinstance(selected_locations['admin_minor'], AdminRegionMinor):
            admin_minor = selected_locations['admin_minor']
            if admin_minor.db_id is None:
                self.add_admin_region_minor(
                    admin_region_minor=admin_minor,
                    admin_region_major=admin_major,
                    country=country)
        if isinstance(selected_locations['city'], City):
            city = selected_locations['city']
            if city.db_id is None:
                self.add_city(
                    city=city,
                    admin_region_minor=admin_minor,
                    admin_region_major=admin_major,
                    country=country)

    def get_location_selection_from_youngest_selected_location_id(self, youngest_selection_item_id: int) -> LocationSelection:
        if youngest_selection_item_id is None:
            return LocationSelection()

        location_selection = LocationSelection()
        youngest_location = self.get_item_by_item_id(youngest_selection_item_id)
        if isinstance(youngest_location, City):
            location_selection.city = youngest_location
            if youngest_location.admin_region_minor_db_id:
                admin_region_minor = self.get_item_by_db_id(
                    item_type=AdminRegionMinor,
                    db_id=youngest_location.admin_region_minor_db_id)
                youngest_location = admin_region_minor
            else:
                admin_region_major = self.get_item_by_db_id(
                    item_type=AdminRegionMajor,
                    db_id=youngest_location.admin_region_major_db_id)
                youngest_location = admin_region_major
        if isinstance(youngest_location, AdminRegionMinor):
            location_selection.admin_minor = youngest_location
            admin_region_major = self.get_item_by_db_id(
                item_type=AdminRegionMajor,
                db_id=youngest_location.admin_region_major_db_id)
            youngest_location = admin_region_major
        if isinstance(youngest_location, AdminRegionMajor):
            location_selection.admin_major = youngest_location
            country = self.get_country_by_iso_code(
                iso_code=youngest_location.country_code)
            youngest_location = country
        if isinstance(youngest_location, Country):
            location_selection.country = youngest_location
        return location_selection


if __name__ == '__main__':
    td = TravellerDatabase("/home/przemek/Projects/traveller/src/traveller/tmp2.sqlite")
    del td
