import time
import datetime


class CustomDateTime:
    def __init__(self, unix_time: int):
        self.unix_time = unix_time  # seconds since 1970

    @classmethod
    def from_calendar_time(cls, year):
        raise NotImplementedError
        return cls()

    @classmethod
    def from_datetime(cls, date_time: datetime.datetime):
        if date_time is None:
            unix_time = None
        else:
            unix_time = int(time.mktime(date_time.timetuple()))
        return cls(unix_time=unix_time)

    def to_datetime(self):
        if self.unix_time is None:
            return None
        return datetime.datetime.fromtimestamp(self.unix_time)

    def to_date_string(self):
        return '{0:%d.%m-%Y}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_time_string(self):
        return '{0:%H:%M:%S}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_date_time_string(self):
        return '{0:%d.%m-%Y %H:%M}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_chronological_date_time_string(self):
        return '{0:%Y.%m.%d %H:%M:%S}'.format(datetime.datetime.fromtimestamp(self.unix_time))


if __name__ == '__main__':
    print(CustomDateTime(31536000).to_chronological_date_time_string())
