class SqliteSerializable:
    @classmethod
    def get_database_column_names(cls):
        column_names_and_types = cls.get_database_column_names_and_types()
        return column_names_and_types[::2]

    @classmethod
    def create_from_database_row(cls, row_tuple):
        column_names = cls.get_database_column_names()
        kwargs = {key: row_tuple[i] for i, key in enumerate(column_names)}
        return cls(**kwargs)

    def validate_before_adding_to_database(self):
        raise NotImplementedError('Incomplete SqliteSerializable object!')

    @staticmethod
    def get_table_name():
        raise NotImplementedError('Incomplete SqliteSerializable object!')

    @staticmethod
    def get_database_column_names_and_types():
        """
        This method have to return list of names and types of data for them.
        Name of variable have to be same as name of class member
        """
        raise NotImplementedError('Incomplete SqliteSerializable object!')

    @classmethod
    def create_table(cls, db_cursor):
        column_names_and_types = cls.get_database_column_names_and_types()
        bracket_placholders = '{} {},' * (len(column_names_and_types)//2 - 1) + '{} {}'
        header = bracket_placholders.format(*column_names_and_types)
        db_cursor.execute('CREATE TABLE {table_name} ({data})'.format(
            table_name=cls.get_table_name(),
            data=header))

    def get_same_element_from_database_if_exists(self, db_cursor):
        # TODO override this check in child classses and search in database instead of gettig whole
        request = 'SELECT * FROM {}'.format(self.get_table_name())
        db_cursor.execute(request)
        rows = db_cursor.fetchall()
        for row in rows:
            row_item = self.__class__.create_from_database_row(row)
            if row_item == self:
                return row_item
        return None

    def add_to_database(self, db_cursor) -> int:
        self.validate_before_adding_to_database()
        column_names = self.get_database_column_names()
        question_marks = '?,' * (len(column_names) - 1) + '?'
        request_txt = 'INSERT INTO {table_name} VALUES ({question_marks})'.format(
            table_name=self.get_table_name(),
            question_marks=question_marks)
        db_cursor.execute(request_txt, [self.__dict__[key] for key in column_names])
        return self._get_last_inserted_item_id(db_cursor)

    def update_in_database(self, db_cursor):
        assert self.db_id is not None
        column_names = self.get_database_column_names()
        item_values_txt = ''
        vals = []
        for i, column_name in enumerate(column_names):
            single_item = self.__dict__[column_name]
            item_values_txt += column_name + '=?,'
            vals.append(single_item)
        item_values_txt = item_values_txt[:-1]  # remove coma
        request_txt = 'UPDATE {}\n' \
                  'SET {}\n' \
                  'WHERE db_id={}'.format(self.get_table_name(), item_values_txt, self.db_id)
        db_cursor.execute(request_txt, vals)

    def check_if_allowed_to_delete(self):
        return True

    def delete_from_databse(self, db_cursor):
        if not self.check_if_allowed_to_delete():
            raise ValueError('Item can not be deleted from database!')

        assert self.db_id is not None
        request = 'DELETE FROM {}\n' \
                  'WHERE db_id={}'.format(self.get_table_name(), self.db_id)
        db_cursor.execute(request)

    def _get_last_inserted_item_id(self, db_cursor):
        request = 'SELECT last_insert_rowid()'
        db_cursor.execute(request)
        id_in_database = db_cursor.fetchall()[0][0]
        return id_in_database

    @classmethod
    def get_from_table_by_db_id(cls, db_cursor, db_id: int):
        request = 'SELECT * FROM {}\n' \
                  'where db_id = {}'.format(cls.get_table_name(), db_id)
        db_cursor.execute(request)
        row = db_cursor.fetchall()[0]
        row_item = cls.create_from_database_row(row)
        return row_item




