
class TravellerException(Exception):
    pass


class ItemAlreadyInDatabaseError(TravellerException):
    pass
