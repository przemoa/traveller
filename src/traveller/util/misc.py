import hashlib
import datetime


def int_or_none_from_string(txt: str):
    if txt:
        return int(txt)
    else:
        return None


def int_from_string_or_string_hash_or_none(txt: str):
    try:
        return int_or_none_from_string(txt)
    except ValueError:
        hash_value = 0
        for i, c in enumerate(txt):
            hash_value += ord(c) * (255**i)
        return hash_value


def is_different_day(date_time_1: datetime.datetime, date_time_2: datetime.datetime) -> bool:
    """Checks if day of to date_time objects is same. Objects can be also None

    :param date_time_1:
    :param date_time_2:
    :return:
    """
    if date_time_1 is not None and date_time_2 is not None:
        if date_time_1.date() != date_time_2.date():
            return True
    elif date_time_2 is not None or date_time_1 is not None:
        return True

    return False
