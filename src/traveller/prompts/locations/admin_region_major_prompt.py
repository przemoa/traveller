from traveller.items.spots.locations.admin_region_major import AdminRegionMajor


class AdminRegionMajorPrompt(AdminRegionMajor):
    """ Used for prompting admin region major
    Almost same as base class, but to avoid feature misunderstandings,
    column with id name changed, as well as database name
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'admin_regions_major_prompts'
    # DATABASE ACCESS END
