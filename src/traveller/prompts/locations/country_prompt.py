from traveller.items.spots.locations.country import Country


class CountryPrompt(Country):
    """ Used for prompting countries
    Almost same as base class, but to avoid feature misunderstandings,
    column with id name changed, as well as database name
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'countries_prompts'
    # DATABASE ACCESS END
