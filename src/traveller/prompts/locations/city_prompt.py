from traveller.items.spots.locations.city import City


class CityPrompt(City):
    """ Used for prompting cities
    Almost same as base class, but to avoid feature misunderstandings,
    column with id name changed, as well as database name
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'cities_prompts'

    def validate_before_adding_to_database(self):
        pass

    @staticmethod
    def get_database_column_names_and_types():
        base_column_names_and_types = City.get_database_column_names_and_types()
        index_of_admin_region_major_db_id = base_column_names_and_types.index('admin_region_major_db_id')
        del base_column_names_and_types[index_of_admin_region_major_db_id] # delete column name
        del base_column_names_and_types[index_of_admin_region_major_db_id] # and type for it
        index_of_admin_region_minor_db_id = base_column_names_and_types.index('admin_region_minor_db_id')
        del base_column_names_and_types[index_of_admin_region_minor_db_id] # delete column name
        del base_column_names_and_types[index_of_admin_region_minor_db_id] # and type for it
        return base_column_names_and_types
    # DATABASE ACCESS END
