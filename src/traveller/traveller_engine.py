from typing import List

from traveller.service.prompt_database.prompt_database import PromptDatabase
from traveller.service.traveller_database.traveller_database import TravellerDatabase
from traveller.common import config

from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.location import Location
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.city import City
from traveller.service.prompt_service import PromptService


class PromptSourceType:
    ALL_AVAILABLE, TRAVELLER_DATABASE, PROMPT_DATABASE = range(3)


class TravellerEngine(PromptService):
    """

    if user input (e.g. database path) is invalid ,traveller user (e.g. GUI) is responsible for it,
    so do not handle exceptions, leave it for user
    """
    def __init__(self,
                 traveller_database_path: str,
                 prompts_database_path: str = None):
        super().__init__()
        self._prompts_database = self._open_prompts_database(prompts_database_path)
        self._traveller_database = TravellerDatabase(traveller_database_path)

    def get_traveller_database(self):
        return self._traveller_database

    def _get_prompt_services_for_type(self, prompt_type: PromptSourceType):
        if prompt_type == PromptSourceType.ALL_AVAILABLE:
            return [self._traveller_database, self._prompts_database]
        elif prompt_type == PromptSourceType.PROMPT_DATABASE:
            return [self._prompts_database]
        elif prompt_type == PromptSourceType.TRAVELLER_DATABASE:
            return [self._traveller_database]
        else:
            raise Exception('')

    @staticmethod
    def _open_prompts_database(prompts_database_path: str):
        if not prompts_database_path:
            return None
        return PromptDatabase(prompts_database_path)

    # API starts here
    def get_country_prompts(self,
                            contained_text: str = '',
                            limit: int = config.PROMPTS_LIMIT,
                            prompt_type: PromptSourceType=PromptSourceType.ALL_AVAILABLE) -> List[Country]:
        """ Get countries containing some text, so it will be easy to choose apropriate.
        If item is from prompting database or from traveller database can be determined checking item id

        :param contained_text: text, which have to be in location name or iso_code
        :return: list of matched countries, both from Traveller and prompts database
        """
        all_prompts = []
        for ps in self._get_prompt_services_for_type(prompt_type):
            all_prompts += ps.get_country_prompts(
                contained_text=contained_text,
                limit=limit)
        return all_prompts[:limit]

    def get_admin_region_major_prompts(self,
                         parent_location: Location = None,
                         contained_text: str = '',
                         limit: int = config.PROMPTS_LIMIT,
                         prompt_type: PromptSourceType=PromptSourceType.ALL_AVAILABLE) -> List[AdminRegionMajor]:
        all_prompts = []
        for ps in self._get_prompt_services_for_type(prompt_type):
            all_prompts += ps.get_admin_region_major_prompts(
                contained_text=contained_text,
                parent_location=parent_location,
                limit=limit)
        return all_prompts[:limit]

    def get_admin_region_minor_prompts(self,
                                       parent_location: Location = None,
                                       contained_text: str = '',
                                       limit: int = config.PROMPTS_LIMIT,
                                       prompt_type: PromptSourceType=PromptSourceType.ALL_AVAILABLE) -> List[AdminRegionMinor]:
        all_prompts = []
        for ps in self._get_prompt_services_for_type(prompt_type):
            all_prompts += ps.get_admin_region_minor_prompts(
                contained_text=contained_text,
                parent_location=parent_location,
                limit=limit)
        return all_prompts[:limit]

    def get_city_prompts(self,
                         parent_location: Location = None,
                         contained_text: str = '',
                         limit: int = config.PROMPTS_LIMIT,
                         prompt_type: PromptSourceType=PromptSourceType.ALL_AVAILABLE) -> List[City]:
        """

        :param parent_location: Location, witch is parent for searched city,
        that is Country od adminRegionX class instance
        :param contained_text:
        :return:
        """
        all_prompts = []
        for ps in self._get_prompt_services_for_type(prompt_type):
            all_prompts += ps.get_city_prompts(
                contained_text=contained_text,
                parent_location=parent_location,
                limit=limit)
        return all_prompts[:limit]

    def get_parent_location_for_child(self, parent_type, child_location):
        if child_location.db_id is not None:
            return self._traveller_database.get_parent_location_for_child(parent_type, child_location)
        else:
            return self._prompts_database.get_parent_location_for_child(parent_type, child_location)


    def __getattr__(self, name):
        #by default, redirect all unknown methods to traveller_database
        func = getattr(self._traveller_database, name)
        return func


if __name__ == '__main__':
    engine = TravellerEngine(
        "/home/przemek/Projects/traveller/src/traveller/tmp.sqlite",
        '/home/przemek/Projects/traveller/src/traveller/prompts/data/location_prompts.sqlite')

    country = engine.get_country_prompts('pl')[0]
    print(country)

    engine.get_item_by_db_id(5, 6)


    # admin_region_1_A = engine.get_admin_region_major_prompts(contained_text="LUB")
    # print(*admin_region_1_A, sep='\n')


