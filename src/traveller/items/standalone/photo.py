import exifread
from typing import Tuple
import datetime
import os
from PIL import Image
import io

from traveller.items.item import Item
from traveller.common.custom_datetime import CustomDateTime
from traveller.util import misc
from traveller.common import config


class Photo(Item):
    """
    """

    def __init__(self,
                 db_id: int = None,
                 file_name: str = '',
                 latitude: float = None,
                 longitude: float = None,
                 camera_model: str = '',
                 photo_time: int = None,
                 photo_time_from_metadata: int = None,
                 directory_name: str = '',
                 thumbnail: bytes = None,
                 associated_item_id: int = None,
                 original_path: str = '',
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        super().__init__(note=note, modification_time=modification_time, creation_time=creation_time)
        self.db_id = db_id
        self.file_name = file_name
        self.latitude = latitude
        self.longitude = longitude
        self.camera_model = camera_model
        self.photo_time = photo_time  # When photo was taken. Can be changed manually
        self.photo_time_from_metadata = photo_time_from_metadata  # Datetime read from image file
        self.directory_name = directory_name
        self.thumbnail = thumbnail
        self.associated_item_id = associated_item_id  # e.g. visit or journey
        self.original_path = original_path

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'photos'

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'file_name', 'TEXT',
            'latitude', 'FLOAT',
            'longitude', 'FLOAT',
            'camera_model', 'TEXT',
            'photo_time', 'INTEGER',
            'photo_time_from_metadata', 'INTEGER',
            'directory_name', 'TEXT',
            'thumbnail', 'BLOB',
            'associated_item_id', 'INTEGER',
        ]

    def validate_before_adding_to_database(self):
        if self.item_id is not None or self.db_id is not None \
                or self.thumbnail is None or self.file_name is None \
                or self.directory_name is None:
            raise ValueError('Can not write Photo to database, invalid object!')

    def get_same_element_from_database_if_exists(self, db_cursor):
        request = 'SELECT * FROM {} WHERE file_name = "{}"'.format(self.get_table_name(), self.file_name)
        db_cursor.execute(request)
        rows = db_cursor.fetchall()
        for row in rows:
            row_item = self.__class__.create_from_database_row(row)
            if row_item == self:
                return row_item
        return None
    # DATABASE ACCESS END

    def __eq__(self, other):
        # Photo is same, if it has same file name and was taken on the same day
        if isinstance(other, Photo):
            if self.file_name == other.file_name:
                self_date_time = CustomDateTime(self.photo_time).to_datetime()
                other_date_time = CustomDateTime(other.photo_time).to_datetime()
                is_same_day = not misc.is_different_day(self_date_time, other_date_time)
                return is_same_day
            return False
        return False

    def __hash__(self):
        return id(self)

    @classmethod
    def create_from_file(cls, file_path: str):
        with open(file_path, 'rb') as f:
            tags = exifread.process_file(f)

        latitude, longitude = cls.extract_coordinates_from_exti_tags(tags)
        if 'Image Model' in tags:
            camera_model = tags['Image Model'].values
        else:
            camera_model = ''
        photo_time_from_metadata = cls.extract_unix_time_from_exti_tags(tags)
        photo_time = photo_time_from_metadata
        file_name = os.path.basename(file_path)
        directory_name = os.path.basename(os.path.dirname(file_path))
        thumbnail = cls._create_thumbnail(file_path)
        return cls(latitude=latitude,
                   longitude=longitude,
                   camera_model=camera_model,
                   photo_time_from_metadata=photo_time_from_metadata,
                   photo_time=photo_time,
                   file_name=file_name,
                   directory_name=directory_name,
                   thumbnail=thumbnail,
                   original_path=file_path)

    @staticmethod
    def _create_thumbnail(file_path) -> bytes:
        im = Image.open(file_path)
        im.thumbnail((config.THUMBNAIL_SIZE_IN_PIXELS, config.THUMBNAIL_SIZE_IN_PIXELS))
        bytes_io = io.BytesIO()
        im.save(bytes_io, format='JPEG')
        bytes_array = bytes_io.getvalue()
        return bytes_array

    @staticmethod
    def extract_coordinates_from_exti_tags(tags) -> Tuple[int, int]:
        latitude = None
        longitude = None

        def deg_min_sec_to_float(vals: list) -> float:
            deg = vals[0].num / vals[0].den
            min = vals[1].num / vals[1].den
            sec = vals[2].num / vals[2].den
            return deg + min / 60 + sec / 3600

        if 'GPS GPSLongitude' in tags:
            value = deg_min_sec_to_float(tags['GPS GPSLongitude'].values)
            if tags['GPS GPSLongitude'].values == 'E':
                value = -value
            longitude = value
        if 'GPS GPSLatitude' in tags:
            value = deg_min_sec_to_float(tags['GPS GPSLatitude'].values)
            if tags['GPS GPSLatitudeRef'].values == 'S':
                value = -value
            latitude = value
        return latitude, longitude

    @staticmethod
    def extract_unix_time_from_exti_tags(tags) -> datetime.datetime:
        if 'Image DateTime' in tags:
            txt = tags['Image DateTime'].values
            return CustomDateTime.from_datetime(datetime.datetime.strptime(txt, '%Y:%m:%d %H:%M:%S')).unix_time
        else:
            return None

    def get_photo_time_as_full_string(self) -> str:
        if self.photo_time is None:
            return ''
        return CustomDateTime(self.photo_time).to_chronological_date_time_string()


if __name__ == '__main__':
    photo = Photo.create_from_file(file_path='/home/przemek/Projects/traveller/tests/test_files/img/IMG_20180812_131416.jpg')
    import sqlite3

    # TODO move to tests
    connection = sqlite3.connect('/home/przemek/Desktop/tmpfile')
    cursor = connection.cursor()
    # cursor.execute('CREATE TABLE t (thebin BLOB)')

    cursor.execute('INSERT INTO t VALUES(?)', [bytes(100000)])
    cursor.execute('SELECT * FROM t')
    rows = cursor.fetchall()
    connection.commit()
    connection.close()
    print(len(rows))
    print(rows)



