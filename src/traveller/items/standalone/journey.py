import datetime
import time

from traveller.items.item import Item
from traveller.util import misc
from traveller.common.custom_datetime import CustomDateTime
from traveller.items.common_members.start_end_time_holder import StartEndTimeHolder
from traveller.items.common_members.start_end_location_holder import StartEndLocationHolder


class Journey(Item, StartEndTimeHolder, StartEndLocationHolder):
    """ Whole journey
    """
    UNSPECIFIED_JOURNEY_NAME = 'Unspecified'  # For trips or other items not belonging to any journey
    UNSPECIFIED_JOURNEY_DB_ID = 1

    def __init__(self,
                 db_id: int = None,
                 name: str = '',
                 start_location_item_id: int = None,
                 end_location_item_id: int = None,
                 start_unix_time: int = None,
                 end_unix_time: int = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        Item.__init__(self, note=note, modification_time=modification_time, creation_time=creation_time)
        StartEndTimeHolder.__init__(self,
                                    start_unix_time=start_unix_time,
                                    end_unix_time=end_unix_time)
        StartEndLocationHolder.__init__(self,
                                        start_location_item_id=start_location_item_id,
                                        end_location_item_id=end_location_item_id)
        self.db_id = db_id
        self.name = name

    # DATABASE ACCESS START
    def check_if_allowed_to_delete(self):
        if self.name == self.UNSPECIFIED_JOURNEY_NAME:
            return False
        return True

    def validate_before_adding_to_database(self):
        if self.item_id is not None or self.db_id is not None \
                or not self.name:
            raise ValueError('Can not write Journey to database, invalid object!')

    def get_short_description(self):
        if self.start_unix_time is not None:
            date_str = CustomDateTime(self.start_unix_time).to_datetime().strftime('%m.%Y')
            return '{} ({})'.format(self.name, date_str)
        else:
            return '{}'.format(self.name)

    @staticmethod
    def get_table_name():
        return 'journeys'

    def __eq__(self, other):
        # Journey is same, if it has same name and same start day
        if isinstance(other, Journey):
            start_date_time = self.get_start_time_as_datetime()
            start_date_time_other = other.get_start_time_as_datetime()
            is_same_day = not misc.is_different_day(start_date_time, start_date_time_other)
            return self.name == other.name \
                and is_same_day
        return False

    def __hash__(self):
        return id(self)

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'name', 'TEXT',
            'start_location_item_id', 'INTEGER',
            'end_location_item_id', 'INTEGER',
            'start_unix_time', 'INTEGER',
            'end_unix_time', 'INTEGER',
            ]
    # DATABASE ACCESS END
