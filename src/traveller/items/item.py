"""
    def get_allowed_belongings(self) -> list:
Each instance of specific item can belong to other types of item.
Items can be grouped (e.g. group of locations), which will be often shared.
Some belongings may be transitive, e.g. photo can belong to the city, city to the journey as well as to location
"""
from traveller.common.sqlite_serializeable import SqliteSerializable


class Item(SqliteSerializable):
    """Base class for all travel elements
    """

    def __init__(self,
                 db_id: int = None,
                 item_type: int = None,
                 item_id: int = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        self.item_id = item_id  # id in all item database, will be determined after adding to database
        self.db_id = db_id  # id in database of specific type, e.g. id of city in city tables. Required before write to database
        self.item_type = item_type

        self.creation_time = creation_time
        self.modification_time = modification_time
        self.note = note

    @classmethod
    def get_type_name(cls):
        return cls.__name__

    # DATABASE ACCESS START
    def validate_before_adding_to_database(self):
        if type(self) != Item:
            raise NotImplementedError('Function "validate_before_adding_to_database" not implemented for {}!'.format(self.__class__))
        if self.item_id is not None or self.db_id is None:
            raise ValueError('Can not write Item to database, invalid object!')

    @staticmethod
    def get_table_name():
        return 'items'

    @classmethod
    def get_metadata_database_column_names_and_types(cls):
        return [
            'creation_time', 'INTEGER',
            'modification_time', 'INTEGER',
            'note', 'TEXT',
        ]

    @staticmethod
    def get_database_column_names_and_types():
        return [
            'db_id', 'INTEGER',
            'item_id', 'INTEGER PRIMARY KEY',
            'item_type', 'INTEGER',
            # 'parent_item_id', 'INTEGER',
        ]
    # DATABASE ACCESS END

    def get_short_description(self) -> str:
        return str(self)

    def get_short_description_with_engine(self, traveller_database):
        """If item requires engine to generate description"""
        return self.get_short_description()

    # def get_long_description(self, engine) -> str:
    #     """
    #     Also multiline description allowed.
    #     May require to use traveller engine to get detailed information about some members defined by id only
    #     """
    #     return str(self)

    # def get_what_is_attached_to_it(self):
    #     """What item are attached to this one element
    #     e.g. if it is journey, some photos can be returned
    #     """
    #     raise NotImplementedError
    #
    # def get_what_is_it_attached_to(self):
    #     """To What item is this item attached
    #     e.g. if it is photo, journey can be returned
    #     """
    #     raise NotImplementedError
    #
    # @staticmethod
    # def get_what_can_be_attached_to_it():
    #     """Returns list of items, which can belong to this type of item"""
    #     raise NotImplementedError('Allowed belongings have to be overwritten in child class!')
    #
    # @staticmethod
    # def get_what_can_it_be_attached_to(self) -> list:
    #     """Return list of items, to which this type of item can be attached"""
    #     raise NotImplementedError('Allowed belongings have to be overwritten in child class!')
