from traveller.items.standalone.journey import Journey
from traveller.items.standalone.photo import Photo
from traveller.items.visits.visit import Visit
from traveller.items.spots.places.place import Place

from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor

from traveller.prompts.locations.city_prompt import CityPrompt
from traveller.prompts.locations.admin_region_minor_prompt import AdminRegionMinorPrompt
from traveller.prompts.locations.admin_region_major_prompt import AdminRegionMajorPrompt
from traveller.prompts.locations.country_prompt import CountryPrompt


TYPES_OF_ITEMS_IN_DATABASE = {
    Journey: 1,
    # Trip: 2,
    Photo: 3,
    City: 4,
    Country: 5,
    AdminRegionMinor: 6,
    AdminRegionMajor: 7,
    Visit: 8,
    Place: 9,
}

INV_TYPES_OF_ITEMS_IN_DATABASE = {v: k for k, v in TYPES_OF_ITEMS_IN_DATABASE.items()}

# TYPES_OF_ITEMS_IN_DATABASE = {
#     Journey: Journey.__name__,
#     Trip: Trip.__name__,
#     Photo: Photo.__name__,
#     City: City.__name__,
#     Country: Country.__name__,
#     AdminRegionMinor: AdminRegionMinor.__name__,
#     AdminRegionMajor: AdminRegionMajor.__name__,
# }


PROMPTS_ONLY_TYPES = {
    CityPrompt,
    CountryPrompt,
    AdminRegionMajorPrompt,
    AdminRegionMinorPrompt
}


def is_prompt_database_item(item):
    for prompt_class in PROMPTS_ONLY_TYPES:
        if isinstance(item, prompt_class):
            return True
    return False


CAN_BELONG_TO = {

}

"""
Relations:
what and how can belong

"""
