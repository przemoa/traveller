from traveller.items.item import Item


class Coordinates:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude


class Location(Item):
    """
    Base class for administrative locations (Country, City, AdminRegionMinor and -Major)
    """
    def __init__(self,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        super().__init__(note=note, modification_time=modification_time, creation_time=creation_time)
        # self.parent_location = None
