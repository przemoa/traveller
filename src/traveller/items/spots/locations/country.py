from traveller.items.spots.locations.location import Location
from traveller.items.item import Item


class Country(Location):
    """
    """

    def __init__(self,
                 iso_code: str = '',
                 name: str = '',
                 continent_code: str = '',
                 db_id: int = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        super().__init__(note=note, modification_time=modification_time, creation_time=creation_time)
        self.db_id = db_id
        self.name = name
        self.iso_code = iso_code
        self.continent_code = continent_code

    # DATABASE ACCESS START
    def validate_before_adding_to_database(self):
        if self.item_id is not None or self.db_id is not None \
                or not self.name or not self.iso_code:
            raise ValueError('Can not write Country to database, invalid object!')

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'iso_code', 'CHARACTER(2)',
            'name', 'TEXT',
            'continent_code', 'TEXT',
        ]

    @staticmethod
    def get_table_name():
        return 'countries'
    # DATABASE ACCESS END

    def __str__(self):
        return '{0} [{1}]'.format(self.name, self.iso_code)

    def __repr__(self):
        txt = '{0}(name={1}, iso_code={2})'.format(self.__class__, self.name, self.iso_code)
        return txt

    def __eq__(self, other):
        if isinstance(other, Country):
            return self.iso_code == other.iso_code
        return False

    def __hash__(self):
        return id(self)

    def get_short_description(self):
        return '{0} [{1}]'.format(self.name, self.iso_code)

