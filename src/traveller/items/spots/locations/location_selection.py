from enum import Enum

from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor


class SelectionType(Enum):
    ANY, ONLY_CITY = range(2)


class LocationSelection:
    def __init__(self):
        # For each field: None, appropriate location (e.g. Country object, or text with name,
        # if field is not Location object, just plain text )probably not existing location)
        self.country = None
        self.admin_major = None
        self.admin_minor = None
        self.city = None

    @staticmethod
    def get_all_location_types() -> dict:
        return [
            ('country', Country),
            ('admin_major', AdminRegionMajor),
            ('admin_minor', AdminRegionMinor),
            ('city', City),
        ]

    def get_youngest_selection(self, selection_type: SelectionType = SelectionType.ANY):
        youngest_selection = None
        if isinstance(self.city, City):
            youngest_selection = self.city
        elif selection_type in [SelectionType.ANY]:
            if isinstance(self.admin_minor, AdminRegionMinor):
                youngest_selection = self.admin_minor
            elif isinstance(self.admin_major, AdminRegionMajor):
                youngest_selection = self.admin_major
            elif isinstance(self.country, Country):
                youngest_selection = self.country
        return youngest_selection

    def get_location_description_string(self):
        youngest_selection = self.get_youngest_selection()
        if youngest_selection is None:
            return '-'

        description = ''
        for location_type, location_class in self.get_all_location_types():
            if isinstance(self[location_type], location_class):
                description += self[location_type].get_short_description()
            elif self[location_type]:
                description += self[location_type]
            else:
                description += '-'
            if isinstance(youngest_selection, location_class):
                return description
            description += ', '
        description = description[:-2]
        return description

    def get_short_description_text_or_empty_for_field(self, selection_type_to_describe=SelectionType.ANY):
        if self.get_youngest_selection() is None:
            return '-'

        full_text = ''
        # if selection_type_to_describe
        if isinstance(self.city, City):
            full_text += self.city.ascii_name + ', '
        elif selection_type_to_describe == SelectionType.ONLY_CITY:
            return 'City not selected'

        if isinstance(self.admin_minor, AdminRegionMinor):
            full_text += self.admin_minor.ascii_name + ', '
        else:
            if full_text is not '':
                full_text += '-, '

        if isinstance(self.admin_major, AdminRegionMajor):
            full_text += self.admin_major.ascii_name + ', '
        else:
            if full_text is not '':
                full_text += '-, '

        if isinstance(self.country, Country):
            full_text += '[' + self.country.iso_code + ']'
        else:
            if full_text is not '':
                full_text += '-'
        return full_text

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, item):
        self.__dict__[key] = item
