from traveller.items.spots.locations.location import Location
from traveller.items.item import Item


class City(Location):
    def __init__(self,
                 ascii_name: str = '',
                 country_code: str = '',
                 local_name: str = '',
                 population: int = None,
                 geoname_id: int = None,
                 latitude: float = None,
                 longitude: float = None,
                 geoname_admin1_code: str = '',
                 geoname_admin2_code: str = '',
                 admin_region_major_db_id: int = None,
                 admin_region_minor_db_id: int = None,
                 non_ascii_name: str = '',
                 db_id: int = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        super().__init__(note=note, modification_time=modification_time, creation_time=creation_time)
        self.db_id = db_id
        self.ascii_name = ascii_name
        self.country_code = country_code
        self.local_name = local_name
        self.population = population
        self.geoname_id = geoname_id
        self.latitude = latitude
        self.longitude = longitude
        self.admin_region_major_db_id = admin_region_major_db_id
        self.admin_region_minor_db_id = admin_region_minor_db_id
        self.non_ascii_name = non_ascii_name
        self.geoname_admin1_code = geoname_admin1_code
        self.geoname_admin2_code = geoname_admin2_code

    # DATABASE ACCESS START
    def validate_before_adding_to_database(self):
        if self.item_id is not None or self.db_id is not None \
                or not self.ascii_name or not self.country_code:
            raise ValueError('Can not write City to database, invalid object!')

    @staticmethod
    def get_table_name():
        return 'cities'

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'ascii_name', 'TEXT',
            'country_code', 'TEXT',
            'local_name', 'TEXT',
            'population', 'INTEGER',
            'geoname_id', 'INTEGER',
            'latitude', 'FLOAT',
            'longitude', 'FLOAT',
            'geoname_admin1_code', 'TEXT',
            'geoname_admin2_code', 'TEXT',
            'admin_region_major_db_id', 'INTEGER',
            'admin_region_minor_db_id', 'INTEGER',
            'non_ascii_name', 'TEXT'
            ]
    # DATABASE ACCESS END

    def __str__(self):
        return '{0} [{1}]'.format(self.ascii_name, self.country_code)

    def __repr__(self):
        txt = '{0}(ascii_name={1}, country_code={2}, geoname_id={3})'.format(self.__class__, self.ascii_name, self.country_code, self.geoname_id)
        return txt

    def __eq__(self, other):
        if isinstance(other, City):
            return self.country_code == other.country_code \
                and self.admin_region_major_db_id == other.admin_region_major_db_id \
                and self.admin_region_minor_db_id == other.admin_region_minor_db_id \
                and self.ascii_name == other.ascii_name
        return False

    def __hash__(self):
        return id(self)

    def get_short_description(self):
        return '{0}'.format(self.ascii_name)
