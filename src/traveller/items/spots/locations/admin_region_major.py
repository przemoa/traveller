from traveller.items.spots.locations.location import Location
from traveller.items.item import Item


class AdminRegionMajor(Location):
    def __init__(self,
                 ascii_name: str,
                 country_code: str = '',
                 non_ascii_name: str = '',
                 geoname_admin1_code: str = '',
                 db_id: int = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 latitude: float = None,
                 longitude: float = None,
                 ):
        super().__init__(note=note, modification_time=modification_time, creation_time=creation_time)
        self.db_id = db_id
        self.ascii_name = ascii_name
        self.country_code = country_code
        self.geoname_admin1_code = geoname_admin1_code
        self.non_ascii_name = non_ascii_name
        self.latitude = latitude
        self.longitude = longitude

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'admin_regions_major'

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'ascii_name', 'TEXT',
            'country_code', 'TEXT',
            'geoname_admin1_code', 'TEXT',
            'non_ascii_name', 'TEXT',
            'latitude', 'FLOAT',
            'longitude', 'FLOAT',
        ]

    # DATABASE ACCESS END
    def validate_before_adding_to_database(self):
        if self.item_id is not None or self.db_id is not None \
                or not self.ascii_name or not self.country_code:
            raise ValueError('Can not write AdminRegionMajor to database, invalid object!')

    def __str__(self):
        return '{0} [{1}]'.format(self.ascii_name, self.country_code)

    def __repr__(self):
        txt = '{0}(ascii_name={1}, country_code={2}, geoname_admin1_code={3})'.format(
            self.__class__, self.ascii_name, self.country_code, self.geoname_admin1_code)
        return txt

    def __eq__(self, other):
        if isinstance(other, AdminRegionMajor):
            return self.country_code == other.country_code \
                and self.ascii_name == other.ascii_name
        return False

    def __hash__(self):
        return id(self)

    def get_short_description(self):
        return '{0}'.format(self.ascii_name)
