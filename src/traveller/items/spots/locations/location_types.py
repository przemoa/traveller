from traveller.items.spots.locations.city import City
from traveller.items.spots.locations.country import Country
from traveller.items.spots.locations.admin_region_minor import AdminRegionMinor
from traveller.items.spots.locations.admin_region_major import AdminRegionMajor

LOCATION_TYPES = [
    Country,
    AdminRegionMajor,
    AdminRegionMinor,
    City,
]
