from traveller.items.item import Item


class Place(Item):
    """ General place, which is not enumerated as separate location class
    E.g. Some attraction, sight, lake, mountain ...
    Place belongs to some Location
    """

    def __init__(self,
                 db_id: int = None,
                 name: str = '',
                 parent_location_id: int = None,
                 latitude: float = None,
                 longitude: float = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        super().__init__(note=note, modification_time=modification_time, creation_time=creation_time)
        self.db_id = db_id
        self.name = name
        self.parent_location_id = parent_location_id
        self.latitude = latitude
        self.longitude = longitude

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'places'

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'name', 'TEXT',
            'parent_location_id', 'INTEGER',
            'latitude', 'FLOAT',
            'longitude', 'FLOAT',
            ]

    def validate_before_adding_to_database(self):
        if not self.name:
            raise ValueError('Can not write Place to database, invalid object!')

    def get_short_description(self):
        return self.name
    # DATABASE ACCESS END

    def __eq__(self, other):
        if isinstance(other, Place):
            return self.name == other.name
        return False

    def __hash__(self):
        return id(self)
