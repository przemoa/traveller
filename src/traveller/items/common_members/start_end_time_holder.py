import datetime

from traveller.common.custom_datetime import CustomDateTime


class StartEndTimeHolder:
    def __init__(self,
                 start_unix_time,
                 end_unix_time,
                 ):
        self.start_unix_time = start_unix_time
        self.end_unix_time = end_unix_time

    def get_start_time_as_datetime(self) -> datetime.datetime:
        return CustomDateTime(self.start_unix_time).to_datetime()

    def get_end_time_as_datetime(self) -> datetime.datetime:
        return CustomDateTime(self.end_unix_time).to_datetime()

    def set_start_time_from_datetime(self, date_time: datetime.datetime):
        self.start_unix_time = CustomDateTime.from_datetime(date_time).unix_time

    def set_end_time_from_datetime(self, date_time: datetime.datetime):
        self.end_unix_time = CustomDateTime.from_datetime(date_time).unix_time
