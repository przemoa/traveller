class StartEndLocationHolder:
    def __init__(self,
                 start_location_item_id: int,
                 end_location_item_id: int,
                 ):
        self.start_location_item_id = start_location_item_id
        self.end_location_item_id = end_location_item_id
