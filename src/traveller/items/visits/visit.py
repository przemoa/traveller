from traveller.items.item import Item
from traveller.items.common_members.start_end_time_holder import StartEndTimeHolder
from traveller.util import misc


class Visit(Item, StartEndTimeHolder):
    """
    Base class for any visit
    """

    def __init__(self,
                 db_id: int = None,
                 visited_item_id: int = None,
                 start_unix_time: int = None,
                 end_unix_time: int = None,
                 journey_db_id: int = None,
                 note: str = '',
                 modification_time: int = None,
                 creation_time: int = None,
                 ):
        Item.__init__(self, note=note, modification_time=modification_time, creation_time=creation_time)
        StartEndTimeHolder.__init__(self,
                                    start_unix_time=start_unix_time,
                                    end_unix_time=end_unix_time)
        self.db_id = db_id
        self.visited_item_id = visited_item_id
        self.journey_db_id = journey_db_id

    # DATABASE ACCESS START
    @staticmethod
    def get_table_name():
        return 'visits'

    @staticmethod
    def get_database_column_names_and_types():
        return Item.get_metadata_database_column_names_and_types() + [
            'db_id', 'INTEGER PRIMARY KEY',
            'visited_item_id', 'INTEGER',
            'start_unix_time', 'INTEGER',
            'end_unix_time', 'INTEGER',
            'journey_db_id', 'INTEGER',
            ]

    def validate_before_adding_to_database(self):
        if self.item_id is not None or self.db_id is not None \
                or self.visited_item_id is None or self.start_unix_time is None:
            raise ValueError('Can not write Visit to database, invalid object!')

    # DATABASE ACCESS END

    def __eq__(self, other):
        # Visit is same, if it has same visited item id and same start day
        if isinstance(other, Visit):
            start_date_time = self.get_start_time_as_datetime()
            start_date_time_other = other.get_start_time_as_datetime()
            is_same_day = not misc.is_different_day(start_date_time, start_date_time_other)
            return self.visited_item_id == other.visited_item_id \
                and is_same_day
        return False

    def __hash__(self):
        return id(self)

    def get_short_description_with_engine(self, traveller_database):
        return self.get_visit_name(traveller_database=traveller_database)

    def get_visit_name(self, traveller_database) -> str:
        """
        To create visit name, visited spot is required to be known (and read from database), therefore database access is required
        :param traveller_database: database service
        :return:
        """
        if self.visited_item_id is not None:
            visited_item = traveller_database.get_item_by_item_id(item_id=self.visited_item_id)
            return self.get_visit_name_for_selected_spot(visited_item=visited_item)
        else:
            return self.get_visit_name_for_selected_spot(visited_item=None)

    def get_visit_name_for_selected_spot(self, visited_item) -> str:
        """
        To create visit name, selected spot (location/place) is required to be known.
        Visit contains only id, so Spot item have to be passed
        :param visited_item:
        :return:
        """
        if visited_item is None:
            return '-'
        else:
            visit_name = visited_item.get_short_description()
            if self.start_unix_time is not None:
                visit_name += ' ({})'.format(self.get_start_time_as_datetime().strftime('%d.%m.%Y'))
        return visit_name
