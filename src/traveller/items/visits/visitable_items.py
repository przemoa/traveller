from traveller.items.spots.locations.location_types import LOCATION_TYPES
from traveller.items.spots.places.place_types import PLACE_TYPES


VISITABLE_ITEMS = LOCATION_TYPES + PLACE_TYPES


class VisitedItemType:
    LOCATION = 'Location (Country, region, city)'
    PLACE = 'Place'  # TODO

    ALL_VISIT_TYPES = [LOCATION, PLACE]

