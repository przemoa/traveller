from traveller.items.visits.visit import Visit


class LocationVisit(Visit):
    """
    For visiting item of type Place
    """
    pass
