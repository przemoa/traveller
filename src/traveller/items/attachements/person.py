from tests.tmp.component import Component


class PersonType:
    FAMILY_MEMBER = 'Family member'
    FRIEND = 'Friend'


class Person(Component):
    """Stores information about single person, which can be companion during travel.

    Attributes:
        first_name (str): First name of person
        last_name (str): Last name of person (if don't known then empty)
        person_type (PersonType): Defines type of acquaintance, useful for displaying description of person
        family_title (str): Title which can be displayed instead of name (for family members only)
    """

    def __init__(self, person_type: PersonType, first_name: str, last_name: str = '', family_title: str = ''):
        super().__init__()
        self.first_name = first_name
        self.last_name = last_name
        self.person_type = person_type
        self.family_title = family_title

    def get_short_description(self) -> str:
        """Returns short description of person, for displaying purpose
        """
        description = ''

        if self.person_type == PersonType.FAMILY_MEMBER:
            if self.family_title:
                description = '{0} ({1})'.format(self.family_title, self.first_name)
            else:
                description = '{0}'.format(self.first_name)
        elif self.person_type == PersonType.FRIEND:
            if self.last_name:
                description = '{0} {1}'.format(self.first_name, self.last_name)
            else:
                description = '{0} (friend)'.format(self.first_name)
        else:
            raise Exception('Unknown person type, can not obtain short description')

        return description
