import os

MAIN_DIR = os.path.dirname(os.path.realpath(__file__))
GENERATED_FILES_DIRECTORY = os.path.join(MAIN_DIR, 'generated')
DOWNLOADED_FILES_DIRECTORY = os.path.join(MAIN_DIR, 'download')


def make_dir_if_not_existing(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def create_generated_directory():
    make_dir_if_not_existing(GENERATED_FILES_DIRECTORY)


def create_downloaded_directory():
    if not os.path.exists(DOWNLOADED_FILES_DIRECTORY):
        os.makedirs(DOWNLOADED_FILES_DIRECTORY)


create_generated_directory()
create_downloaded_directory()
