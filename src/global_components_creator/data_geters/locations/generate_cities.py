"""
Gather data about cities:
- name
- location
- coordinates
- population
"""

import os
import csv
import logging
import urllib.request

import zipfile

from traveller.prompts.locations.city_prompt import CityPrompt

import global_components_creator.config


def _download_city_info_file():
    logging.info('Downloading city info file...')
    final_txt_file = os.path.join(global_components_creator.config.DOWNLOADED_FILES_DIRECTORY, 'cities1000.txt')
    if not os.path.isfile(final_txt_file):
        zip_file_path = os.path.join(global_components_creator.config.DOWNLOADED_FILES_DIRECTORY, 'cities1000.zip')
        if not os.path.isfile(zip_file_path):
            url = 'http://download.geonames.org/export/dump/cities1000.zip'
            urllib.request.urlretrieve(url, zip_file_path)
        logging.info('Unzipping city info file...')
        zip_ref = zipfile.ZipFile(zip_file_path, 'r')
        zip_ref.extractall(global_components_creator.config.DOWNLOADED_FILES_DIRECTORY)
        zip_ref.close()
    logging.info('City info file downloaded: {}'.format(final_txt_file))
    return final_txt_file


def _parse_city_info_file(file_path, searched_countries_codes: list) -> list:
    geonameid_fileds = [
        'geoname_id', 'name',	'ascii_name',
        'alternative_names', 'latitude', 'longitude',
        'feature_class', 'feature_code', 'country_code',
        'cc2', 'admin1_code', 'admin2_code',
        'admin3_code', 'admin4_code', 'population',
        'elevation'
    ]
    column_with_geoname_id = geonameid_fileds.index('geoname_id')
    column_with_name = geonameid_fileds.index('name')
    column_with_ascii_name = geonameid_fileds.index('ascii_name')
    column_with_latitude = geonameid_fileds.index('latitude')
    column_with_longitude = geonameid_fileds.index('longitude')
    column_with_population = geonameid_fileds.index('population')
    column_with_country_code = geonameid_fileds.index('country_code')
    column_with_admin1_code = geonameid_fileds.index('admin1_code')
    column_with_admin2_code = geonameid_fileds.index('admin2_code')

    cities_list = []
    with open(file_path, 'rt') as csv_file:
        cities_reader = csv.reader(csv_file, delimiter='\t')
        for row in cities_reader:
            city_country_code = row[column_with_country_code]
            if city_country_code not in searched_countries_codes:
                continue
            city = CityPrompt(
                ascii_name=row[column_with_ascii_name],
                country_code=city_country_code,
                population=int(row[column_with_population]),
                geoname_id=int(row[column_with_geoname_id]),
                latitude=float(row[column_with_latitude]),
                longitude=float(row[column_with_longitude]),
                non_ascii_name=row[column_with_name],
                geoname_admin1_code=row[column_with_admin1_code],
                geoname_admin2_code=row[column_with_admin2_code],
                # local_name=row[column_with_ascii_name],
            )
            cities_list.append(city)
    return cities_list


def get_cities_for_countries(countries_list: list) -> list:
    cities_file_path = _download_city_info_file()
    countries_codes = []
    for country in countries_list:
        countries_codes.append(country.iso_code)
    cities_list = _parse_city_info_file(cities_file_path, searched_countries_codes=countries_codes)
    return cities_list


if __name__ == '__main__':
    from traveller.items.locations import Country
    cities = get_cities_for_countries([Country(name='Poland', iso_code='PL', continent_code='EU')])
    for city in cities:
        print(city)
