"""
Gather data about countries:
- name
- location
- capital
- population
"""

import os
import csv
import logging
import urllib.request

from traveller.prompts.locations.country_prompt import CountryPrompt

import global_components_creator.config


def _download_country_info_file() -> str:
    logging.info('Downloading country info file...')
    countries_file_path = os.path.join(global_components_creator.config.DOWNLOADED_FILES_DIRECTORY, 'countryInfo.txt')
    if not os.path.isfile(countries_file_path):
        url = 'http://download.geonames.org/export/dump/countryInfo.txt'
        urllib.request.urlretrieve(url, countries_file_path)
    logging.info('Country info file downloaded to {}'.format(countries_file_path))
    return countries_file_path


def _parse_country_info_file(file_path, searched_continent: str) -> list:
    logging.info('Parsing location info file...')
    countries_list = []
    with open(file_path, 'rt') as csv_file:
        country_reader = csv.reader(csv_file, delimiter='\t')
        last_row_with_hash = None

        headers_row_analysed = False
        column_with_iso_code = None
        column_with_name = None
        column_with_continent = None

        for row in country_reader:
            if row[0][0] == '#':
                last_row_with_hash = row
            else:
                if not headers_row_analysed:
                    column_with_iso_code = last_row_with_hash.index('#ISO')
                    column_with_name = last_row_with_hash.index('Country')
                    column_with_continent = last_row_with_hash.index('Continent')
                    headers_row_analysed = True
                if row[column_with_continent] != searched_continent:
                    continue
                country = CountryPrompt(
                    name=row[column_with_name],
                    continent_code=row[column_with_continent],
                    iso_code=row[column_with_iso_code]
                )
                countries_list.append(country)
    logging.info('Country info file parsing finished, found {} countries'.format(len(countries_list)))
    return countries_list


def get_countries_in_europe_list():
    countries_file_path = _download_country_info_file()
    countries_list = _parse_country_info_file(countries_file_path, searched_continent='EU')
    return countries_list


if __name__ == '__main__':
    countries = get_countries_in_europe_list()
    for country in countries:
        print(str(country))
