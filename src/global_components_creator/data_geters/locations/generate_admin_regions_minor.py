"""
Gather data about cities:
- name
- location
- coordinates
- population
"""

import os
import csv
import logging
import urllib.request

from traveller.prompts.locations.admin_region_minor_prompt import AdminRegionMinorPrompt

import global_components_creator.config


def _download_admin2_info_file():
    logging.info('Downloading Admin2 info file...')
    admin1_info_file_path = os.path.join(global_components_creator.config.DOWNLOADED_FILES_DIRECTORY, 'admin2Codes.txt')
    if not os.path.isfile(admin1_info_file_path):
        url = 'http://download.geonames.org/export/dump/admin2Codes.txt'
        urllib.request.urlretrieve(url, admin1_info_file_path)
    logging.info('Admin2 info file downloaded to {}'.format(admin1_info_file_path))
    return admin1_info_file_path


def _parse_admin2_regions_info_file(file_path, searched_countries_codes: list) -> list:
    geonameid_fileds = [
        'country_admin1_and_2_code', 'name',	'ascii_name'
    ]
    column_with_country_admin1_and_2_code = geonameid_fileds.index('country_admin1_and_2_code')
    column_with_name = geonameid_fileds.index('name')
    column_with_ascii_name = geonameid_fileds.index('ascii_name')

    admin2_regions_list = []
    with open(file_path, 'rt') as csv_file:
        cities_reader = csv.reader(csv_file, delimiter='\t')
        for row in cities_reader:
            country_code, admin1_code_str, admin2_code_str = row[column_with_country_admin1_and_2_code].split('.')
            if country_code not in searched_countries_codes:
                continue
            admin2_region = AdminRegionMinorPrompt(
                ascii_name=row[column_with_ascii_name],
                country_code=country_code,
                geoname_admin1_code_parent=admin1_code_str,
                geoname_admin2_code=admin2_code_str,
                non_ascii_name=row[column_with_name],
            )
            admin2_regions_list.append(admin2_region)
    return admin2_regions_list


def get_admin2_regions_for_countries(countries_list: list) -> list:
    admin2_file_path = _download_admin2_info_file()
    countries_codes = []
    for country in countries_list:
        countries_codes.append(country.iso_code)
    admin2_list = _parse_admin2_regions_info_file(admin2_file_path, searched_countries_codes=countries_codes)
    return admin2_list


if __name__ == '__main__':
    from traveller.items.locations import Country
    admin2_regions = get_admin2_regions_for_countries([Country(name='Poland', iso_code='PL', continent_code='EU')])
    for admin2_region in admin2_regions:
        print(admin2_region)
