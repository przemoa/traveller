from traveller.items.attachements.person import Person, PersonType


def generate_persons_for_przemek() -> list:
    persons_list = [
        Person(first_name='Dagmara', type=PersonType.FAMILY_MEMBER),
        Person(first_name='Agnieszka', type=PersonType.FAMILY_MEMBER),
        Person(first_name='Radek', type=PersonType.FAMILY_MEMBER, family_title='Father'),
        Person(first_name='Hania', type=PersonType.FAMILY_MEMBER, family_title='Mother'),
        Person(first_name='Danusia', type=PersonType.FAMILY_MEMBER, family_title='Grandma'),
        Person(first_name='Krzysztof', type=PersonType.FRIEND),
    ]
    return persons_list


def generate_random_persons() -> list:
    persons_list = [
        Person(first_name='Maribel', last_name='Robbins', type=PersonType.FRIEND),
        Person(first_name='Jasmin', last_name='Blair', type=PersonType.FRIEND),
        Person(first_name='Chasity', last_name='Riley', type=PersonType.FRIEND),
        Person(first_name='Hanna', last_name='Ponce', type=PersonType.FAMILY_MEMBER, family_title='Mother'),
        Person(first_name='Agustin', last_name='Trujillo', type=PersonType.FAMILY_MEMBER, family_title='Father'),
        Person(first_name='Elyse', last_name='Welch', type=PersonType.FAMILY_MEMBER),
        Person(first_name='Rylee', type=PersonType.FRIEND),
        Person(first_name='Fabian', type=PersonType.FRIEND),
    ]
    return persons_list


if __name__ == '__main__':
    random_persons = generate_random_persons()
    for i, person in enumerate(random_persons):
        print('{}: {}'.format(i, person.get_short_description()))

    persons_for_przemek = generate_persons_for_przemek()
    for i, person in enumerate(persons_for_przemek):
        print('{}: {}'.format(i, person.get_short_description()))