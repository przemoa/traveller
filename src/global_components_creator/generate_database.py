# use sqlitebrowser to view files

import os
import sqlite3 as lite

from traveller.prompts.locations.city_prompt import CityPrompt
from traveller.prompts.locations.admin_region_major_prompt import AdminRegionMajorPrompt
from traveller.prompts.locations.admin_region_minor_prompt import AdminRegionMinorPrompt
from traveller.prompts.locations.country_prompt import CountryPrompt

from global_components_creator.data_geters.locations.generate_cities import get_cities_for_countries
from global_components_creator.data_geters.locations.generate_countries import get_countries_in_europe_list
from global_components_creator.data_geters.locations.generate_admin_regions_major import get_admin1_regions_for_countries
from global_components_creator.data_geters.locations.generate_admin_regions_minor import get_admin2_regions_for_countries


def main():
    geoname_code_for_next_dummy_region = 1

    countries = get_countries_in_europe_list()
    cities = get_cities_for_countries(countries)
    admin1_regions = get_admin1_regions_for_countries(countries)
    admin2_regions = get_admin2_regions_for_countries(countries)

    for city in cities:
        if city.geoname_admin2_code == '':
            # attach to dummy adminregion2
            dummy_region = None
            for admin2_region in admin2_regions[::-1]:  # from back, before there will be added dummy region
                if admin2_region.country_code == city.country_code and \
                        admin2_region.geoname_admin1_code_parent == city.geoname_admin1_code:
                    dummy_region = admin2_region
                    break
            else:
                dummy_region = AdminRegionMinorPrompt(
                    ascii_name='-',
                    country_code=city.country_code,
                    geoname_admin1_code_parent=city.geoname_admin1_code,
                    non_ascii_name='Dummy Region',
                    geoname_admin2_code='dummy_' + str(geoname_code_for_next_dummy_region))
                admin2_regions.append(dummy_region)
                geoname_code_for_next_dummy_region += 1
            city.geoname_admin2_code = dummy_region.geoname_admin2_code

    print('cities:', len(cities))
    print('admin1 regions:', len(admin1_regions))
    print('admin2 regions:', len(admin2_regions))

    database_path = 'location_prompts.sqlite'
    try:
        os.remove(database_path)
    except OSError:
        pass

    conn = lite.connect(database_path)
    cursor = conn.cursor()

    calculate_coordinates(countries, admin1_regions, admin2_regions, cities)

    CountryPrompt.create_table(db_cursor=cursor)
    for country in countries:
        country.add_to_database(db_cursor=cursor)

    CityPrompt.create_table(db_cursor=cursor)
    for city in cities:
        city.add_to_database(db_cursor=cursor)


    AdminRegionMinorPrompt.create_table(db_cursor=cursor)
    for admin2_region in admin2_regions:
        admin2_region.add_to_database(db_cursor=cursor)

    AdminRegionMajorPrompt.create_table(db_cursor=cursor)
    for admin1_region in admin1_regions:
        admin1_region.add_to_database(db_cursor=cursor)


    conn.commit()
    conn.close()

    # READ
    conn = lite.connect(database_path)
    cursor = conn.cursor()
    example_request = \
        '''
        SELECT non_ascii_name, countries_prompts.name, population/1000 as population_in_thousadns 
        FROM cities_prompts 
        INNER JOIN countries_prompts
        on cities_prompts.country_code = countries_prompts.iso_code
        WHERE
        population > 100000
        ORDER BY population DESC
        LIMIT 30
        '''
    cursor.execute(example_request)
    single_data = cursor.fetchone()
    print('Fetched data single:', *single_data)

    all_data = cursor.fetchall()
    for i, row in enumerate(all_data):
        print(i, ':', *row)
    conn.close()

    '''
    If you need to get the id of the row you just inserted use lastrowid:
    id = cursor.lastrowid
    print('Last row id: %d' % id)
    '''


def calculate_coordinates(countries, admin1_regions, admin2_regions, cities):
    admin1_region_coordinates = {admin1_region: [0, 0, 0] for admin1_region in admin1_regions}
    admin2_region_coordinates = {admin2_region: [0, 0, 0] for admin2_region in admin2_regions}

    coordinates_structure = {country.iso_code: {} for country in countries}

    for admin1_region in admin1_regions:
        coordinates_structure[admin1_region.country_code][admin1_region.geoname_admin1_code] = {}

    for admin2_region in admin2_regions:
        if not admin2_region.geoname_admin1_code_parent:
            continue

        country_structure = coordinates_structure[admin2_region.country_code]

        admin1_region_structure = country_structure.get(admin2_region.geoname_admin1_code_parent, None)
        if admin1_region_structure is None:
            continue
        admin1_region_structure[admin2_region.geoname_admin2_code] = [0, 0, 0]  # count, lat sum, lon sum

    for city in cities:
        country_structure = coordinates_structure[city.country_code]

        admin1_region_structure = country_structure.get(city.geoname_admin1_code, None)
        if admin1_region_structure is None:
            continue

        admin2_region_structure = admin1_region_structure.get(city.geoname_admin2_code, None)
        if admin2_region_structure is None:
            continue

        if admin2_region_structure[1] is not None:
            admin2_region_structure[0] += 1
            admin2_region_structure[1] += city.latitude
            admin2_region_structure[2] += city.longitude

    for admin2_region in admin2_regions:
        country_structure = coordinates_structure.get(admin2_region.country_code, None)
        if country_structure is None:
            continue

        admin1_region_structure = country_structure.get(admin2_region.geoname_admin1_code_parent, None)
        if admin1_region_structure is None:
            continue

        admin2_region_structure = admin1_region_structure.get(admin2_region.geoname_admin2_code, None)
        if admin2_region_structure is None:
            continue

        if admin2_region_structure[0] != 0:
            admin2_region.latitude = admin2_region_structure[1] / admin2_region_structure[0]
            admin2_region.longitude = admin2_region_structure[2] / admin2_region_structure[0]


    for admin1_region in admin1_regions:
        country_structure = coordinates_structure.get(admin1_region.country_code, None)
        if country_structure is None:
            continue

        admin1_region_structure = country_structure.get(admin1_region.geoname_admin1_code, None)
        if admin1_region_structure is None:
            continue

        count = 0
        lat = 0
        lon = 0
        for key, admin2_region in admin1_region_structure.items():
            if admin2_region[0] != 0:
                count += 1
                lat += admin2_region[1] / admin2_region[0]
                lon += admin2_region[2] / admin2_region[0]
        if count != 0:
            admin1_region.latitude = lat / count
            admin1_region.longitude = lon / count

if __name__ == '__main__':
    main()
